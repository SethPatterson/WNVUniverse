<!--

SPDX-FileCopyrightText: 2020 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

[![REUSE status](https://api.reuse.software/badge/codeberg.org/RileyDuffield/WNVUniverse)](https://api.reuse.software/info/codeberg.org/RileyDuffield/WNVUniverse)

# WNVUniverse

The WNV universe is a free culture story world featuring. Woethief Nyla Valora
as the main character.

This repository contains stories and outlines for future stories. Stories can be
any genre as long as they take place in the WNV Universe.
