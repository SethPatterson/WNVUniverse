<!--
SPDX-FileCopyrightText: 2022 Riley Duffield

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# True Love's Kiss

![True Love's Kiss Cover](CoverArt/TrueLovesKiss-Cover.png)

"Tarry, take that one," the Lieutenant said.

Tarry ambled toward the house. Screams and pleas for mercy rent the air. Tarry threw up in his mouth.

He shoved the door open. A young woman, named Lealynn, cowered in a corner, clutching her mother's body. Green tattoos of animals covered the arms of her dead father. His pooled blood stained Tarry's moccasins. Tarry rummaged through the house loudly and ignored Lealynn.

He hated raids. Why should they make trouble with weak villages? The villagers never did anything to him besides call him names. His own people did that too. He just wanted to work on a nice, peaceful ranch. Animals made more sense than people.

When Slither entered the house to loot it, Lealynn rushed Tarry. Tarry raised his hands to redirect a blade, but she didn't have one. She threw her arms around him and kissed his lips.

Tarry froze: confused. As she leaned into him, it threw him off balance.

"Hey, you can't have her. This one's mine," Slither jerked her away by her hair. Tarry pummeled Slither's eye. Tarry didn't like people who hurt girls.

Slither drew a knife.

"What's going on here?" the Lieutenant interrupted.

"He's trying to steal my girl," Tarry said.

"Get your own girl, Slither. We need to move. Enemy bird-riders are coming," the Lieutenant said.

They left Tarry and Lealynn alone.

"Please pack your bag, miss. The death squad'll kill you if you don't come with me right quick," Tarry said.

"I don't want to go," she sobbed.

"Now don't cry. I hate seeing women cry," Tarry said. "I promise I'll get you back to your people."

"Please don't make me go with you," Lealynn begged.

Tarry stuffed food and clothes into a bag. After he sheathed her father's hunting knife, which was worth more than everything Tarry owned, he put his hand on her shoulder.

"God, please keep us safe," he prayed. "Please help me be her friend."

The prayer from one of the "godless" toads shocked Lealynn.

After the prayer, he took her hand and led her to the safety of his mount. The beast's thick hair brushed against her face as she climbed the rope ladder up to the saddle.

Tarry climbed on behind her and told her to take the reins. He cupped his hands around her ears to block out the sound of the death squad's work. Only female captives survived.

They rode hard. Flies buzzed around them and drew lots of blood. When the captives wept, they met their enemies' fists. Tarry wished he were brave enough to save them.

As they fled the bird-riders, Lealynn made no sound. Tall grass mutilated her bare feet. Tarry didn't know how to treat human wounds, only those of animals. He feared infection.

Despite the captives's entreaties, the band refused to stop until the sun touched the horizon.

Tarry kept his mount on the perimeter of the band. No one liked him and he didn't like them.

As Slither approached Tarry and Lealynn, Tarry's beast growled. Tarry gripped his sword.

"She's mine," Tarry said. "If you come near her, I'll fight you."

"They're putting you on the front lines next time. You won't live long enough to keep her," Slither sneered.

Slither grabbed Tarry by the collar and pressed him against his beast.

"I might not wait long enough for you to die in battle," Slither said.

He threw the smaller Tarry to the ground. When he approached Lealynn, she drew her father's knife and held it before her.

"She doesn't like you," Tarry said. "Leave her alone."

Tarry's mount readied to charge Slither.

"That knife will end up in your back, Tarry," Slither walked away with his wine-flagon unstopped.

"I'm sorry, miss. Smart thinking grabbing that knife," Tarry said.

"Aren't you afraid I'll use it?" she asked.

"I don't know. It's your knife," Tarry shrugged. "I don't have the right to keep it from you."

"I don't want to stay here," she shouldered her bag and turned.

"Please don't leave. The wolves will catch you. Your feet are too hurt to walk," Tarry said.

"You can't stop me," she said.

"If you must go, take my mount, but I can't protect you from the band if you leave," Tarry said.

She stepped on the rope ladder, but tumbled off. Tears sprang from her eyes because of her feet. She tried not to cry. She failed.

Tarry wrapped her feet in purple-grass, the same way he treated animal wounds. Images of her dead parents flashed through both of their minds. He hated what his people did to hers.

"Please don't leave tonight," he said. "My beast will keep you warm."

He showed her how to climb into the beast's left pouch. After parting the curtain of yarny hair, the pouch's entrance invited Lealynn in. She climbed in and fell asleep from exhaustion.

The violent wind caused Tarry to shiver terribly as he kept vigil outside Lealynn's pouch. The wind blocked out the captives's screams. He resolved to protect whatever innocence Lealynn had left.

Morning came after a sleepless night for Tarry. He wiped his bleary eyes and prayed over a distasteful meal. He gave Lealynn a larger portion than he gave himself.

His beast fed on the plentiful grass. Tarry brushed her fur and removed parasites from her. Humans feared the beasts, despite their sweet temperaments. The cruelty of his people turned them into monsters, but his beast had never felt a whip.

His ward shoveled food into her face with apathy. Fear and pain numbed her to her surroundings. He placed his moccasins on her feet to protect them from the grass.

Lealynn said nothing all day. She stared blankly at the endless sea of grass. The biting flies returned. Tarry placed his cloak around her to protect her.

"You're awful tender to that girl," Slither said. "You do know she's a slave right?"

"Injuries make them less pretty," he replied, though he didn't care about her beauty. He didn't want her hurt. He hated their lewdness.

At midday, they stopped at a spring to water their beasts.

"Time for the prisoners to bathe," the Lieutenant, who really wanted to see their forms, said. "They'll fetch more on the market tomorrow if they look fresh."

Horror filled Lealynn's face. She clutched the beast's fur tightly and refused to budge.

"Her too," the Lieutenant pointed.

"She doesn't want to," Tarry said.

"Slaves' opinions don't count," The Lieutenant said. "Get her down."

Tarry reached for his spear.

Five spears pointed at him.

"She belongs to us. We only let you keep her until the market," the Lieutenant said.

"I'll buy her," Tarry said.

"You can't afford her," the Lieutenant said.

Tarry pulled his mother's bangle from a concealed saddlebag.

"This should be enough. It's solid gold," Tarry announced.

"Where did you get that? Holding out with the loot?" Slither asked.

"It was my mother's. It has her name on it," Tarry declared even though none of the men could read.

"Fine," the Lieutenant said. "You can have her."

Tarry tossed his only hope of buying a ranch to the Lieutenant. He hoped he could get Lealynn back home soon. Protecting himself and his beast was hard, but protecting her exhausted him.

Tarry rode around a grove of trees to shield his eyes from the bathers. He wished he were brave enough to help them.

Horror marred Lealynn's face. Tarry felt bad. Humans weren't as tough as his people and she must miss her family terribly. He missed his mother. They hated her because she was too soft, like him.

That night, Tarry tried to keep vigil by whispering hymns, but fell asleep against his beast's side. Lealynn slept securely in the beast's pouch.

At three-to-dawn, brush crackled. Tarry drew his sword and prepared for an attack.

"We know you stole that bangle. That girl is mine," Slither said. "You don't deserve her. You've never done a brave thing in your life."

"I am a coward, but my beast isn't," Tarry threatened.

Tarry's beast, ordinarily docile, snarled at Slither.

Slither shook his head and reached for his empty wine-flagon.

"Go away, you're being too loud. She's trying to sleep," Tarry said.

"You're a fool, Tarry. She'll knife you in your sleep," Slither said as he skulked away.

"I'm sorry he scared you," Tarry stroked his nameless beast. "You rest now. Thank you for carrying me around today. I know the march is hard work with an extra person on your back."

He fell asleep with the fur running through his fingers.

At dawn, he tried to teach Lealynn how to milk the beast, but she didn't pay attention. She drank the milk though. Tarry cringed at the roughness of his band when they milked their animals, which gave milk without calving. Yarnbacks were beautiful creatures that deserved respect [^1].

Sunlight glinted off the spears that decorated the tents in the market city. Tarry's band hoped for a large profit from the slaves they captured. Seventeen women, not counting Tarry's friend, would meet new masters today. Tarry longed to free them, but fear restrained his hands.

The odors of spices, sweat, and animals mingled in the marketplace. Everyone gawked at the captives, contemplating whether they could afford such an extravagant purchase. Tarry kept Lealynn close and his sword closer. He feared a double-cross from his band.

At the unloading point, slavers shoved the captives into a dark tent to dress them for the auction. They ripped Lealynn away from him and clamped his arms behind his back.

"You can't do this. I paid for her right honest," Tarry protested.

"No one here remembers a payment," the Lieutenant said.

"Come on. Back me up, one of you," Tarry pleaded. "You saw me pay with the gold."

No one answered.

Tarry strained against his captors.

"If you really want her, you'll buy her at the auction," the Lieutenant said.

Tarry had no money, since he lost his only gold to their cheating. He searched for anything more valuable than his cheap weapons to give up for Lealynn's freedom.

During the auction, a slaver paraded girls before the hungry crowd. Tarry paid no attention to any of them. He wanted to rescue the girl who had kissed him. He wanted to prove to the band that he could fight for something.

They brought her out in a loose cotton robe, which revealed too much of her shape. Tarry missed her modest, brown dress.

*'How could anyone do that to her?'* Tarry wondered.

Slither made the first bid of 10 copper marbles.

Tarry offered his sword.

Slither countered with 20 copper marbles.

Tarry offered his sword and spear.

Slither offered two silver marbles.

Tarry offered his bridle as well.

The crowd hurled values too high for Tarry to exceed.

"I offer my saddle," Tarry shouted.

The crowd gasped. Each man prized his saddle, which marked his manhood and tribal history, above everything. The auctioneer sent couriers to fetch the saddle. When they returned, the auctioneer appraised its value at 100 silver marbles. No one countered.

Lealynn approached Tarry, timidly. He took her hand and led her to the entrance of the auction tent. He asked an attendant to return the brown dress, but they had already burned it.

"Well, at least I got your shawl miss," Tarry swung the bag of her things off his back and produced the shawl.

She let him put it around her but did not say anything. He handed her the hunting knife, which she strapped around her waist.

"There's nothing to do now but wait with my yarnback," Tarry said as he pulled a package of raisins out of the bag. "I found this at your house. You haven't eaten anything since breakfast."

He held out the raisins to her. She grabbed them and absently popped one at a time into her mouth.

"I'm sorry they took your clothes," Tarry said. "I tried to get them back. I'm glad you're free now."

She stared at her feet.

"Riding's going to be a bit harder without the saddle. You'll have to hang onto the hair," Tarry said.

Lealynn didn't reply.

The raiding party spent the night in the city. Since Tarry couldn't afford lodging, he and Lealynn slept in opposite pouches in the open square. At least the city guards ensured that no one bothered them.

At noon, the Lieutenant rounded up his hungover men and organized them into something that resembled a formation. The rested and reprovisioned troop rode South on the hunt for bison. Lealynn clung to the yarnback in silence.

As they rode, another raider called over his shoulder to Tarry, "You're a terrible fighter. I've never hunted with you, but I hope you're not a coward like in our last battle. How many do you think we'll kill?"

"We only need one to give us enough food for a month," Tarry said.

"But killing them is so much fun," the other man taunted.

"It is, but we should only kill what we need for food. Driving them off cliffs is a waste," Tarry said.

The man grunted and rode further up in the column.

During the trip, Lealynn said nothing. She didn't cry either. After three days of riding, they spotted a herd of bison. A 50-meter cliff rose out of the plane and could provide a point to drive the bison over.

As the Lieutenant discussed his plan, he ordered his men to encircle the animals and herd them to their deaths.

"Lieutenant," Tarry said. "It's such a waste to kill all of them. We can only process the meat of one."

"Tarry, stop being yellow. This is how it's always been done," the officer said.

"And there are fewer bison today than our grandfathers hunted," Tarry retorted.

"You're soft," accused Slither.

"Tarry, unless you plan to take a bison down on your own, we're driving them off the cliff," the Lieutenant said.

"Give me a stouter spear and I will," Tarry blurted.

His companions guffawed.

The Lieutenant tossed a spear to him. "If you can bring down that bison," he gestured to a large female. "I'll buy you a new saddle."

Tarry gulped but couldn't back down now. If he died, who would protect Lealynn?

"You need to get off now," he said to her. "I'll pick you up after I kill the bison."

She shook her head and clung tighter to the beast's back. After two minutes, he gave up on getting her to dismount. Staying with the men was more dangerous for her anyway.

Tarry brandished his spear and rode toward the herd. The largest female stood five meters tall. Its single horn could impale a yarnback and the tentacles on each side of its jaw could crush a man. If he could land his spear on the creature's soft neck, it would fall.

He rode quietly into the center of the herd. Tarry's yarnback, which had musk strong enough to cover Tarry's scent, did not frighten the bison. Once the hunt began, all the bison would panic and stampede.

When he stood before the herd's matron, he hurled his spear. He missed. The spear sliced into the bison's right tentacle. It bellowed in rage and the herd bolted.

The matron charged Tarry and his yarnback narrowly evaded it. The wounded tentacle swiped Lealynn off of her mount. She dug her knife into the tentacle as it strained to crush her.

Tarry readied his other spear, but the matron knocked him off his mount with its left tentacle. He grabbed enough hair to break his fall but his yarny crunched the spear shaft under its feet as it fled the raging monster.

Tarry drew his sword and plunged it into the matron's leg. It shook off Lealynn, who gripped the first spear as she fell. A tentacle swiped Tarry off his feet onto the compacted dirt. As he braced for the crush of its hooves, Lealynn tugged him out of the way.

She handed him the spear and on the bison's next pass, he hurled it. The spear struck true in the throat. The weakened animal hobbled and thrashed in anger.

Tarry carried Lealynn out of the way as the animal's death throes flung dust into the air. Lealynn trembled as she clutched her knife. A green glow snaked from the knife's hilt onto her forearm. After the glow dissipated, a green tattoo of a bison decorated her wrist. Her sheath hovered at her hip without anything supporting it.

"Thank you," Tarry said. "I couldn't have brought it down without you."

"Next time, throw better," she said and massaged her wrist.

Tarry laughed nervously. He eyed Lealynn.

"It is for my first kill," she said. "The knife turns green for a good kill and red for an evil kill."

"Why are you talking now?" Tarry asked. "You haven't said much since I met you. You seem braver."

"I feel braver. I think the knife always does that for those who use it," she said. "I will use it to escape," she hissed.

Tarry's companions encircled the dying beast and kept other bison from returning. After the animal lay still for half an hour, Tarry stepped through a pool of blood to examine it. It died in more pain than he wanted it to, but, at least, only one bison died.

They spent the rest of the day gutting, skinning, and butchering the animal. Nothing smelled worse than bison entrails. They sent Tarry into the animal to help remove its guts.

By sundown, a skeleton remained and every scrap of meat cooked over open fires. The smoke and salt preserved the meat for their future journeys. Dried bison meat required a sharp knife and strong jaw to consume.

On their ride back to the city to sell their excess meat, Slither decided to kill Tarry, whom he envied now that he showed his ability as a hunter.

While Tarry nursed a wound on his yarnback's leg, Slither crept behind him with an upraised knife. As long as no one saw him, he would get away with murder. Slaves hated their masters, so they didn't intervene when one of their masters died.

"Tarry, look out," Lealynn yelled.

Tarry turned and caught Slither's wrist. Slither punched Tarry in the jaw with his free hand, which caused Tarry to release his grip. With Slither's next slash, he drew a streak of blood on Tarry's left shoulder.

Tarry reached for his knife but Slither seized the handle first. While he held one blade to Tarry's throat and the other ready to strike, Lealynn snuck behind him and buried her hunting knife in Slither's neck. Green light radiated from the hilt.

Slither coughed and sputtered as he collapsed. Tarry disarmed Slither and pushed a cloth against Slither's neck to slow the bleeding. Despite his best efforts, Slither died in less than a minute.

"Why did you try to save him?" Lealynn asked.

"I don't like death," Tarry said.

"So, you are mad at me?" she asked as she rubbed a new, man-shaped tattoo.

"No, you saved my life. Thank you," Tarry said.

"I've been afraid to talk. I hated you and all you toads," she said. "I don't hate you, Tarry. You've done your best to help me."

Tarry frowned.

The Lieutenant came over with two men to investigate the sound of the scuffle.

"What's the ruckus about?" the Lieutenant asked, then noticed the body. "So, Slither got himself killed, eh?" He noticed the blood on Tarry and Lealynn.

"Yes, sir," Tarry said. "He attacked me, sir."

"I knew one of you two would die sooner or later," the Lieutenant said. "Who did him in?"

"I did," Lealynn said.

The officer scrutinized her.

"Why did you do it?" he asked.

"He attacked Tarry. Would you let someone be murdered?" she asked.

The Lieutenant chuckled. "We can overlook this if we can keep his equipment. Or you can challenge me for it?" he said.

Lealynn's knife hovered out of Slither into her hand on its own. The hilt glowed green.

"That won't be necessary," Tarry said and put his hand on Lealynn's shoulder. She released the knife and it sheathed itself.

The Lieutenant ordered his men to bury Slither and confiscated his belongings.

"Your girl is braver than you," a raider chuckled.

After they left, Tarry cried.

"I'm sorry you had to do that," he said. "No one should have to see that."

Lealynn showed him her wrist. A blue teardrop displayed next to the man-shaped tattoo. "I didn't want to do that either, but I couldn't let anything happen to you."

They both wept quietly as the sun set. Even with her knife, Lealynn feared her captors. She felt better after Tarry prayed with her.

In the morning, they set off. The biting flies tormented them relentlessly. Slither's empty mount reminded Lealynn of the attack.

Tarry's yarnback stepped in a hole and they heard a sharp crack. Lealynn launched off the mount into a clump of grass, which sliced her right arm and face.

"What was that?" the Lieutenant asked as the column stopped.

Tarry dismounted and looked at his beast.

"She broke her leg, sir," Tarry said. He wiped a teardrop from his eye.

"You have a spear," the Lieutenant said. "Use it and leave the body behind."

"I think It can mend," Tarry said.

"We don't have time to wait for it," the officer said.

"Then I'll stay here on my own," Tarry said.

"If you stay, you'll lose our protection. You won't be allowed to return to the city," the Lieutenant warned.

"Then I resign, sir," Tarry said as he ripped off his tribal clasp and handed it to his commander.

"You can't just leave, you have to buy out your contract," the Lieutenant said.

"I'll give you my weapons," Tarry said.

"I'll settle for the girl," the Lieutenant said.

"No, I'll only offer the weapons," Tarry insisted.

The Lieutenant confiscated Tarry's knife, sword, and spears.

"Give me her knife too," the Lieutenant demanded.

"It's her knife," Tarry said.

"She's a slave," the officer said.

"I freed her," Tarry said. "She helped me bring down your bison."

Lealynn's hand hovered over her knife.

"Fine," the Lieutenant grunted. "I'll keep your share of the meat and call it even. I don't want to see you again."

They rode off. Tarry and Lealynn coughed in the cloud of dust.

"Do you really think it's mendable?" Lealynn asked.

"I doubt it, but I will try," Tarry said.

"The wolves will be attracted by the blood," Lealynn said.

"I hope your knife is thirsty. It's better for us to die fighting wolves than to stay enslaved to them forever. I did this so I can help you get back to your people," Tarry said.

"But, they said you won't be allowed to return to your home," she said.

"My only home is up there," Tarry pointed to Paradise.

The yarny rested on its knees and moaned. Jagged bone protruded from its right fore-leg. Tarry petted her neck and tried to comfort her.

They dug around the leg, but couldn't lift the beast out of the hole. Flies swarmed around the wound and landed on it in blankets of pain. After working for an hour, Tarry gave up.

"May I please have your knife?" he asked Lealynn.

"No," she said. "Only I can touch it without getting burnt. It only responds to one master at a time."

"Then, please kill my yarny," Tarry asked. "I'll hold her head still. I don't want to see her suffer anymore."

"Are you sure?" Lealynn asked.

"I can't do anything for her," Tarry threw up his hands. "We have to get away from the carcass before the wolves find it or we'll die."

Lealynn put her hand on his shoulder. Her knife handle glowed blue and green.

The gleaming blade slowly approached the faithful beast's neck. The yarny moaned and looked her in the eye as she prepared to end its life.

"I am so sorry," she said. "You deserved better."

She steeled herself for the killing blow.

"Stop!" I boomed from behind them.

They turned. My blend of human and toad attributes surprised them, because halflings were always killed at birth.

"Who are you?" Tarry asked and stepped in front of Lealynn.

"I am Crystal Comfort," I said. "Your world's steward sent me to grant you one wish, Tarry."

"I wish her leg was healed," he said.

"I will take care of her," I promised.

I examined the beast and pitied it. As I spun silk around its leg, I sang to it in the Darkish tongue. The music soothed the animal as I worked.

"Why were you sent?" Tarry asked.

"You've done something no one else has done for hundreds of years," I said. "You've sacrificed for a human."

"I only saved one," Tarry said. "If I were braver, I could have saved more."

"Just save this one," I said. "It seems small, but I believe your actions will lead to much good."

I returned to my singing and wrapped the yarny's leg, while Tarry nursed Lealynn's wounds.

"Take her to her uncle's house," I said. "My light will shine over it to guide you."

The yarny ambled over to its master without any trace of a limp.

"How?" Tarry asked.

"Travel straight toward that mountain for three days and you will reach her uncle's territory," I smiled and disappeared in a flash of grey light.

After three days of riding, hunger gnawed at them. Despite being alone, Tarry respected Lealynn and they slept on opposite sides of the yarny.

At the border of her uncle's territory, a group of bird-riders met them. Their arrows thirsted for Tarry's blood.

"Peace," Lealynn called. "I am your kin and we mean no harm to you."

"Prove you are one of us," the leader said.

She raised her sheathed knife, which hovered above her fingertips.

The men dismounted and bowed to her.

"I am Lady Lealynn, daughter of Rand, granddaughter of Benjamin. I seek my uncle, Lord Rend," she proclaimed.

"Who is he," the leader asked and pointed his spear at Tarry.

"He is my friend, Tarry," she said. "He is unarmed and under my protection."

"It is wiser to leave him and his beast behind," the leader said.

"So he can be killed when I am not here to protect him? He stays with me," she said.

Lealynn dismounted from Tarry's beast and rode the leader's bird. He guided it for her and Tarry rode immediately behind.

The men remounted and escorted them through palisades toward her uncle's homestead.

Forty men formed a wall of spears outside Lord Rend's home.

Lealynn dismounted and told them to stand down.

"They will not stand down," Lord Rend said. "Who dares defile my land with a yarnback?"

"I brought it," Lealynn said.

"Why?" Rend asked.

"It carried me to freedom. When my village was razed," Lealynn explained. "Tarry rescued me."

Her uncle raised his bow and placed an arrow into the yarny's throat. Tarry tumbled off the dying beast.

"How could you?" Lealynn shouted through angry tears.

"I will do what I must to protect my people," Lord Rend said.

"Tarry and his beast are not threats," she shouted. "They saved my life."

"I'm sure Tarry asked you for favors in return. Will I be great-uncle to a halfling?" Rend asked.

"What?" Lealynn asked. "Tarry didn't ask for anything from me. He protected me from men like that."

"Arrest him," Lord Rend ordered.

"No," she reached for her knife. "He hasn't done anything wrong."

"Even if that is so," Rend said. "He is a toad and must be hanged for trespassing on our territory."

"He is under my protection," Lealynn said.

"You are still young," Rend said. "You will learn in time what it takes to lead men."

"You have a red tattoo," Lealynn said. "A red yarny marks your left bicep."

"What do you mean?" Lord Rend asked and confirmed its presence on his bicep.

"You killed the yarny without cause," Lealynn accused. "Will you add a man's blood to your conscience?"

Rend's knife glowed a pale red.

"If it keeps my people safe, then yes," Rend admitted.

As Lord Rend approached the bound Tarry, Lealynn shoved him out of the way. She threw her arms around Tarry and kissed him.

"We are betrothed," she said. "You cannot kill my betrothed husband."

"Get off of him," Rend dragged her away.

"You accused him of using me, which he has not done, because he is honorable. He gave up everything he owns to save my life. He is banished from his city for helping me," she said. "He did not expect anything as payment, but I will pay him back. You mentioned halfling children. We will have them because we will be married."

"Do you accept her betrothal offer?" Lord Rend asked Tarry.

"Yes, sir," Tarry said. "I will be a faithful husband."

"No, you will die," Rend said as he drew his knife. "I will not have halfling kin."

"I challenge you, uncle," Lealynn shouted.

Her uncle turned to her. Since she challenged him in front of his men, he could not back down.

"Are you sure you want to do this?" he asked.

"We don't have to if you allow Tarry and I to leave safely," she said. "I do not want a green tattoo of your silhouette."

"I accept your challenge," he said. "It is evil to marry a toad."

"I really love you, uncle," she said as tears streamed down her face. "I cannot let you murder Tarry."

Lord Rend's men encircled the duelists. A warden explained the rules. When they drew their knives, the blades would hover before them. His or her own blade would kill the one in the wrong.

Sweat formed on Rend's forehead, but he told the warden to count off anyway.

One

Lealynn's aunt rushed to the edge of the circle.

Two

A soldier held her back.

Three

They both drew. Lealynn's knife hovered before her chest and pointed its blade at Lord Rend. The vibrant, green glow vindicated her.

At the same time, her uncle's blade turned toward him. Red light glinted in his terrified eyes. As the knife plunged toward his chest, Tarry rushed forward and clenched the handle.

Tarry screamed as the hilt scalded his hands. His muscles strained against its otherness as he wrestled it. After twenty seconds, he stabbed it into its sheath.

Tarry rolled on the ground and grimaced. Lealynn stooped and cut his bonds.

"Thank you," she said.

She held him upright and kissed him. He held his hands away from her because it hurt to touch anything.

Lealynn's uncle trembled. A new tattoo of a red, sheathed knife decorated his forehead. He could never draw his blade again. It would remain entombed in its sheath until his heir wielded it.

"Why did you save me?" he asked as he knelt before Tarry.

Lealynn's aunt rushed to his side.

"I don't like death," Tarry replied as two soldiers dressed the wounds on his hands.

"What am I going to do?" Rend asked. "No one will respect me now that I have the Mark of the Sheathed Blade."

"Uncle, it isn't the knife that makes you fit to lead. It is your choices," Lealynn said. "Your people are loyal. You lost your way, but I will help you find it again," she stretched out her hand and raised him to his feet.

"What say you, men?" she asked. "Will you still serve your clan leader?"

"Aye," the soldiers said in unison.

"You have seen today that a toad saved the life of Lord Rend, even though he didn't act very neighborly," Lealynn said. "Toads are people just like us. We are capable of the same cruelty or the same nobility. Today starts a new era. Peace is possible with toads as long as we are willing to work at it."

Tarry rose with the support of two soldiers.

"I am sad about my beast. She was true and kind," Tarry said. "But, I offer forgiveness to you. I haven't been anything but kind to your niece and I swear that I will love her truly."

"I accept your forgiveness," the humbled Lord Rend said after his wife whispered in his ear.

When I materialized in a flash of grey light, soldiers gasped at the presence of a halfling. I had a bulbous face with grey eyes and curly hair. My delicate, webbed hands smoothed my dress over a well-proportioned frame. Lealynn beamed as she realized that her children would look like me.

"Do not be alarmed," I said. "I am Crystal Comfort. I came to grant a wish to Lady Lealynn, in an unexpected way. She has stayed true to her beloved, though it could have cost her everything. What do you wish Lady Lealynn?"

"I wish for Lord Rend to be freed from the Mark of the Sheathed Blade," she said.

"Yes, please heal me," he said.

"You will be healed, but the cost is your pride," I said. "You must use your influence to bring as much peace as you can to this land [^2]."

Lealynn's aunt embraced him as tears streaked down her face. I surrounded them in a cloud of grey light that swirled around the couple like a tornado. As I did, silk wound them in a cocoon.

"Come out now," I ordered.

A green glow came from inside the cocoon as Lord Rend cut his way out. The Mark of the Sheathed Blade had disappeared from his forehead. He held his knife in his free hand.

Lealynn's aunt looked into her husband's eyes. He was now a halfling with tusks and enormous muscles. His skin remained the same color, with the same tattoos of his kills. Despite his new form, Lealynn's aunt admired him. In the transformation, they both became twenty years old again.

"I realize changing his form affects you too," I said to her. "I am sorry it had to be this way, but the Steward commissioned me to bring peace to this world. I hope the youth I gave you will make up for the change."

"Aren't you going to make me a halfling too," she asked.

"Is that something you wish for?" I asked.

"No, she's perfect the way she is," Lord Rend said.

"Is that how you feel?" I asked.

"I am content," she said. "I am not upset that you have changed him. Will we have children again?"

"That is why I made you young again," I confessed. "Your family will bring peace to this region. Your steward always intended for toads and humans to dwell in harmony."

"Thank you," Lord Rend said. "We will accomplish our mission."

He buried his lips in his bride's. His men cheered at the miracle.

"What will happen to Tarry's hands?" Lealynn asked.

"Come here, Tarry," I ordered. "Hold out your hands."

I removed his bandages and wrapped my silk around his hands and the burns healed, with scars the same color as Lealynn's skin.

"Thank you, ma'am," he said.

While Tarry and Lealynn waited the customary six months of betrothal to marry, I visited their people as often as I could. I healed anyone with injuries by replacing the injured part with a toad part. A human with one arm left with a new, toad arm.

Because of the healings I performed, the soldiers accepted my claim that I represented the Steward. Despite their leader's change in appearance, most of his men still accepted his leadership. They rebuilt and fortified Lealynn's village. Toads who wanted relief from endless war filtered into the village and became residents after they proved their peaceful intentions.

At the end of six months, Lealynn and Tarry married. I wove a wedding dress for Lealynn. Everyone marveled at her perfect gown. When Tarry lifted the veil, he relished her lips, which he had sacrificed so much for.

Lealynn ruled her village with fairness and compassion. Lealynn and her uncle formed a buffer between the warring humans and toads and established trade with both sides.

Some humans and toads never learned to trust one another, but many humans married the toad refugees.

Tarry and Lealynn named their first daughter after me, just like many other couples I have met. The beauty and hunting ability of their children became legendary. Peace reigned in their region of the world.

[^1]: Proverbs 12:10
[^2]: Romans 12:18
