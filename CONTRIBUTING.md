<!--

SPDX-FileCopyrightText: 2020 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Contributing

You can contribute to this project in several ways.

* Finding typographical errors in wiki articles or stories.
* Filing "bugs" for plot holes.
* Writing original stories set in the story world in the Markdown format.
* Writing outlines for original stories for others to build on.
* Spreading the word about the WNV Universe.

Please ensure that all submitted files follow the [REUSE](https://reuse.software/) standard for licensing.
