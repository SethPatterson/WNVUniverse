<!--

SPDX-FileCopyrightText: 2022 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# The Last Bridge

![The Last Bridge Cover](CoverArt/LastBridge-Cover.png)

Roars filtered out of the jungle. Despite the humidity, he shivered. Soldiers fled past him. They hurled aside their weapons in their frenzy.

The roars grew louder. The last bridge swayed in the wind. Fugitives crowded it. The ropes strained.

The noise did not bother Earl. Earl heard nothing, but he felt the rumble, the low-frequency throb of primal fear.

Leaves trembled as the first monster, a Pentapod, revealed its tentacle. Villagers shouted at him. If he stayed to fight, he would not survive. His deaf ears heard no calls to return to the village.

They never loved him. His people mocked relentlessly, even now. He loved them.

As he stooped to retrieve a discarded rifle, a Pentapod emerged fully. He pressed the safety off and brought the rifle to his shoulder. When it kicked, he dropped the gun.

The Pentapods advanced. Their oily skin writhed around their five tentacles. Claws flexed in and out of their "hands".

Earl fumbled with the rifle, trying to copy what he saw the soldiers do. The bolt slid backward. Brass glinted in the setting sun. He forced the bolt forward and aimed again.

He missed his second and third shots.

A tentacle curled around the rifle and jerked it from his hand. He drew a borrowed revolver and blasted its middle. It lurched away with purple slime spewing everywhere.

A second Pentapod slashed Earl's face with its claws and tasted more lead. The rest of the revolver emptied into the two Pentapods.

The few soldiers "brave" enough to watch Earl's lonely fight shouted warnings at him. He did not hear them. He seized the rifle again.

He clutched the rifle too tightly and sent bullets in the general direction of his foes. After he fired his last round, he resigned himself to death.

He drew a knife and hacked at the bridge's rope. If he could isolate the island-cliff, his people would be safe. Tentacles jerked at his limbs. Claws lacerated his flesh, but he slashed at the bridge. The soldiers at the other side had the same idea and started cutting.

Earl's blade slipped from his hand and plunged into the abyss. He gnawed at the ropes, praying for his peoples' salvation.

Pentapods hurled him into a boulder, then surged past him into the village. His skull fractured. He blacked out. The bridge stood.

When he awoke, smoke blocked out the morning sun. Death hung in the air thicker than the soot. Enemies stooped over his broken body, drunk with the memories stolen from the brains of the entire village.

They bickered in their booming language. The ground shook from their voices.

"I am the leader," one yelled. "I own the right to the only brave one's essence."

"It wasn't your brothers that he killed. I claim him," another shouted with extended claws.

"I claim him," I said in the weak voice of a female Pentapod. "I will fight for the freedom of the only brave man."

The troop guffawed. They had never seen me before, since I teleported to their world that day, but they hated me anyway.

With my body, half the size of theirs, I had little chance of victory. The Steward of this world prevented me from using my powers to fight.

I dodged the thrust of the first attacker. From behind, two more enemies pinned me to the ground. Spikes approached my middle. As a woman, I did something they could not. I inked.

My inky toxin numbed their muscles for a few seconds. During my reprieve, I skewered one of them. The other two recovered and rolled me onto my back. As they approached with their memory-probes, Earl staggered between me and them.

I folded my body around his and probes plunged into me. They missed my core, but the pain tormented me. Two of my tentacles died.

I shielded the man and pleaded for his life. With my telepathy, I probed his mind. Hitting the boulder had erased his memories.

"He is too brave to die." I purred. "Please, I beg you, spare him. He was the only person who fought to save his village. Think of the memories of his people, memories you stole."

"His people hated him," the leader said.

"For no reason. He was deaf. That is no sin." [^1]

"Those born with bodily imperfections deserve to die," an elder said.

"He hit his head. His mind won't taste very good. He has amnesia."

"You're lying," the leader said.

While I stalled them, healing coursed into his body. I hoped the Steward would not punish me.

I unfurled and beneath, a young Pentapod appeared.

"He is  mine," I said. "I claim him as a son."

Despite their wickedness, they would not kill their own. I prayed they would realize all people are equally valuable whether they have tentacles or arms.

When I woke from my dream, I wiped sweat off my forehead. My nightmare had seemed so real. The carnage of the village stuck in my mind.

Now in my natural form, I stuck my head between my legs. No one in my village understood me. As I sobbed, rumbling filled my room. At the end of the bed, a Pentapod rumbled.

I held my new, real, son close.

"You were worth the pain," I whispered to him.

[^1]: John 9:1-3
