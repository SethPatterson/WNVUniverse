<!--
SPDX-FileCopyrightText: 2021 Seth Patterson

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Invisible Vengeance

1. A fragment of WNV visits an industrialized world.
2. While she is there, known criminals and bullies are found strangled.
3. A sweet, polite teenager befriends WNV. She works at a grocery store and is trusted by everyone in town.
4. Murders continue. The victims include criminals, school bullies, adulterers, and drunks.
5. Even though the murders are shocking, most people are secretly happy about them because the victims "had it coming".
6. WNV writes a letter to the editor pleading for the murderer to surrender to the police.
7. The murderer is convinced that her actions are righteous because evil is being stopped.
8. The murderer can become invisible (except her eyes) and phase through solid matter, though she must still breathe.
9. The murderer decides to kill WNV because of her tattoos, which mark her as an evil person.
10. WNV uses pepper spray to fight off an attack from the murderer.
11. The teenager is the killer.
12. When WNV goes public with the news, no one believes her because of her dishonest face.
13. WNV is teleported off world.
14. The murders continue until the teenager dies of old age.
