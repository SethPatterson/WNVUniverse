<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Headache for Two

1. When Ruby is learning to use her powers, she sees a witch-queen sacrifice a good man in a pool of acid.
2. Ruby connects the man's soul to the queen to save his life.
3. The once-powerful queen gets a migraine and is shut up in her bower for weeks.
4. Ruby appears and tells the queen that the pain will only subside if she lets the man run her life.
5. The queen resists the man, but gradually learns to rely on his wisdom.
6. A coup forces her to flee.
7. In a remote village, she is hired to tend a sick, old man.
8. A witch offers to heal the old man and the queen must convince him to say no.
9. Bounty hunters capture the queen.
10. Her eyes are put out and she is forced to serve the new king as a waitress.
11. Ruby sees the queen's humility and repentance. She places a gray flower in each eye socket to heal the queen.
12. The queen and the man in her head are married.
13. The usurper dies of a head injury.
14. The queen reclaims her throne and purges magic from her kingdom.
15. The queen sees auras of a beautiful man.
16. In a migraine, the man is reembodied, looking like an aura, without the pain.
17. She kisses the new king and gives her throne to him.
