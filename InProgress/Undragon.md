<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Undragon

1. Sir Wilhelm duels dragon to the death as punishment for her murders and thefts.
2. Wizard cheats by shining light in dragon's eyes.
3. Knight refuses to kill dragon after he discovers cheating.
4. Wilhelm gives dragon healing drink, but collapses.l himself.
5. Dragon carries knight to dryads and forces them to heal him.
6. Dragon demands to marry Wilhelm in the Royal Chapel since she won the duel.
7. Princess Heidi is enfatuated with Wilhelm.
8. Dragon hides in secondary hoard. Looters steal her gold.
9. Wilhepm concinces dragon to give away gold so looters will not become dragons.
10. Dragon shrinks as hoard shrinks.
11. Dragon uses magic to become temporarily human, but is infertile in that form.
12. Wilhelm wants to wait for his love to make her human rather than using magic.
13. Heidi tries to allure Wilhelm.
14. Wilhelm runs away.
15. Dragon finds Heidi. Wilhelm refuses to let Heidi die, so dragon releases her.
16. Heidi forced to marry wizard.
17. Dragon rescues lost children and cattle.
18. King wants to kill dragon with his army.
19. Wilhelm becomes a dragon so his wife can become human. She looks like a female version of him.
20. King kills Wilhelm in cold blood.
21. Wizard turns Heidi into a dragon and sends her to kill Wilhelmina the Undragon.
22. Wilhelmina restores Heidi's humanity, which turns the wizard into a dragon.
23. Wizard eats king, then is killed by soldiers.
24. Heidi and the king's adopted heir, Franz, both want the throne. They meet with Wilhelmina negotiating.
25. Franz agrees to marry Heidi and name the wizard's child his heir if he can be king.
26. Child is born as a shapeshifting human/undragon. Franz treats him as his son.
27. Wilhelmina knighted and becomes Heidi's bodyguard.
28. Wooden ring, with vines, keeps Wilhelmina young until the child grows up.
29. Wilhelmina marries undragon.
