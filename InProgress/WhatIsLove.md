<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# What is Love

A woman was sent to gather information from a "hero". After a week of spying and living with him, she left because of her shame.

She tried to return her bounty, but her handler told her that her only way to stay alive was to confess to the "hero". Her handler placed a centipede at her brain stem to control her remotely.

A former rival spy/assassin had eloped with the "hero" because she was hired to become the mother of the chosen one. The man who hired her needed someone to be the mother of the villain, so the rival offered freedom from the centipede and protection in exchange for the task.

The desparate spy agreed and her centipede was replaced by another that telepathically bound her to her former rival.

The villain's father was kind to the spy and protected both the chosen one and villain.