<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Charlea's Big Day

1.  Sal ruins her sister's proposal by telling Jon that Charlea wants him to
    propose.
2.  Sal wants to find a present for Charlea to make up for her blunder. The
    wedding is in one week.
3.  Sal hears old women mention a magical flower that blooms every 50 years.
4.  Sal finds the flower after two days of searching.
5.  The Flower Keeper explains that the flower will keep Charlea from aging
    at all for one year.
6.  Charlea sniffs the flower, but must sniff it again the next day for the
    flower to work.
7.  Sal tells the rival about the flower.
8.  The rival sniffs it and turns into a copy of Charlea.
9.  When Charlea sniffs it the next day, nothing happens, which is
    unnoticeable.
10. Charlea gets a letter from her dying aunt and leaves without Sal to say
    goodbye.
11. The rival tells Sal she got a second letter saying the aunt is better.
12. The rival bites Sal in her sleep.
13. Sal finds out that the rival is fake because her cooking is terrible and
    she is mean.
14. Jon is away on a traditional fishing retreat.
15. Jon's family doesn't believe that the rival is a fake.
16. Sal asks Crystal for help, but is told to ask the Flower Keeper instead.
17. The Flower Keeper doesn't believe Sal, so they ride a giant moth to find
    the real Charlea as proof.
18. The moth's wing gets injured, so Sal and Charlea must race back before
    the wedding.
19. They arrive in the beginning of the ceremony.
20. Jon must decide who is the real fiancé.
21. Jon picks the right girl.
22. The Keeper wants to turn the rival into a bush as punishment, but Sal
    convinces him to turn her into a carving knife instead, so Sal can learn to
    carve a new body for her in time.
