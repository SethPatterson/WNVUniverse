<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# First Contact

A team specializes in first contact with powerful beings, hoping to prevent conflicts. Another team enjoys second contact and hopes to stimulate conflict.
