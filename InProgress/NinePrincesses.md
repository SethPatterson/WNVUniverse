<!--
SPDX-FileCopyrightText: 2021 Seth Patterson

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Nine Princesses

## Stars Announce Mission

A flash of light jerked me out of my light sleep. I snatched my dagger from my bedside and surveyed my surroundings. As my eyes adjusted to the light, four forms revealed themselves.

The stars always astonished me. Each one was frighteningly beautiful. It hurt to look at them because of their brilliance.

"Hello, Twilight," their leader, Kin, greeted me.

I sheathed my dagger.

"Hello," I said as I tried to quell the panic in my chest. Whenever they arrived, they brought a curse with them.

"How has life treated you in this town?" Kin asked.

"The people don't hate me yet," I replied. I twisted my straight, red hair through my fingers and tried not to cry. I felt so alone since I had to leave Desmond and Hard to protect them.

"We know you are missing your days when you had a mission, so we found one for you," Kin said.

She hovered nearer to me and stroked my cheek with her fingertips. My tears flowed.

"What do you want from me?" I tried to sound confident.

"We know how much you want to find a good man who will love you, so we decided to give you the opportunity to help other women fall in love. Seeing others be happy will surely fill the void you feel," Kin's sarcasm hurt.

"What exactly are you asking me to do?"

"It is simple," a male star said. "We have located nine women who have no hope of finding husbands in their own worlds. You will help all nine women find men."

"You left out a detail," I said.

"The detail," Kin said. "Is that all nine women will live in your mind until they are married. You will be able to give them physical forms during the day, but each night, they will return to your mind. The only way they can be free is if they are married."

"That's cruel to them," I protested. "What if I can't find a husband for them?"

"Then, I suggest you get used to having another sad, lonely mind inside your head."

"I am willing to help them, but please do not put them inside my mind. I won't be party to your cruelty."

"You can let us put them in your mind or we can find another host who will not be as sympathetic as you, perhaps they would enjoy spending time in Natia's mind."

"I will help them, but only if it is voluntary on their part." I said.

"Oh, they'll volunteer," Kin smiled.

I folded my clothes so I could place them in my knapsack, but an impatient star stuffed my belongings into the bag. I flicked a silver piece onto the night stand as payment for the hotel room just before the stars whisked me away to another world.

## Evynne and River

After the starlight faded, I found myself in foothills. I sniffed the air. The reek-nettle alerted me to where I was. I had visited this world before. I scanned the area around me, but saw no threats.

One of the male stars still held my knapsack and pistol. I wanted my belongings back, but dared not anger the stars by asking for them. Bad things happened to people who angered the stars.

"Who are we here to visit?" I asked. "An octophant or a feather-wolf?"

"Both," Kin answered. "Us stars will be invisible to the people here, but they will be expecting you."

"Let me guess, you already visited while disguised as me."

"Yes. Just say and do what we tell you."

"That depends on what you want me to do."

"You'll tell them that you are a world-traveler who helps eligible young women find honorable husbands. You have never failed to make a suitable match."

"I have never made a match. This is my first job. I don't tell half-truths."

"The first girl's name is Evynne. She volunteered."

I nodded and suddenly appeared in the center of a village 10 kilometers away from where I was before.

A chieftain and his family stood before me. They could not see the stars.

"Greetings, Twilight," the chieftain said. "Everything is prepared for Evynne. She is ready to depart with you to find a husband."

Evynne clutched an overstuffed bag of clothes. She wore no jewelry like her siblings did.

"Are you sure that you want to go with me?" I asked Evynne.

"She is sure," her father said.

"I did not ask you, sir," I said.

"Yes, Twilight. I am willing to go. If I can marry well, perhaps I can bring honor to my family."

"Yeah, then she can make up for failing her hunt," her older brother said.

"Is anyone forcing you to do this?" I asked.

"We want her out of here," her eight year old brother said. "She shames our family."

"Please, just take me away from here," Evynne pleaded.

I absorbed her and her belongings into my mind in cloud of twilight. Her mind slumbered.

The stars transported me 53 kilometers to an octophant village. A similar scene greeted me. A thin octophant stood in the center of the village with a pack at her feet. Her family said nothing as she stared at me. The stars had transformed me into one of her kind. My dress of starlight dazzled the octophants.

"Hello," I said. "Are you the girl who wants my help to find a husband?"

"Yes, don't you remember me, Twilight? You promised that I could go with you last week. Am I still allowed to come?"

"Of course you can come," I said.

The girl's family members hugged her intensely, because they did not know if they would ever see her again.

"Tell her, it's time to go," Kin whispered to me, in my giant ear. "Her name is River, by the way."

"River, it is time for us to go, if you are sure you really want to."

River tore herself away from her grandmother and turned to the proud. "I know I failed the strength trials, but I will make you proud. I will find a good man and we will have children who will not fail the trials."

She took a deep breath through her trunk and I absorbed her into my mind. I placed her sleeping mind next to Evynne's.

## Arrow

In another flash, I was in a moist, tropical world. Night animals wailed and chirped. The stars hustled me along toward a ring of torches.

Guards with bamboo spears and hatchets blocked our way.

"Halt!" one of them demanded. "State your business."

"I am Twilight," I said. "I came to see..."

"Arrow," Kin whispered in my ear.

"I came to see Arrow," I said.

"Prove you're Twilight," the other guard ordered.

Gray flames flared around my shoulders and hair. "Is that sufficient proof?" I asked.

"How did you do that?" he asked.

"The power is a gift from my family," I said. "May I please see Arrow?"

"Of course, your worship," the first guard said. "I'll take you to Arrow right away."

I followed him into the center of the village, where a cluster of guards with spears in their hands eyed me. A pangolin woman stood in front of a door and scowled at me.

"Who are you?" the native girl asked, with both pairs of arms crossed.

"I am Twilight," I said.

"My parents told me about you. From their description, I expected you to look more special," she said.

I blushed and twilight spilled from my cheeks.

"Showing off," she huffed under her breath. "Let's just hope the man you find me is better looking than you."

"Are you ready to go?" I asked. "Is there anyone you need to say goodbye to?"

"I already said goodbye to my parents and I don't think anyone else will have the sense to miss me. They say I act like I'm too good for them."

I absorbed her and her baggage into my mind before she could brag anymore. With her asleep in my mind, I had a break from her hubris.

Kin made me disappear before the guards could ask why Arrow disappeared.

## CCH-32

In a world that smelled like motor oil and mint, I pushed through the foliage into the blue sunshine. Robot-like people worked in their fields or repaired farming equipment. They ignored us as we passed.

A small, metallic rodent bumped into my leg, then scurried up a tree. It chattered in a friendly way like it expected me to give it a treat. As we walked toward a city a kilometer away, I passed a cart with no animal attached to it.

"What's the matter?" I asked its driver.

"My ox bolted. I need to get my goods to market, but I suppose I will need to write this off as a loss."

Kin eyed me in annoyance. The chattering animal wiggled around my ankles. "If you had an animal now, do you think you could make it to the market in time?" I asked.

"There is an 83.2% chance if you only take into account the 20 most important variables," he replied.

I scooped up the rodent from the ground and spoke to it.

"Do you want to go to market with this nice farmer?" I asked.

It chirped in delight.

I placed it on top of a mound of dirt beside the road and increased its size so it was large enough to pull the cart. It parked itself in front of the vehicle so it could be hitched.

"That is not possible," the driver said.

"I used the dirt to add mass to the animal," I smiled. "It makes sense."

"How can I thank you?" he asked.

"May I ride in your cart?" I asked.

"Of course," he replied.

I climbed into the back and tried to listen to him, but I fell asleep in the blue sunshine atop a bundle of homemade rugs.

When we reached the village gate, the ox was waiting in a stall with a policeman guarding it. The little rodent shrunk and left a pile of dirt behind itself when the farmer wasn't looking.

A guard waved me through the gate as the farmer reclaimed his ox. I smiled about the story he would tell his kids when he got home.

Fire danced in Kin's hair. She hated it when I helped mortals.

"It got me to the city faster," I said before she could ask why I did it. "I needed to rest."

"You're always so paranoid," she said. "Why did you fall asleep here?"

"The farmer has an honest aura," I replied. "And I've worked with the police in this region. They keep things safe."

In an affluent neighborhood, an herbdroid girl sunned herself in the blue light. Her leaves energized her.

Kin told me she was the right girl.

"Hello," I called to the herbdroid.

She looked up from her lawnchair with half-closed cameras.

"Hullo," she called. "We don't have many fleshlies around here. Did you come here recently?"

"Yes," I said. "I'm Twilight."

She ran to me and threw her arms around me. "Oh, I've been waiting for 372 hours exactly since you were last here. I'm so excited," she burst into tears, or animated tears drawn on screens on her cheeks.

"What is wrong?" I asked.

"I'll miss my family," she said. "They won't miss me though."

"I know what it's like to miss my family," I said. "I'll do the best I can to help you to not feel lonely."

"I'll go grab my things," she bounded away and grabbed a wicker crate full of clothes, books, and pruning equipment.

I wondered why her emotions swung so wildly and quickly. As she checked her crate for anything she might have missed, I asked her for her name.

"My designation is CCH-32. I'm sorry I reacted strangely," she growled. "My emotional programming is incorrect and the bit-doctors haven't been able to find the bugs."

"I'm not annoyed." I said. "I'm here to help you."

"Everyone else is annoyed. That is why they dislike me. Even though their emotional programing is within one standard deviation of normal, I am enough to irk them."

To the stars' annoyance, CCH-32's parents emerged from their house and alerted their neighbors to our presence. An enterouge formed around us.

We paraded through the city into the marketplace and out to the gate. People threw flowers at CCH-32's feet even though they were secretly relieved that she was leaving.

Once outside of the city gate, her parents and siblings kissed her goodbye and we disappeared in a burst of starlight.

## Grace

## Julie

## Anfisa

## Haiya

## Banou

## Gathering the Princesses

The "princesses" are all unsuccessful members of their societies. When each princess is gathered into WNV's mind, she gives each of them one positive attribute of their choosing. WNV possesses the same attributes that she gives to the princesses, but no one recognizes them because of her tattoos.

Evynne is a feather-wolf. She accepted the star's offer to travel to a new world to find a husband because her society rejected her for her lack of athletic prowess. She wished for the positive trait of agility.

Banou, a green-blue creature made of gel, did not have a choice in entering WNV's mind. The stars chose her with her family's permission. Banou's arrogance was so great that no one could stand to be around her. She chose beauty as her positive trait even though she already possessed vibrant colors.

Julie, a songrat, accepted the stars' offer because she was mute and knew it was the only way she could be healed. She wished for the positive attribute of musicality.

CCH-32 is a plant-robot. The programming that allows her to express emotions is flawed. She is much more emotional than most humans. Her people hated her for her volatility. She wished for the positive attribute of intelligence.

River is a tentacled elephant. She was considered too weak by her people. She despises Evynne because their peoples have been at war for centuries. She wished for the positive trait of strength.

Arrow is a pangolin. Her village was relieved when she entered WNV's mind because her impetuousness and arrogance tormented all of them. She hated her pride too, so she wished for humility.

Anfisa is half-merchant, half-moose. Phyla, WNV's adoptive mother, is Anfisa's half-sister. Her people despised her because they hated her father. She entered WNV's mind so she could venture out and establish a trade-post for the merchants. She and WNV enjoy being able to talk together in the merchant-tongue, a language which is impossible for outsiders to learn. Her positive trait is industriousness.

Grace is a Peytonite, a combination of wolf, deer, and human. When she marries, she and her husband will become mirror-images of each other. She wished for the positive trait of loyalty.

Haiya is a tapir who was raised by Silkwings. She was a child when chosen to enter WNV's mind, but during the contest, she went through her metamorphosis into adulthood.

## Establishing Rules

The princesses are allowed to leave WNV's mind and become physical only during the day. They can bring physical item's into WNV's mind, but cannot bring imaginary items into the real world.

## The Contest

There is a prince who lives in a society that requires him to choose one of nine princesses as his bride before his 21st birthday. The stars chose to give WNV charge over nine princesses specifically for this contest.

The contest lasts for nine weeks. The prince, nine princesses, and a chaperon (WNV) will live together in a palace. Guards patrol the palace grounds, but do not interfere with the contest. Each week, the prince will choose one princess to be eliminated from the competition.

Since there are no servants, all the princesses must work together on chores, cooking, and other tasks. WNV performs all of the translating for the princesses because they speak different languages than the prince.

Once a girl is rejected by the prince, she is not allowed to leave WNV's mind until after the contest is over.

## The Introduction

The Prince is a capable military officer who started as a private. He worked his way up through the ranks instead of accepting a position based solely on his high-birth. He planned to start officer-school a year after his wedding.

He carries a silver-plated revolver everywhere he goes and dresses in a smart, green uniform. All of the princesses and WNV are taken by his politeness, intelligence, and humility.

## The First to Go

The robot is the first to be eliminated. Although she is brilliant and worked hard to learn his language, the prince is concerned by her emotional instability. He feels terrible for sending her away, but knows his parents expect him to complete the contest. He wished he could have chosen a girl of his own kingdom to save the princesses from heartbreak.

A few days into the contest, Haiya weaves herself into a cocoon. She was a child when she entered the contest and must complete her transition into adulthood. The prince and princesses are dismayed when she enters her cocoon, but WNV knows it is a normal life-event for a Silkwing.

## The Second to Go

The songrat is second to go. She lives for music. Although the Prince enjoys music, he does not want every conversation for the rest of his life to be about it.

## The Third to Go

The Prince cannot stand Evynne's constant bickering with the elephant. He fears that she will make new enemies whom she is just as virulently opposed to. The elephant's personality resembles his mother's, which would cause no end of trouble with Evynne.

Although the prince found the Peytonite the least attractive of the princesses, he enjoyed talking with her. He befriended her and asked her for advice in how to navigate the contest. He made it clear to her that he did not love her and asked her to play the role of an advisor for him. Although she was hurt, she agreed. She hoped she could win him over despite his lack of interest in her.

Haiya hatches from her cocoon and is infatuated with the prince when she emerges. She does her best to win him.

## The Fourth to Go

The prince does not dislike or like Haiya. He chooses to eliminate her from the competition. She is distraught because she does not feel like he gave her a chance.

The prince has already determined which woman he wants, WNV. He tries to spend more and more time with her in the garden without her translating for other women. She senses what is happening, but secretly enjoys it.

## The Fifth to Go

To put an end to her interference in the competition, WNV tells the prince that she cannot allow him to continue his wooing of her. It is a violation of the contest's rules. He agrees to her request but secretly searches for a loophole so he can marry her.

The prince eliminates River from the competition because she gloats too much over Evynne being removed from the competition. WNV tried to warn her, but she did not listen.

## The Sixth to Go

The Prince still does not understand that WNV cannot accept his advances, so she shows him her tattoos. After seeing her tattoos, he has difficulty trusting her as a translator. She intentionally kept her tattoo that marks her as a liar hidden, so he does not completely distrust her. All romantic thoughts toward her vanished when he saw her face.

He finally eliminated the Peytonite from the competition when he realized that he could love any of the other three remaining girls. The Peytonite was upset at his rejection, but promised to remain friends if he desired it.

## The Seventh to Go

Although the blob was arrogant for most of the contest and teased the other girls, she recognized her pride and wished to repent of it. Since she knew that either the Merchant or pangolin was a worthier candidate, she used her shape-shifting ability to look like them. With tasteful clothing and jewelry, she demonstrated the beauty that the other girls had kept hidden.

The prince eliminated her from the competition because he realized how beautiful the remaining candidates were. He had rarely seen the Merchant without an apron or work-clothes and the pangolin had always worn heavy fabric that hid her form.

## The Eighth to Go

The prince decided to eliminate the Merchant from the contest. He found both her and the pangolin to have equally attractive personalities and character traits. However, the pangolin, with her sparkling jewels and shape to be more beautiful.

When he proposed to the pangolin, she presented the traditional three bronze knuckles. Since her planet was such a harsh environment, engagement rings were replaced with defensive weapons. As she presented the knuckles, the prince grew an extra pair of arms to wear the knuckles and match her.

They were married with the blob, merchant, and Peytonite as bridesmaids. The silkwing and WNV helped the royal tailors to create the traditional light-blue wedding dress for the pangolin.
