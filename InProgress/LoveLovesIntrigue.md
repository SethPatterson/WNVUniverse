<!--
SPDX-FileCopyrightText: 2022 Seth Patterson

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Love Loves Intrigue

A prince and princess's brother, who was the king, had a secret child. After the king died, his brother took the throne.

The secret child died. The child's aunt and uncle had tried to raise it, but sickness struck the home.

The new king searched for the secret child, but only found its aunt and uncle. He felt pity for them since they were poor and had done everything they could for his nephew.

He ordered the girl into a dungeon and drafted her brother. He said the only way the girl could be freed was if her brother became an officer and war hero. After her brother left for boot camp, he released the girl from the dungeon and had her trained as a personal guard for the princess.

Both siblings excelled at their jobs. The young soldier was promoted quickly because of his drive and competence. He believed his sister had died in the dungeons, so he planned to infiltrate into the palace while on leave to confront the king.

His sister caught him trying to sneak in and told him to leave because she was perfectly happy where she was. She told the princess about her brother's visit.

Once her brother was back on the front lines, the princess sent one of her guards with a message for him. She kissed a device that looked like brass knuckles and enclosed it in a box with instructions for its use.

Her guard was to find the solder and press the device to his forehead to create a mental bond between him and the princess. The guard was captured by enemy soldiers who pressed the device to her forehead instead.

The princess now had a permanent bond with her guard. The device and message, which the guard carried, were sent to the enemy king. He summoned the guard, who promised to explain her mission if the soldier were brought to the palace as well.

Once the soldier was brought from the prisoner of war camp, the guard explained that her mission was to visit the soldier and connect the princess's mind to his as extra encouragement to perform well. After he had made a name for himself, the princess would marry him.

The enemy king, who had risen to power within the last month, tired of the war, which his people started. The guard, who represented the princess, offered a way to end the war.

If the soldier were willing to renounce his claim to the princess, she would marry the enemy king to form an alliance. The soldier chose to allow the princess to marry the enemy king.

The king immediately ordered a withdrawal and started negotiating a cease-fire and exchange of all prisoners. The soldier and guard delivered his terms to their king, who readily accepted them after the princess explained the plan.

Some generals opposed ending the war and tried to assassinate the king, but the secret child's aunt saved the king. Because of her heroics, the king was finally able to court her. He had put her and her brother in positions where they could demonstrate heroism, so they could rise in social station enough for the king and princess to marry them.

Peace was restored between the two kingdoms with the princess and enemy king's marriage. The soldier married the guard who had sought to deliver the message to him. The king married the soldier's sister.
