<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

## Rosie

Dear Sister Spring,

I would like a new flute.

How do you keep the flowers in your hair so nice? The daisies in my hair always wilt.

Love,

Rosie

***

Dear Rosie,

I love your name. Yellow roses are my favorite kind.

If you ask a dryad, he or she can teach you how to make a flute. The art we make with our own hands is often the most satisfying. Through art, we can create like the Maker did.

I am a dryad, so the flowers grow in my hair naturally. Enclosed is a dried petal from my hair. If you are ever hurt badly, add water to it and it can heal your wound.

Sincerely,

Sister Spring

## Thomas

Sister Spring,

I don't believe in you. I think you're a scam for getting little kids to stop being bad and having fun.

It's all a lot of rot,

Thomas

***

Herr Thomas,

There are times when I do not believe in myself either. What is important is duty, not our feelings.

Everything would be rot without me. The Maker created me to serve the new life in this world, which is not always fun.

While you live for fun, you will always be a bad person. Joy only comes from serving the Maker and others.

Sincerely,

She who exists regardless of your doubts

## Elise

Dear Spring,

I like your picture. You are pretty.

Thank you for helping my garden. Blueberries are yummy.

Thank you,

Elise

***

Greetings Elise,

Thank you for calling me pretty. Do not forget that real beauty is in a gentle and quiet spirit. It starts in your heart and ends in your smile.

I like blueberries too. Don't forget that the Maker is who gives our gardens life.

Sincerely,

Sister Spring

## Johan

Dear Sister Spring,

How do you help all the plants in the world? The world is really big.

What should I give mommy for the New Leaf Festival?

Friendly words,

Johan

***

Dear Johan,

The Maker helps all the plants. I only need to help the plants in gardens and farms.

My secret is that I have many helpers. Anybody you see with a green thumb is one of my helpers. The Maker lets my piwer flow through them.

Freshly picked flowers should be a nice gift for your mommy.

Sincerely,

Sister Spring

## Bert and Angeline

Dear Sister Spring,

My sister has green thumbs, white hair, yellow eyes, and orange lips. Does that make her more special than me? I don't have any of those things.

I feel like my parents love her more than me.

From,

Bert

***

Dear Sister Spring,

I have green thumbs, so I like gardening. My brother, Bert, is sad. He thinks I am better than him even though he is so sweet and funny.

Please give him green thumbs too.

Your helper,

Angeline

***

Dear Bert and Angeline,

Angeline is blessed to be a helper to all of us. I am sure you can cheer him up with your love.

Bert, you are just as precious as your sister because you bear the Maker's mark on your heart.

Your special quest is to be your sister's helper. She is meant for great miracles, which you will witness. Stand by her side and you will be richly rewarded.

We sent this letter with a trusted Protecter, Garth the Fleet. Garth will help your parents to train the two of you for your quest.

Sincerely,

Sister Spring

Sister Summer

Brother Fall

Brother Winter

## Clive

Dear Sister Spring,

I am sad that the snow is melting. Can you ask your brother to make winter longer?

From,

Clive (Sledding Champ of Hollow Oak)

***

Greetings Clive,

I like sledding too, but my brother's turn at helping the world is over now. There is a season for everything.

I am sure there are other fun things to do in Hollow Oak. Look for them and learn to be content in any season.

Sincerely,

Sister Spring

P.S. Winter is my favorite season. It gives me time to plan and relax.
