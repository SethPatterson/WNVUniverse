<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWoethief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Woethief

![Woethief Cover](../CoverArt/Woethief-Cover.png)

<!-- Alt Text: A book cover shows a statue of a woman with six bat-ears, enormous bat-wings, and spider-legs. The statue is built into a cave wall and integrated into an underground mountain. Below the statue, bat-like people fly. To their left, a spider-like person holds a club over his head to strike. Next to him, a spider-woman spins silk. Next to her, a human man reaches over a cliff edge to hold the hand of a fairy-like woman. At the bottom of the page, a human woman, with curly hair and scarred skin, holds up her hands in a questioning stance. -->

![A map of the city of Ildylia](../Illustrations/IldyliaMap.png)

<!-- Alt Text: A map of the city of Ildylia. Natia's temple and the royal palaces are at the center. To the NorthEast are the arena, Stonecourt, and Gemma's house. To the East are Faithful's house and the Golden Death. To the South is the storehouse where Denrick worked. To the SouthWest, outside the city, is Den's hideout. -->

## 0 Introduction

The following document is an ear-witness account of the experiences of Nyla Woethief, World-Stewardess. Great pains were taken to verify the accuracy of the reported events and no contradictions to the facts were discovered.

Since this document may someday be read by offworlders, I will explain facts which are elementary to any Ildylian. Ildylia is entirely underground. We live in what offworlders, cursed with eyes, call darkness and navigate by echolocation. Steam from deep vents fogs our streets, making travel impossible at night for those who see by their ears. We are accustomed to mold and mildew, but we hate fire.

Three races, besides the occasional offworlder, inhabit our land. Bats fly and walk on all-fours. Spiders boast in their webs and venom. Ildylians, or Ilds, combine the best of bats and spiders. Ilds come in two varieties. Spider-bats have eight legs. Bat-spiders have two.

Centin and Natia ruled Ildylia as world-stewards, powerful beings tasked with caring for a world. They were married, but Centin did not let that stop him from marrying other women. Both styled themselves as gods, but stole their powers from Gemma and Woethief's parents.

Below them, King JaReel and Queen Syreeta, reigned over everything but the Forges. Because of an injury in Syreeta's youth, they could bear no children. JaReel loved her anyway. No marriage was ever as love-filled as theirs.

Nyla Woethief and Gemma Silver Nectarfang bear immense power. They're the rightful world-stewards of Ildylia, but Centin murdered their parents. After waiting thousands of years to hatch, Natia raised Gemma until she was fifteen, then turned her out into the streets. Gemma discovered Nyla and gave her a second personality, Woethief, and the ability to steal pain.

The following account describes what Nyla Woethief sacrificed to create a family. Her love recreated Stewardess Gemma Silver Nectarfang, who now reigns over half of Ildylia.

Long live Queen Syreeta,

Clay Truescribe - Royal Chronicler

## 1 WoeNyl Fights Ankor

We last died eighty-seven days earlier. As we clutched our baton, its silk-wrapped hilt grounded us in our environment.

*'We just need to win two more fights. I'll steady your nerves,'* Woethief, one of our two personalities thought. She stole Nyla's nervousness, so Nyla could concentrate.

*'Thank you, Woe,'* Nyla replied. *'We will be free soon.'*

As we entered the arena, booing erupted. Our hood obscured our face from the crowd's prying ears. As a woman leaned on his shoulder, an aura of mockery emanated from Ankor, our opponent. His spider body made him more stable than us, with our two, human legs.

**"Don't beat this hag too quickly. I like long fights,"** Flame thrummed into Ankor, her lover. Ildylians vibrate their hands, feet, and tips of their legs to speak. We "heard" her speech with our skin, but could not thrum with our extremities.

Despite her bluster, an aura of nervousness clung to Flame. She feared the day when Ankor's sport would get him hurt. At least normal people could not feel auras, so Flame and Ankor did not know our fear. The crowd's salacious shrieks followed Flame as she exited the platform. Each man wished she belonged to him. Nyla envied her beautiful wings.

After the King announced us combatants, we sipped water from the ceremonial cup. Our fingers traced mushrooms and webs, engraved on the rim. A courier took the chalice away on a silk pillow.

Rather than scrutinize our weaknesses, Ankor thrummed venomous words.

**"Will you murder me today, like you killed my soldier?"** To speak, Ankor's eight feet vibrated the ground.

We had killed one of Ankor's men in self-defense. We were exonerated by a judge, but Ankor held a grudge.

"We will not kill you," we emulated the vibrations of his language with our mouth, because we could not thrum like Ildylians.

**"Everyone knows who you are,"** Ankor said. **"Don't think you're fooling anyone. You're a murderess, and you can't get a man either. You're not pretty enough, nothing like my girl. She's a full-blood and you're a cursed unanimal."**

His words eroded our confidence. They stung because they were true. We were too plain to become Princess. No one expected a slave to progress in this contest, especially not a double-minded girl with our past. Men resented us. If we were beautiful, they might receive us. They might overlook our crimes. If they knew our full past, our hope of becoming the Princess would die.

**"Are the combatants ready?"** the King asked through the rumble machines. The noise interrupted our troubled thoughts and shook the arena.

**"Ready,"** Ankor replied smugly.

"Aye," we stammered. An orb of twilight hovered over our head, to illuminate the underground environment. Since they are eyeless, Ildylians cannot see the twilight that we were born with.

*'After two more fights, we will be heir to the throne. We will be free from our past, from Gemma, men, and Centin.'* Nyla thought.

Because Queen Syreeta was barren, the royal couple chose to adopt the winner of a tournament. Whoever defeated Ildylia's best fighters would deserve heirship. Nyla promised Woethief that we would vanquish them.

Our quivering body tensed as we raised our baton. It balanced impeccably in our hand.

A trumpet blast initiated the duel. The small crowd screeched continuously to visualize their surroundings with their ears. Our head ached. Unlike normal humans, we can hear the high-pitched shrieks of the people of Ildylia.

Ankor advanced first. With each blow from his baton, we scrambled to parry, recovering in time for the next attack.

When Woethief concentrated, she felt the presence of his emotions and tracked his body-heat with our eyes. We sidestepped a swipe at our chest and yanked on Ankor's arm. As our skin touched, Woethief absorbed some of his aggression into herself. Ankor lost his balance, but parried our swing at his head.

![Ankor Struck at WoeNyl](../Illustrations/WoethiefBook1.png)

<!-- Alt Text: A young woman holds a baton as she falls backward. Her curly hair escapes her hood. A man, with bat-ears, spider-legs, and a loose tunic, swings a club at the woman. -->

When we gained the advantage, Ankor tore his arm away from us and spoke again.

**"I've seen you with him, did he make you feel beautiful before she replaced you?"** Ankor said.

*'How does Ankor know?'* Woethief wondered. *'We tried to hide our past! The wedding was a secret.'*

*'Ankor will pay for his insults!'* Nyla thought.

We surrendered to our rage and the rage Woethief stole from Ankor. Our baton danced in wild maneuvers, which Ankor countered. His weapon stung our arm like a venom-bat. Our baton clattered to the ground. Ankor's fist slammed into our left eye like a miner's pickax. We toppled. Ankor poised to deliver a knockout blow. What we did next jeopardized our life, but we could not surrender. Our mistress would make us return to our old job if we did. With a flick of our right hand, we recovered our baton and smashed two of Ankor's eight legs. As he hobbled away from us, we pummeled his brow. He crumpled as if he bowed before us.

His girl, Flame, rushed onto the platform to her fallen lover. Air whooshed by as she folded her wings around herself in shame. Woethief pitied her despite her cruelty toward us. By loving a half-blood, Flame became a fool to the crowd. She collapsed beside Ankor with quiet sobs spilling out across the arena. As Ankor exited on a stretcher, she did not follow. Her champion had failed her and left her alone in her shame and rage.

Flame charged us and clamped her clawed hands around our throat. Woethief absorbed Flame's anger and grief, so she released her stranglehold and wandered away, confused. The egg sac of Ankor's child swayed on her abdomen.

We slumped on the arena floor and wept for Ankor's loss, experiencing Flame's grief in her stead. The warden declared us the victor and the crowds filed out of the arena, bemoaning Ankor's defeat. The rabble felt cheated that a girl with eyes conquered Ankor, a master fighter.

We traveled cold, stone streets to our slum flat.

In our room, tears cascaded down our cheeks. Ankor seemed to know how Centin hurt us. We thought we hid the betrayal of the man who haunted our dreams. Perhaps Ankor did not know. He happened to strike our deepest wound.

Time concealed what our body would reveal. The daughter in our womb would show herself as a bulge in our abdomen. Then people would accuse us of sins which we did not commit. Centin would escape punishment, as men often do. No one would believe, or care if they did believe, that he married us so we would steal all his shame. They would not believe that he divorced us due to no fault of our own. He was worshiped as a "god", and we were a peasant unanimal to discard when he grew bored.

We dreaded the feast, which existed to facilitate betting. While we clothed ourself in a dress tailored for people of another race, a clawed hand grabbed our shoulder.

## 2 WoeNyl Hired for Unknown Job

**"Follow me, quickly!"** our mistress, Gemma, ordered through vibrations in her fingertips.

"Why?" Nyla asked.

**"I've sold you to an honored guest at the feast tonight,"** Gemma said. **"He paid ten times what you're worth, so I'd best deliver you promptly and hear that you're properly dressed."**

"What job did he pay for?" Woethief asked.

*'She promised no more jobs unless we lost a fight,'* Nyla thought.

**"How should I know?"** Gemma asked. **"All I care for is the silver he offered."**

"What name did he give?" Woethief asked.

**"Stop asking so many questions!"** Gemma struck us. Damaging our face would not reduce the amount of silver she got, since she could heal us.

Gemma led us into a pool and massaged our forehead with her thumbs. She used mass from the water to create new skin over our bruises and cuts. Gemma ensured that her healing left scars. She could change our form and we could change hers, but not our own.

![Gemma Transformed WoeNyl](../Illustrations/WoethiefBook2.png)

<!-- Alt Text: A young, human woman kneels in a pool of water by a woman with bat-ears and spider-legs. The spider-woman shapeshifts the woman to have bat-ears. The spider-woman uses water to create false skin around the human woman. -->

We wished she would heal us completely. Instead, she wrapped a false, temporary skin around us. If we were not ugly underneath, someone might buy us to do chores rather than crimes.

Death had robbed our body of its strength and form many times and might rob us again tomorrow. We feared for our daughter. If we died, would she resurrect with us? Gemma charged her customers extra if we died, but no sum could compensate for our child's life.

Customers or those who caught us spying often killed us, so we would not snitch. Little did they know, we always return from death. Gemma sold the information we gathered.

Tomorrow it could be the same, we could resurrect to find our abdomen void of the precious life we carried. We longed to hold our daughter at least once. Our child's mind caressed ours with comforting emotions.

The only telepaths we knew were Centin, Gemma, our child, and a girl, named Theila, who haunted our dreams. Gemma's connection to our mind did not lead her to compassion, but to disgust at our failures.

**"Now, it is my turn,"** Gemma said as she took control of our body.

Gemma made us transform her into a full-blood woman with vocal cords like ours. No one would realize she was Gemma, since she now had wings.

"Why did we change you?" Nyla asked.

**"That is none of your concern,"** she said. **"Make your new master drink this."** Gemma handed us a clay flask.

"What did you put in it?" Nyla asked.

**"It is not poison if that is what you think,"** Gemma said. **"I drank half of it already. I will force you to give it to him if you don't do it willingly."**

***

Gemma dragged us to the great hall, which hosted the feast in honor of the tournament.

**"You can find your way from here,"** our mistress declared. **"I don't wish to be heard. Your new master is 100 throses away, straight ahead of you. He will approach you."**

"Aye," Nyla replied, knowing we were likely to trip, since the heat of so many bodies dazzled our eyes. We knew no one else, besides Theila, who could see heat. Theila's armored head bore eyes, which always seemed on the search for prey. She always found us in our terror-dreams.

With our small, round ears, we cannot hear shapes like Ildylians can. They shriek and know shape and distance based on echoes.

*'What will he be like?'* Nyla wondered.

*'Just as wicked as the rest,'* our other personality, Woethief, guessed.

The cacophony of emotions around us overwhelmed and disoriented Woe. We walked ten throses before we collided with someone in the crowd. His hand clenched our throat.

## 3 WoeNyl Attacked

**"You think you can just barge into me without consequences?"** his fingers thrummed into our neck, communicating without his mouth.

"We are ear-blind," Woethief gasped. "We did not mean to."

**"You are bold to speak to me with your mouth!"** he said.

"We cannot thrum. We are not rude." Each word stung our throat, which was not made to speak Ildylia's thunderous language.

**"Dumb *and* blind?"** he asked.

"Aye, pray, let go," Woethief begged.

**"Very well,"** he said.

He threw us to the ground and stomped away. As we rose, another man tripped over our legs and crashed into a stone pillar. The fallen man struck us. His claws left stripes of blood on our face. As we clutched our cheek, he slammed our wrist into the floor. Our bones shattered. We missed a kick at the man's stomach. Dozens of others watched the 'fight' while we tried to defend ourself. They cursed us because we had defeated their champions in the arena. Now, we could not resist one man.

What seemed like the entire hall shouted at us. Our attacker's slaps bruised our tense body. We dared not cry out, lest we anger him further. We shielded our face with our good arm and deafened him with our twilight fog. We longed for freedom from our tormentor and the crowd's aura of malice.

The crowd's attention turned to a new man, who smashed his palm into our attacker's nose. A dagger flew from our attacker's belt but the King's Guard subdued the brawl before the blade could taste blood.

Our rescuer crouched beside us. He draped a dress over us to hide our body from the crowd's snooping ears.

![Faithful Draped a Dress over a Disguised WoeNyl](../Illustrations/WoethiefBook3.png)

<!-- Alt Text: A man, with bat-ears and bat-wings, drapes a dress over a young woman as she leans against a pillar. The young woman has bat-ears and a short nose. Her face is sad. -->

*'Why did he have a dress?'* Nyla wondered.

As he supported our head, he dripped water from a flask into our mouth. The liquid's coolness contrasted against his warm body.

"Thank you," Woethief mumbled.

**"Why did he beat you?"** the man's claws thrummed gently into our shoulder.

Fear overran us because we suspected his kindness to be feigned. He seemed familiar. His ears streaked from above his brow, across his face, to his wet nose. Fangs protruded from his mouth. He was gorgeous, despite our difference in race.

"You do not know? We beat men in the duel. We need to reach our rent…, cli-ent ," Nyla caught herself. "We did not mean harm to that man."

Despite our protests, the man carried us into a private room, beside the hall. The touch of his clawed fingers terrified us. He ordered a servant girl to fetch a healer. We clutched our broken, left wrist to our chest.

"Why would you help? You could hate. We beat your man in a duel," Woethief said as we clutched the dress against our chest with our right hand.

**"I rescued you because I protect what's mine. I'm your new master and the one who'll oppose you tomorrow,"** the man thrummed without any evidence of emotion.

"If we die, then you will win," Woethief said.

**"Where would be the challenge in that?"** he asked. **"I'm a man of honor. I'd gladly lose to you tomorrow, knowing I fought fairly and well."**

"What did you mean by 'mine'?" Woethief shuddered at the possible meaning of his words. We longed to escape him.

**"Your mistress did not tell you?"** he asked. **"I purchased you permanently."**

*'Permanently?'* Nyla and Woethief wondered in unison.

"Why did you buy? You had to pay much!" Woethief said.

**"Only ten thousand pieces of silver. 'Tis the money I've gained from my victories,"** he said. **"By law, I get ten percent of every bet placed on me. Now that I own you, I'll get your ten percent if you win tomorrow."**

"What will be done if we fail?" Nyla asked.

**"I'll free you, but I hope you'll stay with me as a companion,"** he said. **"I didn't buy you for the money I'll get from your bets. I'll give you three-quarters of all I earn if I win. If you win, may I keep two-thirds of our earnings?"**

"We are your bond-girl," Nyla said. "Do what you want with the prize."

**"I don't intend to buy your friendship,"** he said. **"I only want to be fair to you. If you agree, it won't matter who wins, we'll both be better off for our victory. Do you agree to my terms?"**

"I do," Nyla doubted he would uphold his deal, so consenting presented no risks. He would probably take our earnings like Gemma did. We strained to remember who he was.

**"Excellent, I hope the healer can attend to you soon,"** he said.

"Why would you help? We do not think we have helped you," Nyla's skepticism poured out.

**"But you have,"** he said. **"Six ventings ago, I was attacked in Gallows Lane and you defended me. I never forgot that. Because of you, I lived to raise my ward, Theila, and my son, Denrick."**

Woethief remembered the attack as if she were the criminals. She stole their shame after they beat our new master to the ground. Woethief felt guilty for hurting a good man.

We also remembered Theila, an insectoid girl, who tormented us in our dreams. She insisted that she wanted to help us, but her armor, fangs, and claws terrified us.

**"I will free you in five days, once I am legally allowed to. I want to repay your kindness,"** our master said with his soothing, bass voice.

"You would not help if you knew our crime," Woethief said.

**"I know what you were. You were forced to steal the woes of many. I know the noose of slavery. I know what it's like to be forced to do shameful things,"** he said.

*'There is nothing he has done that is worse than the guilt I carry. I am vile. I am shame!'* Woethief thought.

Faithful said no more, but embraced us despite our emaciated flesh and blood-soiled clothes. As he cradled our head in his claws, his unexpected affection overwhelmed us. It was not a romantic embrace, but the hug of a friend.

Woethief probed his soul. There she found pain mingled with love for us and a peace which she had never encountered in anyone else. Woethief gritted our teeth and drew much of his pain into herself. It hurt less to steal pain because of love than to steal it because of coercion.

**"Thank you, WoeNyl."** He kissed our brow like we were his sister or cousin. **"That is why I love you so, because you're selfless. You're in torment, yet you steal my woes. I wish someone would steal yours."**

Woethief longed for relief from her shame. Only the deaths of those who gave her their shame freed her. Thousands had used her. Nyla longed for vengeance, but Woethief pitied her users. Who would take their guilt in the after-death?

Our marred form matched our ruined soul. Tatters of our dress dangled from us, revealing our humanness. The servant girl returned with a healer, who refused to bind our wounds when he heard we were an unanimal. Our lack of bat or spider blood made us the most despicable creature, second only to a lacerator.

Our master bound our wounds. He tried to be tender, but the salve burned and we shrieked whenever he tightened a bandage. While he worked, his mind and actions showed respect and purity, though we were at his mercy.

After he treated us, he left the room. We donned the new dress, woven from strange silk. It cooled our skin and eased our pain.

Our master returned. **"You're quite lovely, WoeNyl,"** he thrummed through his hand on our shoulder.

"Why do you mock?" Nyla asked and turned our head away.

*'No one has ever accused us of possessing beauty unless we were disguised,'* Woethief thought.

*'We lack the basic prettiness of a child,'* Nyla added.

**"I never jest. I perceive you now as lovely,"** Faithful said. **"I wish you could hear yourself as I do."**

He spoke truthfully. We knew because Woethief peered into his soul. Nyla feared he would change his mind.

"Tell how you hear our form," Nyla requested. She did not know if she believed his words.

**"You have scarred skin, like the walls of a mine that yields the purest silver,"** he said. **"Your lips drip the nectar of encouraging words. Your eyes are gorgeous. They exude compassion and tenderness."**

"You mock. We have one eye for each ear. We are a hag," Nyla said. Ildylians considered people with an eye to be cursed and people with two eyes to be doubly cursed.

**"No, WoeNyl, your eyes are lovelier than the Queen's gems,"** Faithful said.

"You did not tell all you heard," Woethief said.

**"Well, you're rather peculiar,"** he said. **"You seem to have no animal blood."**

"Thank you," Woethief drooped our head. In our ideal form, we were only beautiful like a semi-rare stone: as a curiosity. Our loveliness stemmed from our supposed good character, not our body. If he knew our crimes, he would not think us lovely.

"We are hu-man, not Ild," Woethief apologized. "We lack wing and web."

"What can we call you?" Nyla asked.

**"I am Faithful,"** he replied.

*'I hope his character matches his name,'* Nyla thought.

*'It does,'* Woethief replied. *'I cannot sense any deceit in him. He really loves us.'*

In his presence, our daughter's mind glowed with delight. Since she was the child of world-stewards, she had special abilities, like telepathy.

**"I hope you enjoy the dress,"** Faithful said. **"It is a gift from your sister, Theila. It can shapeshift itself to meet your needs."**

"What?" Nyla raised an eyebrow. Theila was the telepath who haunted our dreams. As far as we knew, Gemma was our only sister. Since Woethief split off from her, Gemma called us her twin.

**"Your mother, Phyla, had her after you disappeared."** Faithful said. **"Theila's a silkwing. Come, friend, a feast awaits us."**

Though he had no malicious intent, Faithful hid something from us. We needed to know more about Theila, the girl who haunted our terror-dreams. What was a *silkwing*? Was she truly our sister?

Faithful escorted us into the hall, where a servant led us to our seats.

We longed for Faithful to think of us as more than friends. In the hour we spent with him, he made it impossible to not love him. Nyla knew the foolishness of those feelings. Though Faithful seemed noble, Nyla's fear tricked her into distrusting him. We must protect our daughter. Nyla knew what men were like. Could this one really be trusted? Would he be willing to raise a half-human daughter, who was not his own?

Though we wanted to pour out the flask, Gemma puppeteered our body to stop us. With her mind-control, Gemma could make our body do whatever she wanted it to. We could not resist, but Gemma exhausted herself if she used her power for long times. We dreaded what the potion would do to Faithful.

## 4 Feast

At the feast, the servants presented many courses, but few of the delicacies were edible. Wool-deer blood and raw meat did not appetize us. Faithful dined across the table, too far to talk to.

We tried to avoid contact with the other guests. Our hood failed to conceal our face. Few people bore the curse of eyes or the drabness of being unanimal. The feasters taunted us about our "cheating" in the duels.

Despite our care, we spilled a man's goblet. Before he could strike, Woethief absorbed his anger into herself. She restrained her newfound rage.

![WoeNyl Spilled a Drink](../Illustrations/WoethiefBook4.png)

<!-- Alt Text: A young, human woman wears a hood over curly hair. She spilled a goblet across a table and a man, with shaggy fur and bat-ears, prepares to strike her. Various foods, in plates and bowls, are spread across the table. -->

The man resumed eating. We trembled as we recognized him. He had killed us three times: each while we were disguised differently. Death should not have troubled us after it robbed us so many times, but it did trouble us. The worst thing about our enemy was that Woethief carried the shame of our own deaths as if she were the murderer. Though Woethief pitied the man for his loneliness, Nyla longed for revenge.

We sniffled and dared not stir, lest we anger him. Rage filled Woethief with a longing to slay our enemy and be free from his shame. Nyla knew it would bring new guilt. We killed one of our clients once, in defense of our life. Our mistress ensured through five torturous deaths that we never did it again.

Terror of the man beside us overcame our rage. We clenched the stone table. Tears dribbled over our lip into our mouth. The man left to search for a different kind of drink.

As we nervously smoothed our hand over our dress, we shuddered at the thought of Theila, a bug-like fiend. She frightened us more than Gemma, but less than Centin. If Faithful trusted her, why did she scare us?

The smooth, flute-music of the royal musicians did not calm us. It added to the chaos of the shrieking and thrumming crowd.

Our new master received accolades and honors, while we endured insults. Though we tried to rejoice for Faithful, jealousy poisoned our soul. Woethief anguished over memories of the rewards stolen from our users. Though She knew the memories were not her own, they felt as vivid as our real past.

Centin gave a dishonest speech about his love for the Ildylian people. Woethief knew he was lying because she had absorbed thousands of years of Centin's shame. He cared nothing for his people, except for the beautiful women, who he used to fulfil his cravings.

As we endured Centin's voice, amplified by the rumble machines, a servant tapped our shoulder.

**"Your master wishes for you to join him,"** the servant said.

Nyla wondered what our master wanted. Despite our fears, we approached Faithful. The feasters' drunkenness flooded the hall with chaotic mirth, anger, and bluster. Their auras made us stagger like we were inebriated. Every step brought us closer to our only friend, Faithful, and greatest fear, Centin.

At the end of the table, near the royal couple, Faithful greeted us.

**"You deserved a better seat, WoeNyl,"** he helped us onto the bench and sat beside us.

**"So, this is the wench who beat Ankor?"** the chief treasurer asked. We recognized his thrum. **"Tell me how you cheated."**

**"She never cheats,"** our master, Faithful, declared.

We wished for as much confidence in ourself as our friend had in us.

**"You're in for a nasty surprise tomorrow. This hag has some crafty trick or devilry,"** Centin said. Our daughter's mind shuddered at her father's malice. No child should need to fear her parent.

**"I'll win tomorrow,"** Faithful said as he squeezed our hand. **"She is a good fighter, but I think I am slightly better."**

**"And what if she wins?"** King JaReel asked.

**"If she wins, I will keep two-thirds of our combined tax on the bets. And if I win, she will keep nine-tenths of the money,"** our friend increased his offer to us.

**"You make it sound like your victory is sure,"** Centin said. Despite talking through his hands, his lips sneered.

**"It is,"** Faithful answered. **"If I win the fight, I will become Prince. If I lose, I'll be so wealthy that I might as well be Prince. WoeNyl and I cannot lose either way."**

**"And what does she have to say?"** King JaReel asked in his slow, precise voice.

"We hope he will not mind great wealth," Nyla said.

The men guffawed at Nyla's bravado.

**"Maybe he won't, Princess,"** Centin's ridicule stung worse than a baton.

Our master squeezed our hand. **"My friend deserves to be Princess. She's more noble than the lot of you."**

We feared Centin's reaction to our master's closeness. No hint of jealousy came from Centin. His new, eight-legged wife clung to his arm. She exceeded our beauty.

**"I hope you didn't pay too much for this mad girl,"** the treasurer jeered.

**"WoeNyl is priceless,"** Faithful said. **"She is worth more than anyone could ever pay."**

**"But how much did you pay? We all know she is a slave,"** the treasurer pressed.

"You may tell them," Nyla hoped a quick answer would end the conversation.

**"I paid more for her freedom than you steal from the treasury in a year and she was worth every pence,"** Faithful said.

Though the accusation was true, the treasurer seethed. Before he could challenge our friend to a duel, Woethief stole his rage. We trembled and released Faithful's hand.

**"I'm sorry, WoeNyl, I didn't mean for this to happen,"** Faithful whispered.

We ate in silence as the noblemen debated the merits of their favorite wines and women. King JaReel refrained from their lewd discourse and drank in his wife's beauty. JaReel cared so much for Syreeta that, when he had claimed the throne, he banned dancing girls. Their strong love formed an aura around the royal couple, which overrode our rage. We longed to become Syreeta's daughter.

Syreeta's dress of jewels, a gift from Natia, sang like chimes. The music perfectly described Syreeta's beauty. Syreeta's lithe form contrasted with her great sadness. Wavy, purple bangs spilled over her forehead to accentuate her wide nose. Her ears ended in sharp tufts.

When we took Faithful's claws in our right hand, we hoped our actions did not embarrass him. Our proximity to his brain shielded Woethief and our daughter from Centin's aura of malice.

**"We disapprove of your lover,"** Centin said. His ageless form saddened us. We missed loving him.

**"She is not my lover,"** Faithful said. **"She's dear as a sister. I greatly respect her."** Faithful never showed emotion, but spoke evenly. Centin hated Faithful's bold speech. If Natia did not favor Faithful, Centin would have killed him.

**"Do you find her beautiful or is there another more beautiful?"** Centin asked.

Faithful did not wish to answer such a personal question, but felt he must.

**"She is more beautiful than Natia herself,"** he answered honestly.

We did not deserve the compliment. We possessed little moral or physical beauty.

*'He must be insane to challenge Centin and blaspheme Natia!'* Nyla's mind exclaimed.

*'Or very fond of us,'* Woethief replied.

Our master answered their questions and taunts with undeserved praise of our virtue. Though Woethief sensed no romantic interest, Faithful's care displayed in his accolades.

Doubt seeped into our minds because of Centin's taunts. We knew we would lose tomorrow's duel. We feared what would become of us and the child in our womb if Faithful won.

*'Does our master really intend to free us?'* Nyla wondered. *'Does greed motivate him rather than kindness?'*

Though we resisted, Gemma controlled our mind to force us to pour the flask into Faithful's cup. Faithful drained the cup without noticing the fluid, since he was distracted by a conversation about knife-making. We feared for him.

![Gemma Forced WoeNyl to Pour a Flask into Faithful's Drink](../Illustrations/WoethiefBook5.png)

<!-- Alt Text: A young, human woman holds her right wrist as an enemy mind-controls her to force her to pour a flask into a goblet. The man holding the goblet has bat-ears and wings and looks distracted. -->

When the meal ended, our master led us toward the dancing couples and music.

**"I fear you'll find me very dull,"** Faithful said. **"I don't know how to dance."**

"You are not dull," Woethief replied.

We held hands. In his presence, Woethief felt safe.

We stood in silence. He did not enjoy the lively music. His brokenness brushed against Woethief's mind like his fur brushed our hand. Someone hurt him so deeply that he could not express his emotions and felt a fraction of what other people felt.

We wished he could feel the beauty of the rumbling chorus and royal orchestra.

"You are good to hear, like the band," Nyla said. Woethief made us blush.

Faithful squeezed our hand tighter, but did not know how to interpret our compliment.

**"I love you, Woethief and Nyla,"** he said.

"You know that we are two?" Woethief asked.

**"Yes,"** Faithful replied. **"How could I not know? You call yourself 'we'."**

"'I' would be a lie," Nyla replied. Our honesty made our life harder. Since we did not know whether we were one or two people, we called ourself "we".

*'He knows we are double-minded, yet does not hate us,'* Woethief thought.

*'We still do not know if we can trust him,'* Nyla thought back.

**"I love that you are so truthful,"** Faithful stroked our hair.

He stated his love as a fact with no more passion than if he said three and three make six. He did not realize his actions were romantic, but thought they were fitting for family. His loveless upbringing left him confused about how to relate to other people.

*'How hard it must be to live without emotion,'* Nyla thought.

*'We live with too much emotion,'* Woethief thought back.

*'It may not be hard since he knows no different,'* Nyla mused. *'I wish we could give him feeling.'*

As fatigue gnawed at us, our master's hand supported us. Despite our proximity to his head, chaos from the drunken minds around us disoriented Woe.

## 5 Further Conversation

We followed Faithful as agony assaulted our frail body. We spoke little. Gemma prevented us from telling him about the flask.

In a few hours, we could retire. Sleep rarely visited us. When it did, it brought terror-dreams of Theila. Still, Woethief longed for freedom from the crowd's chaotic auras.

As we wandered the hall, the drums of Natia announced her entrance. With her approach, the tempo increased to match the pounding of our terrified heart. We hated Natia. In her name, the people of our world committed their atrocities. Her power enslaved women to the basest passions of the other sex.

Centin had murdered Woethief and Gemma's parents so he and Natia could steal their powers as world-stewards. Gemma lay dormant as an egg for thousands of years until she hatched. Natia raised Gemma in secret until Gemma was fifteen. When Centin found out about Gemma, Natia cast her out into the streets, to protect herself from Centin's wrath.

Gemma longed to overthrow the usurpers, but we contented ourself with the idea of being Princess. Warring against Centin and Natia was folly. We stole Gemma's painful memories of Natia's betrayal, so Gemma would not wage a losing war.

For the feast, Natia descended from her temple in a jeweled dress. Fabric and wings rustled as the crowd, including the royal family, bowed to their "goddess". Wind from the priestesses' flapping wings rocked us. Only Centin, Faithful, and I stood before Natia.

![Natia Greeted WoeNyl and Faithful](../Illustrations/WoethiefBook6.png)

<!-- Alt Text:  A human woman, with a hooded poncho holds a bat-like man's hand. A spider-woman, with six bat-ears and fine clothes, stands before them, flanked by bat-like priestesses. -->

We froze when Natia's party arrived. Rather than condemn, Natia thrummed pleasant words into the ground.

**"Greetings, Faithful, you who are highly esteemed. I come to present my blessing on your duel tomorrow,"** Natia lilted.

The silver of Natia's body seemed to suck warmth out of the room. She glimmered in our twilight. I pitied the priestesses, who gave pleasure to men's ears, but did not know the committed love of husbands.

**"I do not worship you,"** Faithful replied.

Natia paused briefly. **"That is well-known to me. I have elected to show my goodwill toward you and wish you victory in tomorrow's duel."**

*'All she can do is wish it, not cause it? She is not truly a great goddess,'* Nyla thought and rolled our shameful eyes.

**"Thank you,"** Faithful said. **"I…"**

"Talk no more," Nyla whispered to him before he embarrassed himself further.

**"I will relish your victory tomorrow. If ever you need aid, I am willing to help you,"** Natia said. **"You will feel my blessing when I perform your wedding. Your betrothed, Silver, said it is to be simple."**

**"I don't need a girl from you,"** Faithful said.

**"Oh, your lover here didn't know about Silver?"** Natia asked, referring to us.

**"She is not my lover,"** Faithful said. His words hurt more than they should have.

Natia smiled and nodded. She wore the same kind of jewels as Syreeta, but her music disturbed us.

**"Thank you,"** Faithful said and fled the hall.

We wondered what Natia meant by "betrothed".

## 6 Impossible Love

"Wait!" we shouted as we followed Faithful.

Our master turned to face us.

**"I suppose you'll rebuke my folly in refusing a girl from Natia,"** Faithful said.

"No, you are brave. We do not bow to her," Nyla said.

We admired him. Unlike other men, lust did not control him. Girls from Natia were among the most gorgeous in existence. How did Faithful resist a girl like that? We hated Natia and her priestesses! They destroyed millions of men, who might otherwise be decent.

**"You think it took great courage to deny her. It didn't,"** Faithful said. **"I've never been more afraid in my life. Nothing terrifies me more than lovely women."**

"You do not feel like you fear," Woethief said.

**"I never seem terrified,"** he said. **"I can't express my emotions the same way others do. Surely you noticed that I didn't react with pleasure to the food or music. I cannot."**

"If you fear the girl, why do we not give you fright?" Nyla asked. "We are good for you to hear."

**"Centin drafted me into the King's personal guard,"** Faithful said. **"He devised a method to remove desires and emotions which impede us. I was stolen from my parents as an infant and tortured all through my childhood. Whenever I cried out or complained, the torture worsened, so I just stopped crying. I can't express myself as you can. I fear women because Centin trained me to. He showed me carvings of gorgeous women then branded me when I beheld them. He used other unmentionable methods. I didn't qualify for the King's Guard, but Syreeta hired me as one of her guards. With her blessing, I left her employ when I advanced in the duels."**

"I mourn their harm to you," Woethief said. We sensed Faithful's mental scars, caused by Centin's telepathy.

Woethief longed to right the evil that deprived him of expressiveness. Woethief's hunger for pain rose to the surface of her consciousness. She longed for the agony of Faithful's past as much as Nyla dreaded to take it.

**"I'm not sorry. I don't know what it means to feel self-pity,"** Faithful paused. **"Perhaps it's better that I never know that pain. I don't fear you because though you are beautiful, you are not beautiful in the same way as other women."**

His words stung. We did not resemble his people. He walked on all fours, yet we stood on two feet. We knew he could never love us as more than a friend or perhaps as a sister. Though he did not say it, we knew our humanness seemed alien to him.

"If you do not fear, what do we make you feel?" Nyla asked.

**"Jealousy,"** Faithful said.

"Why?" Nyla asked.

**"You don't make me jealous. I'm jealous to protect your honor. I'm devoted to you because you saved my life. I love you,"** he said with a dispassionate voice, but Woethief knew he was earnest.

"We love you," WoeNyl whispered into Faithful's ear as we embraced him. Our daughter's aura rejoiced. She loved Faithful, like he was family.

Woethief inhaled and withdrew all his fear into herself. As we trembled, we collapsed at Faithful's feet.

We could not resist when he lifted us and carried us toward his flat.

Woethief's attraction to him went beyond mere appreciation of his character. The agony of fear thrashed at Nyla as he conveyed us to his house.

Woethief trusted him as much as she trusted Nyla. Nyla suspected Faithful of cloaked vices.

Woethief peered into his mind and it was full of honor and purity. Nyla wondered what filth it concealed.

Woethief longed for him as a prisoner longs for freedom. Nyla longed to escape him as if he were aflame.

Woethief loved him, despite the fear she stole. Nyla feared him.

***

Natia and a crowd of priestesses stood at the entrance of Faithful's flat. They had flown ahead of us. The modest street became a festive environment, with pennants stretched between houses. Despite hating their "goddess", Woethief enjoyed sensing the priestesses' jubilance.

Water poured on the ground touched our feet to provide ceremonial cleansing. The priestesses encircled us and Faithful. From a doorway opposite us, a woman, holding a banner to conceal her form, approached. It was Gemma.

![A Mysterious Woman Waited for Faithful](../Illustrations/WoethiefBook7.png)

<!-- Alt Text: A beautiful, young woman, with bat-ears, a braid over her left shoulder, bat-wings, and spider-legs, holds a cloth banner in front of her to hide herself. In the distance, a bat-man and a human girl hold hands and face the woman. -->

*'Gemma, what is happening?'* Nyla asked her mind.

*'I'm getting married. You should be happy for me,'* Gemma thought back to us from the round doorway.

*'What was in the flask?'* Woethief asked. *'Who is Silver?'*

*'A love potion. I bought it from Natia. I had to sell you to afford it,'* she said. *'I chose the name "Silver". Do you like it?'*

*'This is wrong.'* Nyla thought. *'Faithful should be able to choose a wife.'*

We feared love potions. A potion had caused us to fall for Centin, but synthetic love rarely lasts.

*'You're just sore that he won't end up with you,'* we felt Gemma's mental sneer.

"Faith, do you want to wed her?" Nyla asked.

**"Of course, I love her,"** he replied. He felt her joyous laughing vibrate the ground and fell in love with her voice.

"You drank a love draft," Nyla said.

**"That is alright,"** he was lovesick. **"My son deserves to have a mother."**

We could not think of many step-mothers worse than Gemma.

Natia did not care that Faithful was under the effects of a love potion. Since we could not stop the wedding, we helped the marriage as much as we could. We longed to make it last (unlike our marriage to Centin). Despite her lovely body, Gemma's soul was hideous.

*'How can Faithful hope to stay with her?'* Woethief wondered.

We used the water that touched us and Gemma to shapeshift her into Faithful's ideal woman. To make her more like ourself, we replaced her eight legs with two. She became less spider-like and more bat-like. We also gave her eyes like ours, with long lashes.

*'What are you doing?'* Gemma asked our minds. *'Are you trying to make me ugly?'*

We resisted her control, to keep ourself from changing her back. The silk of her dress morphed to accommodate her new legs.

*'We are giving Faithful what he deserves,'* Woethief thought to her. *'We want him to be as happy as possible.'*

*'We could use our power to morph her into part of ourself,'* Nyla thought. Fear of losing her only chance at love overrode her fear of Faithful. *'Then Faithful would be ours.'*

*'I know that we want to marry Faithful,'* Woethief replied. *'But that is not the way. I carry Gemma's shame. Do you want to carry her actual guilt?'*

*'What do you mean?'* Nyla asked.

*'If we become her,'* Woethief thought. *'Every bad thing she has done will be our crime.'*

*'You are right,'* Nyla replied. Our head drooped. Natia placed her hand on our shoulder.

**"She will make him happy,"** Natia whispered to us. **"Had I known you loved him, I would have arranged for you to marry. For this sacrifice, you have my friendship. We both know the sting of Centin's trickery."**

We nodded. Memories of Faithful, tortured before statues of Natia, assaulted us. We feared that Natia may seek revenge for us carrying a child with her husband, though we had not known that he was married.

**"You need not fear me,"** Natia said. **"Centin stole a love potion from me to seduce you. I know you are innocent. I also know that you and Gemma were destined to be world-stewards before we gained power. I am grateful to you for the power I bear and will do you no harm unless you try to take it."**

*'She was so grateful that she charged Gemma for the potion,'* Nyla thought. *'She owes her something for the power she stole.'*

"We aim to be the child of the Queen," Woethief said. "We do not need your throne."

When a priestess removed Gemma's banner, Faithful's mouth hung open. He entwined his fingers in Gemma's. The love potion and Woethief's theft of his pain allowed him to feel love. Natia approached for the exchange of vows.

Through the simple ceremony, Faithful swore to love and cherish Gemma forever. He meant it. They bit each other's necks to share their love-venom. As Woethief sensed his emotions, we continuously improved Gemma's appearance to please him.

We wished we were in Gemma's place. Faithful did not know that "Silver" was a fake name. We would not have deceived him like Gemma had. We would have given him what Centin withheld from us: commitment.

**"Thank you, Natia, for performing the wedding"** Faithful said. **"I will love her well. I must thank my own God for this wonderful blessing."**

**"I am pleased that you accepted my gift. May she continue to bestow her blessings upon you. May your bride have eternal youthfulness and innumerable offspring,"** Natia gave her benediction. Despite the hyperbole of Natia's words, WoeNyl made them literally true. With the power we had over Gemma's form, we made it impossible for her to age. The love potion would act as a fertility drug.

As Natia and her priestesses marched away, Faithful embraced his prize, oblivious to the fact that he had not chosen her.

Fear of Gemma's loveliness overcame Woe. We stood near Gemma, as rigid as the stones at our feet. Faithful bore no remembrance of the fear and the torture of his childhood that Woethief had taken into herself. Grief filled us as Gemma destroyed any chance the one man who might love us ever would.

We had longed to ask for Faithful's claws in marriage, but could not bear for him to endure the misery of wedlock to us. Our inadequacy as a woman already drove one husband to desert us. Centin's new wife was curvaceous rather than skeletal. We deserved to be alone.

## 7 Faithful's Bride

**"You can love someone you've just met?"** Faithful thrummed to Gemma.

**"Yes,"** she thrummed back.

Their conversation stalled. Neither knew how lovers should speak. Gemma was still shocked that her scheme had worked.

"Call her 'good to hear'," Woethief whispered. Anyone would think she sounded good.

**"I don't know how?"** he replied.

As we spoke, we examined Gemma. Her beauty surpassed every girl in Ildylia before her. With the pain of Faithful's past, her loveliness hurt Woethief to behold. By taking Faithful's woes, the trauma and emotions of his childhood afflicted Woethief as they had afflicted him. Memories of Centin's priestesses branding Faithful made comeliness seem like a brand on our eyes. We stared at our feet.

"The Queen would feel drab next to your girl. Peck her lip," Woethief said.

Faithful tapped his lips to Gemma's like she would shatter at his touch.

"No, no, no! Hold her. Make her feel like the one girl in the world," Nyla scolded.

**"But she is not,"** he replied.

"Make her feel like it," Nyla said.

**"What do you mean?"** he asked.

"Tell her no girl can vie for your love: that you will not leave her," Nyla ordered.

**"No other woman can compete for my affections,"** Faithful said.

We rubbed our fingers on our temples and groaned. Even her heat signature was stunning.

"Can you not hear her?" Woethief asked. "Read her and find a way to tell her that you want her."

**"I don't know how,"** he said.

"Try," Nyla ordered him.

Gemma was beautiful by the standards of humans or the high race of our world: the Ildylians. As Faithful's fingers ran through her fine hair, he relished the young woman shaped after his deepest yearnings. To match Faithful's preference, Gemma now bore two clawed legs.

Through our empathy, we had made her attractive beyond natural limits. She was the ideal form of ourself.

Her smile encircled her fangs, which dripped nectar. Her fangs and tongue flicked in and out of her mouth in excitement. Hair comprised of real silver strands and hair like ours flowed over her sharp ears. Large eyes flitted long lashes. She unfurled her bat-wings, to reveal a silk gown. She pulled Faithful toward her with a web woven from the spinnerets in her wrists.

Her wings undulated as if a breeze blew across them. Her claws terminated in pure silver nails. Her unblemished fur cascaded from her neck to her feet.

**"You are Silver but what is the second part of your name?"** Faithful resumed the conversation.

**"Whatever you wish it to be,"** she said.

Faithful stalled again. His presence in the room agonized Woe. Nothing could free her from the pain she stole.

*'How can we ever find a man as good as him?'* Nyla wondered. We still loved Faithful.

**"Silver matches your fur. It's so fine and smooth,"** Faithful ran his fingers through Gemma's hair and thrummed with pleasure. His newfound ability to feel removed all obstacles to affection.

**"Yes, Silver matches me, but any good name needs a second part,"** she replied.

**"Then you are Silver Nectarfang,"** he declared.

Woethief and Nyla found the secondary bower. We hoped Faithful could keep the conversation alive.

We reposed on our bed of spider-silk in a room just like one where we had spied on a businessman three ventings earlier. Ventings are the Ildylian name for "year", since steam fills the world for a week at the end of each year.

Sleep eluded us when the concerns of tomorrow crashed into our minds like a collapsing tunnel. The citizens of Ildylia hated us because we were a woman, unanimal, and a slave. Soon, they would despise us for carrying a child, which no man would claim.

We did not wish to rob Faithful's chance to become heir, nor rob our daughter of the status of royalty. Conflicting desires tore at our soul. If we won, we could gain power to destroy Centin, the man who betrayed our love and killed Gemma's parents. Faithful's memories fueled terror-dreams, robbing our sleep of rest.

As we tossed and turned, Gemma's aura stooped over our minds.

*'I suppose you hate me,'* she thought to us.

*'Mostly,'* Nyla thought back. *'But we love Faithful.'*

*'You love him after all the pain he has put you through tonight? He forsook you for a stranger created to appeal to his passion. The thought of you has comforted him since you saved his life, yet he does not show the same comfort to you. Instead he gives you all his pain,'* Gemma thought.

*'I took his pain willingly,'* Woethief thought.

*'Why? Why did you help me?'* Gemma's mind asked.

*'We love him,'* Woethief replied. *'We want him to enjoy your love. That is why we made you beautiful'*

*'You also hate him,'* Gemma thought.

*'Yes,'* Woethief thought. *'I stole so many woes tonight that I cannot help but do both.'* The bad memories of Faithful's upbringing had left his mind and entered Woethief. Though she did not want to, Woethief resented Faithful for giving her his woes. Now Woethief's terror-dreams would include the beatings of his childhood.

We would never be free from Faithful's past, unless he died. Despite the torment in Woethief's mind, we would never want Faithful to be harmed.

*'That potion was rather effective, was it not?'* Gemma asked.

*'We tried to warn him,'* Nyla said. *'We knew you were false!'*

*'Do I need to fear your revenge?'* Gemma asked. Her gloating turned to concern. *'Will you kill me like you killed Ankor's man?'*

*'He was going to kill us,'* Nyla thought. *'You did nothing to help us.'*

Gemma did not remember that her crimes had led us into the deadly situation.

*'But you think I'm threatening you,'* Gemma said.

*'We love Faithful. While he lives, we do not wish any harm on you. We will not break his heart. Why do you torment us? What did we do to earn your hatred?'* Nyla asked Gemma's mind.

*'Do you really want me to recount all your treacheries? All the murders? All the betrayals?'* Gemma asked.

*'No, no, please no!'* Woethief begged, reminded of woes we had stolen.

Theila, the lacerator, appeared as a vision in our minds.

*'Silver, please leave Nyla Woethief alone,'* Theila asked. *'I love Faithful's son and will marry him once I come of age. I want to love you as family. Please act like it. You must allow WoeNyl to sleep. Please do not disturb their minds any more.'*

*'I think you disturb them well enough on your own,'* Gemma chuckled as she left our minds.

Despite her good intentions, Theila horrified us more than Gemma. The gaze from Theila's eyes melted through our soul and probed the depths of our depravity. The terror-dream started.

> Venom dripped from her fangs as Theila stalked us. We fled the lacerator. Every time we turned into an alley to avoid her, she appeared at the end of it. Our legs burned. People in the streets kept moving obstacles in front of us.
>
> ![The Insectoid Girl Pursued WoeNyl](../Illustrations/WoethiefBook8.png)
<!-- Alt Text: A young, human woman runs from an insectoid girl. The human's hair trails behind her as she looks over her left shoulder. The insect has antennae, armor, and clear wings. -->
>
> We vaulted over a cart of loaf-plant and a pit opened beneath us. Wind rushed out of our lungs when we hit the bottom.
>
> The Lacerator stooped over us. Her mandibles wagged back and forth. Her body glowed with twilight.
>
> *'Please let me bite you,'* Theila said. Her antennae leaned toward us. *'My venom can heal you. I can help you.'*
>
> *'No, we not let you,'* Nyla wheezed. We did not know how an animal could speak, but we did not want to hear her voice.
>
> We focused on waking up. The dream dimmed.
>
> *'Please don't wake up yet. I need to explain,'* Theila wailed.

We forced our eyes open. No one heard our internal screams or offered to deliver us. We wished Faithful could hold us.

*'Why did we not beg him to marry us? Why did we let Gemma steal him?'* Nyla wondered. *'We might forget our terror if we had him to hold.'*

After hours of turmoil, we drifted off.

## 8 Preparations for the Duel

Gentle rapping on our door roused us from our troubled sleep. We admitted Faithful and Gemma into our room.

**"I trust you slept well and are prepared for the duel,"** said Gemma, disguised as Silver and feigning politeness.

*'How could anyone sleep before such a momentous event?'* Nyla wondered.

"We dozed for an hour. We fear the duel. We feel like we will not win," Nyla snapped.

**"I am sorry to anger you. I was only trying to be friendly,"** Gemma thrummed with sarcasm.

We sobbed as the bottles of our tears burst. Fear of Gemma and the duel overwhelmed us.

"We fear the duel," Woethief said. "We want to win, but we want you to win. We do not want to hurt your hope for a good life."

**"Regardless of who wins, we have each another. We won't desert you if I win,"** Faithful declared. **"My son and his betrothed love you too. Denrick was at work last night, but he should be back after the fight. Theila will keep you safe."**

"We do not fear that you will leave," Woethief said. "We do not want to hurt you. We broke the hand of a man we fought. And, and…"

**"What is it, WoeNyl?"** Faithful asked.

"We are with child," Woethief whispered. "We do not want her to be hurt."

Faithful stroked his chin. As a parent, he felt the same desire to protect his child.

**"Congratulations,"** Faithful said. **"The world is blessed to have another WoeNyl born to it."**

**"Yes,"** Gemma thrummed with hidden sarcasm. **"I cannot think of anything better that could happen to Ildylia."**

**"Can she feel me if I touch your abdomen?"** Faithful reached toward us.

We nodded and Gemma and Faithful put their hands on our belly at the same time. Our daughter's mind moved toward Faithful's loving aura.

"You are loved by our child," we said. "Her mind reached for you."

**"Hello, sweet one,"** Faithful said and smiled. **"I love you. I can't wait to meet you."**

We squeezed Faithful's hand. "You will be good to her."

**"I will cede my victory to you. I will not endanger your child,"** Faithful offered.

"They will not let you give up," Nyla objected.

"If you do, they will hate you. It will not be fair to your wife or Den," Woethief added.

**"Your safety is more important than my reputation,"** Faithful replied.

"You will not give up. We ban it!" We crossed our arms, with our left arm in the sling, and stood on our toes.

**"You can't forbid it,"** Faithful said. **"I'm your master."**

"If we do not ban it, we will be the heir with the right to ban it," Woethief stopped Nyla before she could say something even more ludicrous. "Why would you help the child of a vile bond-girl?"

**"Because the child is innocent,"** Faithful said. **"Whatever was done to you isn't your fault."**

"You do not throw blame?" Woethief asked.

**"Should he?"** Gemma asked. Her gentle thrumming contrasted with the sarcasm in her mind.

"We did no wrong!" Nyla exclaimed, remembering our divorce from Centin. "When we wed him, we did not know he had a wife. One month in, he left for a new girl. We want to win. Then we can kill him!"

Gemma didn't respond at first. She held us until our eyes dried.

**"Nyla, you won't seek revenge in the duel, will you?"** Gemma thrummed quietly.

"No, we love Faith," Nyla whispered back.

**"*I* love him. Thank you for making me beautiful to him,"** Gemma used her thanksgiving to disarm us.

"We are glad to make him glad," Woethief said.

**"Please do not tell him that I am Gemma,"** she asked. **"He changed my life. I traded all my wealth for the love potion, so I could start a new life with Faithful. I have never been happier."**

When Woethief reached into Gemma's mind, she found Gemma's words to be true.

**"Last night, I felt so guilty about everything I've done to you,"** Gemma's ears drooped. **"Faithful is so kind and I feel so wicked. As you slept, I tried to steal back the pain and shame I gave you. I couldn't, but I won't send you any more either."**

Her words proved true, the river of shame which perpetually flowed from Gemma was stopped.

"Why did you do that?" Woethief asked.

**"Faithful told me of his God,"** Gemma answered. **"I did it because I might be able to earn redemption for myself. Perhaps, if I can replace my hatred for you with devotion to my husband, God might forgive my crimes."**

"No good you do can clean your bad life." Woethief told her, knowing it was true for ourself. "If you are for-giv-en, it will be by a woe-thief more great than we are."

As Woethief examined Gemma's aura, she felt three new souls inside of Gemma. "We are glad that we are an aunt," we said.

**"What do you mean?"** Faithful asked. **"You're not related to Silver or I. You are closer to Denrick's age than an aunt would be."**

**"WoeNyl and I are like sisters,"** Gemma said. She dared not tell Faithful that we are twins.

"You will bear three," we said as we placed our hand on Gemma's navel. "We will hold back no boon from each child."

Faithful turned to his wife and smothered her in kisses. Gemma awkwardly smiled. **"The love potion worked quickly,"** she said.

**"I love them,"** Faithful stated. **"They will be gorgeous and kind like you."**

"We are glad for you," we hugged our sister. Despite the fear and pain she caused us, we loved Faithful and his children, who would grow to be world-stewards like Gemma. Faithful joined the embrace. Our family's auras of happiness intermingled like spices in a delicious meal.

Woethief could not explain why, but she hoped Faithful could reform Gemma. She hoped Gemma could find redemption and become the woman of honor he deserved. Woethief would forgive her all she did against us to make Faithful and his children happy.

After Faithful left, we asked Gemma to heal us. Gemma healed the gashes on our skin, but refused to heal our broken wrist because she wanted Faithful to win the duel.

The dress that Faithful gave us the night before was now shaped like trousers with a short gown over the top. We could not explain how it had transformed itself in the night, but Faithful ahd told us that it would change to meet our needs. Armor was illegal in this duel, so we needed no further clothing. We would not be allowed to use our baton either.

While we breakfasted, Gemma convinced Faithful that he should not yield the fight. We feared injuring him or our child in the duel. Our left arm throbbed in its sling.

## 9 The Final Duel

When we entered the arena, the crowd cheered and stamped their feet for Faithful. Ankor's disappointed fans hissed at us with their mouths. In the whole crowd, not one person wanted us to win.

As a slave led us to the combat platform, where the King proclaimed the rules of engagement to the opulent onhearers. Sweat trickled down our back. We felt dizzy, weak, and tired.

King JaReel held the ceremonial cup before us. Faithful drank first. The water cooled our parched throat. Queen Syreeta confiscated our weapons and gave us identical staffs.

**"Fight as you have never fought before, Princess,"** Faithful said.

"You too, Prince," we lisped, since his future title was hard to pronounce.

His aura of love for Gemma augmented our sight and sense of his body-heat.

*'We should surrender,'* Woethief thought. *'What if our daughter gets hurt?'*

The horn-blast threw us into a struggle that we did not want.

*'It is too late,'* Nyla retorted.

We catapulted ourself at Faithful.

He parried effortlessly. We stumbled and blinked our bleary eyes.

**"WoeNyl, you're too hurt to continue. I will still surrender,"** he offered.

"No," we hefted our staff. Our child's aura comforted us.

**"As you wish,"** Faithful said.

His stick swung unpredictably about our kneeling form. When we blocked one swipe, a strike from the other direction followed. Our skill forsook us, since we fought with the wrong hand. Centin had forced the royal couple to ignore the unfairness of our injury.

We rose. Our arms throbbed.

Faithful drove our staff away from our body. We clutched it desperately. One more strike and we would drop our weapon. To our bewilderment, Faithful did not press.

We exploited a momentary weakness. Our staff blasted his away, then crashed into his head. A crack sounded as the weapon hit his skull. We lowered our splintered weapon, afraid to look at him.

To our horror, his body-heat disappeared.

He solidified into a silver statue. The crowd erupted in hissings and scraped their nails against the stone seating in protest. Who dared to interfere with the sport they loved?

Syreeta gasped. She loved Faithful like a son and would have adopted him without the duels if JaReel had let her. How could someone harm such a loyal friend?

Silver screamed. How could someone hurt her husband on the morning after his wedding?

Centin sneered and JaReel stood indifferent.

![Faithful Solidified into a Silver Statue](../Illustrations/WoethiefBook11.png)

<!-- Alt Text: A young, human woman leans on her right hand. A shattered staff falls next to her as she looks up at a bat-like man, turned into a silver statue. -->

*'How did he get Gold-Water?'* Nyla wondered. Gold-Water turns anyone who drinks it into gold if they are guilty and silver if they are innocent.

As we clutched his face, we felt a poison dart in his neck. Our legs collapsed. Agony tore through our left shoulder as we landed on it.

"We did not mean to. We will help. You will live." Woethief muttered.

Gemma rushed to her husband.

**"What did you do to him?"** Gemma roared at us. **"How could you poison him?"**

We turned our face toward her, so she saw the movement of our lips, with her ears. "We did not freeze him," Woethief replied.

**"No, Faithful, I need you. You cannot depart like this!"** Gemma sobbed on her husband's metal body.

Grief overwhelmed us. Sweat soaked our tunic like the guilt which stained our soul.

Gemma tapped Faithful's hand with her nail. **"He is silver,"** she said. **"He never had a chance to ring the bell."**

We swooned.

## 10 Empty Victory

We awoke to slave-girls in a frenzy to make us presentable. While they wiped blood from our face, more slaves brought a dress into the room. Envy filled every girl at the sound of it. We shook off our grogginess and thrust the nearest slave aside. 

**"What are you doing, Princess?"** she asked us as we exited the small chamber. **"We must prepare you before you are presented as victor."**

"We will go how we are," Nyla said. "We will not for-get the harm. We will not for-get that Faith froze."

**"But we have our orders,"** the slave protested.

"We care not!" Nyla screamed. "They want to hide our deed. We will not have it! A good man froze. How can we glor-y in our win?"

The slaves did not reply, so we left them all in their befuddled state. Gemma met us, wiping tears from her eyes. Bruises adorned our haggard limbs. Our left wrist and shoulder throbbed.

We scrambled up a flight of stairs toward the royal family. When we slipped, Gemma caught us.

*'Please heal us,'* Woethief asked Gemma's mind. *'This meeting will go better if we can salute with our left hand.'*

*'Very well,'* Gemma thought back. *'Faithful would want me to. He liked you for some reason.'*

Gemma's touch realigned our broken bones and filled our hand with a pleasant sensation. She needed no water since she did not need to add any new material to our body.

"Thank you," we said as we removed our sling.

We shuffled to the King with the air of a conquered soldier rather than a triumphant princess. Our salute earned Centin's scorn.

**"Why do you present yourself before your god in such a squalid condition?"** he asked.

"A good man fell. How can we have joy?" Woethief asked.

Tension filled the room like steam from the vents in the wastes of the Barren Caverns. We quivered before Centin, Natia, and the royal couple in fear of their displeasure. We had won. We were Princess, but it did not satisfy us.

**"How can you not rejoice? You are being made royalty,"** Centin replied.

*'Rejoice?'* Nyla asked within ourself. *'Faithful is a statue. His wife mourns at his feet. He should stand here in our stead.'*

Queen Syreeta rose from her place beside her husband and approached us. We quivered. Syreeta placed her left hand behind our back and guided us into a room adjacent to the platform. Silver stayed with the King, Centin, and Natia.

**"Nyla"** Syreeta addressed us.

"Aye," Nyla stammered.

**"Tell me, did you turn him into silver?"** she asked.

"No," we pulled away from her.

**"Did he know the risks associated with this contest?"** Syreeta asked.

"He knew," Nyla said.

**"Then how can you blame yourself?"** she asked. **"Did you shoot him with that dart?"**

"No," Woethief cried as tears filled our eyes. "We love him. But we hit Faith in the head with our pole!"

**"Nyla…"** Syreeta began.

"You cannot com-fort," tears stung our eyes as we backed away from her.

**"Compose yourself, dear,"** Syreeta put her claws on our right shoulder. **"He is not truly dead. When the bell in the Golden Death rings, he will come back. Please lay aside your grief until you are in the privacy of your own bower."**

"Who will pay for the crime?" Woethief asked "Who will a-venge him."

**"We will catch the criminal who shot him. Today should be happy for you,"** Syreeta pressed our baton into our hand. **"We rejoice that Natia gave you to us."**

Syreeta wrapped her arms around us in a motherly embrace. As it touched our skin, her musical dress described our form too. Happiness flowed from our daughter's mind as she telepathically sensed Syreeta's kindness.

**"My dress says your hair is lovely,"** Syreeta kissed our cheek. **"I've sacrificed to Natia for so long, to hear this moment."**

When she released us from her hug, relief filled us. Faithful's fear of beauty made us fear the Queen.

After she told us to don a dress that was hung on the wall, Syreeta departed. We intended to disobey, but the dress from Faithful repaired itself in an explosion of silk. The gown, lovelier than any we had ever felt, flowed to match our form.

![The Gown Wrapped around WoeNyl](../Illustrations/WoethiefBook12.png)

<!-- Alt Text: A young, human woman's hair blows to her right as a silk dress weaves itself around her. The dress comes to her ankles and has sleeves, which leave her shoulders bare. -->

With the fabric's touch, grime left our skin and courage filled us. The fabric fit so perfectly that we scarcely felt its presence.

When we emerged, the King pronounced us victorious and gave a speech, welcoming Gemma and I as his daughters. Since Gemma was disguised as Silver and married to Faithful, Syreeta loved her. Faithful had been one of Syreeta's guards for years. Tears spilled from our repugnant eyes. The agony of our wounds and shame prevented all pleasure.

*'Who could feast and dance after the loss of her friend?'* Woethief asked Nyla.

The King and Queen wished Faithful had won, but did not show it. King JaReel feigned pleasure at his new daughters as he blessed Centin and Natia for bestowing us upon him.

Syreeta envied our lovely curls. Her longing for a child conflicted with her fear that we had poisoned Faithful, who had been her favorite guard and dear friend. She wilted under the bittersweetness of the day's events. Our daughter sent an aura of comfort toward the Queen.

During the celebration, the dress strengthened us despite our injuries. Gemma clung to Syreeta's hand for comfort. The Queen's aura of love soothed Gemma's soul.

After hours of feasting, in which we embarrassed ourself by our lack of social graces, we retired. We needed rest after the anxiety and anguish of our day. Gemma stayed with Syreeta and Natia, who loved her witty comments and fabricated stories. Gemma disguised her grief well.

Centin ordered two half-drunk guards to escort us to our new palace. They did so with no hint of professionalism. Their presence brought more danger than if we groped through the streets alone. Ildylia ignored us as the non-uniformed men herded us into the slums.

## 11 Assassination Attempt

"Why are we here?" Nyla asked as we walked. The night fog dampened our footsteps and dampened our hair.

**"Orders say take you to yer palace, girly,"** the taller guard slurred.

We plodded on.

Three lacerators fought over a bone in a garbage pile. Their cold armor contrasted with the heat of the decomposing rubbish. As they squabbled, their blade-like legs shredded their surroundings. We shuddered. Theila was like a humanoid version of the scavengers in the trash.

**"We is here, girly,"** They stopped us outside a hovel instead of our palace.

We were at Skinners' Crater, the territory of the fiercest gang. We had spied on many criminals on this street. Shame and the reek of rotting waste washed over us.

*'They took us here to kill us. If we die now, our child will die too,'* Woethief panicked in our head.

A guard toppled us. The taller guard's thin legs pinned our right wrist to the ground. The safety on his pistol clicked off.

**"Don't keel her you idit,"** he slurred. **"She be the Woethief. Lit 'er steal ar pain first."**

The air-gun's barrel jabbed into our temple and a soiled hand pressed against our face. The other guard's bare legs pinned our left arm to the ground.

**"Do it, girly. Take our pain,"** he said.

As hard as Woethief tried, she could not manipulate them to avoid stealing their shame. They bore no anger toward us, which would be easy to remove. Greed motivated them.

"Why would I?" Woethief asked. "We will die if we do or do not."

**"You'll live longer if you obey,"** he said.

*'An extra five minutes cannot induce us to bear their shame,'* Nyla thought to Woe.

"Vow that we will live," Nyla said.

**"Obey us, girly!"** the guard with the gun hissed.

Woethief channeled some of his anger into herself. He removed his pistol from our head and raised it to strike us. Once he did, we twisted our wrist free and bit his leg. Howling escaped his fingers. We kicked the other guard in the groin. A bullet slammed into our chest. The fabric of our strange dress stopped the bullet from entering our torso. We ran.

Footsteps thundered toward us. But we formed a shield of twilight fog behind ourself. Without being able to hear, the drunk guards tripped. We ran until we reached Faithful's house. We collapsed, out of breath, and pounded on the door.

An unanimal boy unlocked Faithful's door.

## 12 Confronting Den

"Who are you?" Nyla stammered, surprised that the boy was an unanimal. He filled the entire doorframe. His veins bulged in his arms.

"I am Denrick, son of Faithful," he replied in a language which we did not know. The collar of our dress vibrated in the Ildylian language to translate for him. "Faithful adopted me and raised Theila as his ward."

He lifted us to our feet. Denrick weighed thrice as much as us and could easily overpower us. Woethief detected the truth in Denrick's soul and assured Nyla that would not hurt us.

After Denrick barred the door, our breathing slowed.

"Why did he make her a ward?" Nyla asked about Theila.

"The Duke of the Gateway Region, your father, was punished for his wife's disappearance," Denrick said. "Since Centin disliked the Duke, no one in his court offered to raise your sister, so my ebu did."

We walked further into the house.

"We've been waiting for Gemma to let us talk to you," Denrick said. "Theila wants to bite you to plant her symbiont, a living part of her, in you. She cannot be healthy without a host to live in."

*'How can a dress hear?'* Woethief wondered as the fabric translated.

Denrick handed us a flask of water, which we guzzled.

"She shapeshifted into your dress," Denrick said as if it were normal. "Your sister is special. She wants to share her power with you."

**"I wish to give you my symbiont,"** Theila thrummed through the dress's fabric. **"I know I am frightening, but I love you dearly."**

"Why would we want that?" terror filled Nyla's mind. Woethief considered the offer, since our daughter's mind sensed Theila and seemed at ease. Woethief pitied the lonely monster.

Memories of our terror-dreams tormented Nyla. '*The monster wants to bite us, but we always fight it off. Why would Woethief want that thing near us?'*

**"As my hostess, you would receive complete physical healing. You would become beautiful again and your scars would fade,"** Theila promised. We shuddered as the living dress spoke.

"Why not bite *him*?" Nyla asked as we pointed to Denrick.

**"I will bite him when I come of age and we are married,"** Theila said. In our dreams, she still had the body of a child. **"I need two willing hosts."**

"We will not let you bite!" Nyla shouted.

"Since our childhood, my ebu, Faithful, has helped us to search for you," Denrick said. "We've done all we can to aid you. Theila only bears love for you!"

"We killed Faith," we said.

"You didn't kill him," Denrick declared. "I heard that Centin gave him Gold-Water. My ebu froze before your staff hit his head."

Woethief probed Denrick's mind and found no aura of deceit. Assuming the dress translated accurately and Den was well-informed, he spoke the truth. Feelings of love for Theila, played near the surface of his mind.

Relief washed over us as we realized we did not poison Faithful. Someone else must have given that shame to Woethief. We hated Woethief's link to our users' minds.

"Why did you not keep Faith from harm?" Woethief asked.

**"I didn't hear the dart that hit Faithful,"** Theila said. **"Besides, I was not allowed to act as armor during the duel. I was only your clothing."**

"Why did you not help him?" Shame for surviving, when Faithful froze, overwhelmed us. "*We* would have come back to life."

"But your daughter might not. My ebu taught us to protect women and children first," Denrick said as he held us.

"Why would you help a bad girl? Will we need to take your pain?" Nyla asked.

"No, you will be my sister-in-law. I love you." he said. "I would have been there to help, but I had to be at work. I had just come home after a long shift when you came to our door."

"Why do you love me?" Nyla asked Theila, our dress.

**"Because you are my sister and you created me,"** Theila said.

"Made you?" Nyla asked incredulously.

**"Yes,"** Theila said. **"Remember your visit to Phyla, the Duchess of the Gateway Region? You and Gemma were paid as healers for her. A lacerator, planted its egg under our eba's skin and the poison nearly killed her."**

"We helped Gem heal her. Gem healed the girl in her womb too," Woethief recalled.

"Our eba was not with child," Theila claimed.

"What?" we asked.

"The child you remember was the lacerator egg," Theila said. "You ensouled me. I was the egg. I'm a silkwing now. I think that name fits me better than lacerator."

We started back, legends said that only Natia could give souls to beasts. "How could we have?" we asked. "Only Nat-ia can."

*'Are we like Natia?'* Nyla wondered. *'Is that why we are cursed with power?'* We shuddered.

"You grasped the soul of a banished twilight-person and put it into the egg," Denrick replied. "You are powerful, like Gemma, Centin, and Natia. You and Gemma are the true rulers of Ildylia, but Centin and Natia stole your position from Gemma's parents."

"We turned that thing in-to a girl?" Nyla sputtered.

*'How can she bear to be so ugly?'* Woethief wondered. *'How could we have cursed her like that?'*

**"Yes, you blessed me with a physical form. When I was pure twilight, I could not fully interact with this world."** Theila answered. **"I long to bite you because you are my friend. I cannot help it that you fear my true form or that you run away in your dreams."**

"We fear you," Nyla declared. Theila, who appeared as the lacerator in our terror-dreams, still frightened us in our waking hours.

"We might take your gift," Woethief said. "It might take a while to get Nyl-a to."

Relief swept over Theila. She hoped to change Nyla's mind.

**"That is all I ask,"** Theila said. **"I don't want to be frightening. You could have designed me however you wanted. You refused my mind-touch, so I had to design myself, with power borrowed from you. Full-blooded twilight-people, like me, are natural shapeshifters. I became your dress so Faithful could present me as a gift."**

"Why a gown?" Nyla asked as we played with our twilight orb.

![WoeNyl Played with her Twilight Orb](../Illustrations/WoethiefBook16.png)

<!-- Alt Text: A young, human woman holds up an eye-sized orb of twilight while she stares at it. She has curly hair and a silk dress. -->

**"I take any form you require, just by thinking about it. I knew you would not allow me to meet you in my normal form, so I shapeshifted into a gown,"** Theila said.

"We wear you now?" Woethief asked, to be certain that we understood.

**"Yes,"** Theila said. **"The form you see in visions is my natural form."**

Denrick longed to marry Theila, the strange insectoid girl, who haunted our dreams. He did not understand why she frightened us.

"Den, why would you help? We are a bad girl." Woethief asked. Because of his kind aura, she trusted him. He reminded us of Faithful. Beside the giant, we felt like a little girl.

"You can change," Denrick said. "Theila and I want to help you overcome your past."

Tears filled our eyes as we remembered our past. When we were an infant, Gemma kidnapped us and shapeshifted us into an adult. Our earliest memories were of Gemma's beatings. She rented our power to anyone who wanted their shame removed. She forced our body to spy on people, so they could be hurt by unspeakable crimes.

Woethief carried the weight of the sin of thousands of our enemies. Den couldn't imagine how it hurt. Damnation was our cloak and vice was our constant companion.

Denrick wept with us as we remembered our story. We lacked the words to tell him our pain.

Gemma had used our body as her puppet. Whenever we were caught, while spying, she left us to die. When we awoke from death, we bore a new outer body and the shame of our own murder.

We hated Gemma for her crimes. She stole us from our mother. Why did she keep us from our sister and Faithful too?

*'Why are we the only person for whom death is not permanent?'* Woethief wondered. *'We have done so much evil. How can Den and Theila befriend us?'*

"I love you," Den reassured us.

After he released us from his hug, we returned to the secondary bower. Den rested in another room. Our daughter made us sleep deeply. Her powers seemed amplified by Denrick and Theila's presence.

## 13 WoeNyl Awakened by Crystal

*'Eba… Eba, it is time to rise,'* a feminine mind thought to us.

*'What?'* Woethief thought. *'Who is calling us "mother"?'*

![A Feminine Mind Thought to WoeNyl](../Illustrations/WoethiefBook15.png)

<!-- Alt Text: young, human woman sleeps peacefully with her curly hair splayed beside her. The woman wears a conservative, silk dress. Her abdomen glows as her unborn daughter telepathically communicates with her. -->

*'Get up silly,'* an innocent, girlish mind ordered.

We opened our eyelids, which reminded us of our ugliness. Cold stone greeted us.

*'Who are you?'* Nyla asked the voice in our head.

*'Why, your daughter of course. I'm Crystal Comfort,'* she replied.

Pleasant shock reverberated through us.

*'You have never spoken to us before,'* WoeNyl thought with both of our minds.

*'Aunt Theila taught me to. I'm a telepath like you. Aren't you proud of me?'* Crystal said.

*'Of course, darling. We just never expected that you would be so intelligent. Who named you?'* The fact that Theila thought to our child without permission unnerved us. However, we never gave Theila the chance to ask for permission.

*'I did,'* Crystal thought. *'Theila says I must wait a long while before I am born. Is that so?'*

*'Yes, dear,'* we replied. Near Crystal, Woethief and Nyla felt unified. *'How did you meet Theila?'*

*'I found her mind and she let me explore it,'* Woethief felt a smile in Crystal's thoughts.

As quickly as her voice entered our consciousness, it left.

"How did your find the mind of our child?" Nyla asked Theila.

**"My mind is potent. Crystal found it."** Theila replied. **"I thought it would bless you to meet Crystal, so I trained Crystal to contact you. It was impossible to introduce you to your daughter until you dropped your defenses. I hope you're not displeased."**

"We were glad to talk to her," the gift filled us with joy.

**"Your daughter inherited yours and Centin's power,"** Theila said. **"I'm training her to use it for good. I've tried to become what Gemma should have been for you. I know you fear me, but I like being a silkwing."**

"Thank you," Woethief said to Theila. "We cannot take your gift yet."

**"I can wait,"** Theila said.

Denrick's bulk dominated the doorway. He bore twilight and did not resemble Ildylians. He looked like us: human. Short hair poked out of his scalp above thick eyebrows. His dark skin blended in with the shadows in the house.

Moisture spilled from our eyes. We threw our arms around Den's neck and wiped away joyful tears. We could not remember the last time we were happy. Crystal wove webs of love between us and our nephew.

After a simple meal, Denrick surprised us with a question. "WoeNyl, may I braid your hair and prepare your adornments for the celebration?"

"How did you learn to braid?" Woethief asked.

"Theila taught me," Denrick said. "She loves to make herself and others beautiful. I enjoy what she enjoys."

"You can braid," WoeNyl beamed. For the first time in our lives, someone prettified us for the joy of it, rather than as a disguise. Theila, as a gown, looked spectacular.

Denrick knelt behind us. With his thick fingers, he wove our hair into an elegant latticework. His giant hands never faltered. Theila thrummed her approval.

While Denrick worked, we questioned him. "Who are we?"

Pain filled his mind as he remembered something unpleasant, something he dreaded to tell us.

"Den, we can bear it," Nyla said.

"I know little. Phyla arrived in Ildylia with a newborn ward, you. Duke Strongweb took Phyla as his wife and adopted you. Shortly afterward, Gemma kidnapped you and stole your powers. She gave you her ability to steal woes and created Woethief's personality."

"What did they do when I left?" Woethief asked.

"They searched for you, without success. Around that time, my mother, Luma, arrived in Ildylia and died giving birth. Phyla nursed me and Faithful adopted me. Then Strongweb and Phyla celebrated the birth of Theila," Denrick said. "When Phyla disappeared, Faithful took Theila in. Though you are not related to her by blood, Theila is your sister."

*'He is telling the truth,'* Woethief thought.

Loathing for Gemma caused us to dig our fingernails into our palm till they drew blood. We desired vengeance for her oppression and abuse.

**"You have a little creativity left from your eba. Just as Woethief's parents were world-stewards, Nyla's eba was a world-steward. That is how you made me,"** Theila said. **"You are my stewardess, so you have creative power over me. Crystal will be born more powerful than you, since her father is special too."**

"No one can take our load. We do not want to take pain," Woethief said about our power.

"No, your compassion cannot be removed from you," Denrick said.

"Gem will pay," Nyla said. "We vow we will…"

**"Don't say something you'll regret,"** Theila said. She pitied us for the hatred we suffered from. **"You cannot lie, so if you make an oath, you must fulfill it."**

"You wish to kill Gemma?" Den asked.

"Aye. How can we not hate her? You know Gem earned death through her crime," Nyla said.

"What will her death avail?" Den asked.

"It will free me from her guilt," Woethief said.

"But it will bring new guilt," Denrick said. "Guilt would drive you mad as you lived on knowing you killed the girl Faithful loved. I know she used a love-potion, but she is my stepmother now."

"We cannot for-give her. No thing can take our hate," Nyla said.

"You are right. You are powerless to overcome your hatred," Denrick said.

Red tears, mingled with twilight, filled our eyes. Unlike normal humans our tears look like blood. Despair seized us. The fangs of despondency gnashed our soul.

"Do we have hope?" Woethief asked. "We are vile. We are damned for what we want to do to our foe!"

<!-- Psalm 103:12 -->

"There is hope. There's a greater Woethief than you, the Son of Faithful's God. He'll not bear the feelings of guilt, as you have done for others. He'll bear your actual guilt so far away that it will never be found again."

"Why would God hear our prayer? What can we pay to be free of guilt?" Woethief asked.

<!-- Romans 3:27-28 -->

"He loves making helpless rebels his friends," Den replied. "There's nothing you can give to earn your salvation."

"If we cannot pay, how can we get him to take our guilt?" Woethief asked.

<!-- Romans 10:8-13, 2 Corinthians 7:10-11 -->

"Believe that he is able to remove it and forsake your wrongdoing," Den replied.

"How can we leave our bad way? We are vile. How can we leave our want for re-venge?" Nyla asked.

**"He is more powerful than your evil,"** Theila said. **"He will aid you."**

"What would be-come of him if he took our e-vil?" Woethief asked.

<!-- 2 Corinthians 5:21 -->

"He was murdered to bear your damnation in your stead. He was innocent of all evil, so bad people like us can receive his perfection," Denrick said.

"What good can a dead man do?" skepticism poured from Nyla.

"He's no longer dead," Den's internal light increased in vibrancy. "God resurrected him to prove his satisfaction with the sacrifice. He reigns over all worlds and demands the worship of all."

"If he died for the Ild world, are we not left out?" Nyla asked. "We are not Ild."

<!-- Romans 5:12-19 -->

"There are many worlds. All worlds and all peoples descended from one initial world. Salvation is available to all."

"But we are two-mind-ed," Nyla said. "We not know if we are one or two peo-ple. How can he love?"

<!-- John 6:37 -->

"He knows. If you come to him, he will not turn you away," Denrick said.

We talked for another hour. We learned startling things about our world. Like most world-stewards, Centin and Natia used their power to gain worshipers. World-stewards who abused their God-given powers were cursed. We resolved that if Gemma and Woethief regained their rightful rule from Centin and Natia, we would not demand the worship of our people.

Of all the things Denrick said, nothing was more intolerable than Den telling us to give up our hatred. We deserved revenge. Forgiveness was impossible!

## 14 The Other Princess

When we left Faithful's house, we memorized the streets so we could return alone. Denrick and Theila said nothing as we journeyed. Street-merchants sold drab pennants, snacks, and perfumes to celebrants.

Overpriced, clay figurines depicted Silver's beauty with varying levels of accuracy. Despite her eyes, the city admired our sister. Satisfaction filled us at recreating Silver to be so lovely.

Closer to the palaces, guards recognized us and escorted us to the Great Hall. The crowd parted for as as they drank in Gemma's gorgeous form. At the foot of the dais, a guard helped us up. Den stayed at the front of the crowd.

Queen Syreeta's heart filled with relief. She had thought we were dead.

**"Praise Natia,"** Syreeta exclaimed. **"She has brought back our daughter from the dead."**

We saluted our new mother.

"Our friend helped," we said as we pointed to Denrick. He saluted the Queen, who recognized him as Faithful's son.

We took our place next to Gemma, who wore the crown of the Princess. They chose her as the heir and us as her little sister.

Centin placed his hand on our shoulder. "Not dead, are we?" Nyla asked Centin through clenched teeth. "When you try to kill, do not do it with drunk men." We formed a shield of twilight, so no one else would hear our words.

*'Did you know they would attack me?'* Nyla thought to Gemma. *' Was it like when you killed us all those times?'*

*'No, I swear to you that I did not know,'* Gemma thought. She had forgotten the shame of repeatedly murdering us.

Woethief probed Gemma's mind to determine whether she spoke the truth. As she did, Woethief encountered the three innocent lives. They could not speak, but sent simple emotions to their mother's mind.

*'Please, Nyla, do not do anything to harm Faithful's children. One day they will inherit the kingdom from me,'* Gemma's mind pleaded.

*'And what of our child? What of the opportunity she is missing?'* Nyla thought back.

*'I didn't choose this. I know you are angry, but please give me the chance to become what Faithful wanted me to be,'* Gemma thought to us.

We hated her. For the sake of Faithful's children, we kept our hands at our side. Our chest heaved with anger.

Centin stepped up to the rumbler and said, **"This girl is unfit to be Princess. She entered the tournament, contrary to the rule which stated that the victor would become the royal son. Nowhere did the rules permit her, a girl, to compete. Thus, she is ineligible for heirship. The widow of the man she killed became Princess in her stead."**

"No rule claimed we need to be a man. We won in a fair way," Nyla shouted. "You take re-ward of our fight, for we are a girl and un-ani-mal," The long words hurt to say. Calling ourself "we" confused our audience.

Gemma's mind touched ours, *"Please, don't put yourself in danger. I'll do all I can to help you, but provoking Centin will only make him hurt you."*

*'What do you suggest?'* Nyla thought to her.

*'I don't know,'* she thought back. *'I'll find some way to fix this.'* With our thermal vision, we saw Gemma's knees trembling beneath her clothes. Everyone, except his new wife, feared Centin.

*'Do not fail us. Were you not with child we would slay you,'* Nyla threatened Gemma's mind.

**"You're a fool, Nyla,"** Centin whispered. **"Do you really think you deserve to be Princess?"**

Centin struck our face with his claws. Blood spattered onto our dress and the force of his blow sent us to our knees. Gemma and Syreeta thrummed screams into the ground.

"Why do you hate?" Woethief whimpered.

**"I hate you because of all the pain you inflict on my people. You will not go unpunished,"** Centin replied.

When he mentioned the pain we inflicted, he meant the crimes he transferred to Woe. He hated Woethief because he hated himself.

**"Please, just let her go,"** Gemma pleaded.

Centin disregarded her entreaties and lifted us by the throat. The crowd gasped. Woethief's residual affection for him prevented us from fighting back. A squad of Centin's guards blocked Denrick from coming to our aid.

![Centin Lifted WoeNyl by her Throat](../Illustrations/WoethiefBook17.png)

<!-- Alt Text: A tall man, with bat-ears and bat-wings, holds a human girl above the ground by her throat. She grasps his hand to try to pry herself free. The woman has curly hair and a silk dress. The man has a silver bangle on his right elbow. -->

We couldn't plead for help. Centin's claws tightened as Theila's dress hardened. The fabric around our throat kept Centin from strangling us.

**"Show pity, Centin,"** Syreeta begged. She and Gemma feared Centin too much to help us.

**"You are unfit to be a princess,"** Centin said as he threw us to the ground. **"You are fit only to be a slave."**

Centin whispered to a guard, who ordered Denrick to leave. We nodded to him, not wanting our nephew to be harmed. The crowd stared at us.

**"Come here, Princess Silver,"** Centin ordered.

**"Yes, my lord,"** Gemma replied.

Centin shoved us to the ground, at Gemma's feet. **"Swear your eternal service to your Princess,"** Centin ordered.

*'How can he make us do this?'* Woethief wondered. *'I thought we were free.'*

We trembled with rage, but to protect our daughter, we obeyed.

We placed Gemma's left foot on our head to show our submission. "We are owned by you. We will be true to you and wait on you."

**"I accept your service,"** Gemma said. **"Rise, Nyla Woethief, and attend me."**

For the rest of the ceremony, we stood at Gemma's left side. We sniffled.

*'Congratulations, Nyla you certainly made a mess of that situation,'* Woethief chided.

*'Centin deserved a reprimand,'* Nyla retorted.

*'Your reprimand nearly got us killed. You put Denrick and Crystal in danger,'* Woethief thought. *'You threatened to kill my twin sister.'*

Our head drooped onto our chest. We blamed ourself for Centin's harsh punishment.

*'Why did we lose our temper?'* Nyla wondered. *'If we were calm, Centin might not have humiliated us.'*

## 15 Servitude

After the ceremony, we and Gemma retired to her palatial bower.

**"Help me change into something more comfortable,"** Gemma ordered.

**"Aye,"** we said.

**"That's 'Yes, Princess,'"** Gemma scolded. **"Say it."** She knew we could not pronounce those words.

"Yeth, Printheth," we lisped as the harsh sounds stung our throat.

We unfastened her dress and helped her to change into a dressing gown.

*'She should be serving us,'* Nyla thought. *'She should pay us back for all the hurt she has caused.'*

**"I will rest now,"** Gemma said. **"You may do so too."**

"Thank you," we said. Politeness kept Gemma's fists at bay most of the time.

We curled up in a corner of the room with no blanket to warm us.

***

A terror dream woke us from our nap. In the quiet, memories of our users haunted us. In vivid detail, we relived murders, thefts, and betrayals.

*'Why did they do this to us?'* Woethief wondered.

*'Hush, dear,'* Nyla replied. *'I am with you.'*

We hugged ourself. Nyla's embrace comforted Woe.

The memory of mugging Faithful attacked us. Thugs beat a wonderful man, but Woethief had been hired to bear their shame. Most of our users were criminals or profligates. After they hurt others, Woethief offered a permanent release from shame, which wine could not compete with.

*'It is not fair,'* Woethief thought. '*None of it is fair. They hurt the best man and now he is gone.'*

*'What they did is not your fault,'* Nyla replied. *'I know we argue, but I love you. I will always be here for you.'*

We did not know whether we were sisters or the same person. Despite our confusion, we loved each other. No one could ask for a better friend than Nyla.

Faithful's tormented childhood surged through Woe. She saw herself in the memories. Men with whips and paddles beat us. Beautiful women were paid to speak unkind words to us and make us feel worthless. *'How could they say that to us, I mean him?'* Woethief demanded. *'He did not deserve the Forges. He deserved to be Prince. I miss him.'*

*'I miss him too,'* Nyla replied as she wiped red tears from our eyes. *'I hope someone rings the bell in the Golden Death.'*

*'Me too,'* Woethief thought.

The crash of breaking pottery broke us out of our musings. We clutched our baton.

**"Help me!"** Gemma pleaded as she clutched her side. Pain filled her aura.

## 16 A Chance at Revenge

Blood poured from a wound in Gemma's side. A bullet had entered between her ribs.

"How did a gun hit you?" we asked.

**"Spring-gun on table,"** she said. A jar had hidden a spring-loaded gun. When Gemma reached for a drink of water, the gun shot.

Gemma swayed and fell into our arms. We brought her to her couch, as a trail of red decorated the floor.

*'She is losing blood too quickly,'* Woethief warned.

We wanted to kill Gemma or maybe let her die, but Woethief made us lift her gown to survey her wound. The shot had missed her organs, but the wound gushed blood.

*'This is our chance to kill her. No one will know,'* Nyla thought.

Terror filled Gemma's eyes. She doubted we would help her.

*'If we let her die, are we the murderer or is the assassin?'* Nyla wondered.

We placed our hand against Gemma's side to quell the bleeding.

*'Do you really think we would be innocent?'* Woethief asked. '*Theila will know what happened.'*

Agony from Gemma's wound coursed into our body as Woethief stole her pain. We pressed harder against the wound.

*'What will we do?'* Woethief asked Nyla's mind.

"We hate you!" Nyla hissed.

Gemma could not resist if we decided to throttle her. She was helpless, at our mercy. Her eyes pleaded with us to spare her, eyes identical to our own.

We wanted her dead. Then we thought of her children, Faithful's children. We lacked the strength to save them from ourself.

*'God of Faithful, we cannot forgive her. She has wronged us sorely. Give us power to forgive and we will serve you evermore,'* our minds begged.

Woethief took control of our body. We shapeshifted new skin over Gemma's wound to quell the torrent of blood.

Gemma's eyes closed.

"No, we do not hate you. Do not leave," we said aloud, ashamed of our hatred.

*'God! Save her. We cannot bear for Faithful's children to die,'* our minds prayed.

As we knitted Gemma's flesh back together, exhaustion and thirst seized us.

*'It's too late!'* Nyla screamed inside us.

*'No, we must keep trying,'* Woethief thought back.

We slumped to the floor and laid our head on the couch beside Gemma's.

<!--Inspired by Luke 18:9-14 -->

*'We know we do not deserve your forgiveness,'* our minds cried in prayer. *'We know you can save us from ourself. We know you can save Gemma. We need you to be our Woethief.'*

Our hands convulsed with agony but our soul was freed from the hatred that enslaved us. Tears spilled from our eyes onto the cheeks of our enemy. Where our tears fell, patches of life returned to Gemma's sallow face.

Gemma's dread-filled eyes fluttered open. As twin, world-stewards, we and Gemma could shapeshift and heal each other. We thanked God that our special power could heal our sister.

**"How soon will you k… it be before you k-k-k-kill me?"** Gemma trembled.

We examined her. Where the wound had been, only perfect, silky fur remained. Fear plagued Gemma's pretty face. We smoothed away a stray lock from her brow. Our healing had left no scars. She still looked perfect.

"We for-give you," we declared aloud, through our cracked lips. Woethief and Nyla felt unusual unity.

**"Please, don't play with me. If I am to die, make it quick,"** she begged.

"We for-give you," God empowered us to forgive her, just as he forgave us.

**"What? Why… why you would forgive me?"** Gemma stammered. **"I don't deserve it. If you kill me here, no one will blame you."**

*'No one will think we're the assassin,'* Nyla suggested.

*'I do long for freedom from Gemma's shame,'* Woethief thought.

*'God give us strength.'* our minds prayed together. *'We cannot resist ourself on our own. We want your peace.'*

We squeezed Gemma's hand and smiled at her. "We love you. How can we kill you?" Woethief asked. "You will be our queen one day."

*'Did you really just say that?'* Nyla thought. *'You just acknowledged her usurpation as legitimate.'*

*'It was not her fault.'* Woethief observed. *'I long to repay Faithful's children for their father's kindness.'*

*'What of Crystal?'* Nyla asked. *'She deserves a better life. Gemma might hurt her too.'*

*'Denrick and Theila will give her a better life,'* Woethief thought. *'She needs no power or riches, she needs their tenderness and affection.'*

Crystal slept peacefully, exhausted by trying to read the auras in the room.

Gemma left the couch and prostrated herself before us. She lifted our left foot and placed it on her head in the ultimate form of submission.

![Gemma Lifted WoeNyl's Left Foot](../Illustrations/WoethiefBook18.png)

<!-- Alt Text: A woman, with bat-ears and bat-wings, kneels before a human woman. The bat-woman holds the human's left foot on top of her head as the human woman looks surprised. Behind the women, shelves hold baskets and jars of food. -->

**"I swear my…"** Gemma began.

We raised her to her claw-feet. "No, we for-give for free," Woethief said.

*'Where did that come from?'* Nyla's mind asked.

**"B-b-but that that's impossible,"** Gemma stammered.

"Aye," Woethief replied.

Despite our quivering and Faithful's fear of beauty, we embraced our mistress. We marveled at the power of Faithful's God. He conquered our murderous soul with his love. 

*'Could God change Gemma or Centin too?'* Woethief wondered.

**"I knew you would do the right thing,"** Theila thrummed.

Red tears decorated Gemma's cheeks. We realized how much Gemma resembled us. Faithful did not lie when he said we were lovely. His bride was modeled after us. That knowledge stung, yet made it easier to sympathize with Gemma.

We guzzled four cups of water, after ensuring there were no more traps. Shapeshifting Gemma's wound had dehydrated us, since we used water from our own body.

We longed to see Denrick. Crystal thought sweet words to us to strengthen us. The fabric of the dress massaged our shoulders.

## 17 Denrick Becomes Gemma's Guard

After Gemma recovered, she called for help. Servants fetched guards, who examined the trap and led us to another room. Despite the danger, Centin convinced the King and Queen that assigning a guard to Gemma was unnecessary. Who could want to harm a new princess that no one had heard of before the feast?

We knew Centin's style. He was using Gemma as bait to catch the assassin. We hoped he succeeded.

**"What are we going to do?"** Gemma asked in her new bower, which had been swept for traps.

"Den will help you," we said.

**"I don't know anything about him,"** Gemma replied. **"How can I trust him?"**

"Den loved Faith," we said. "Now, he will love you."

Gemma probed our mind. Since Faithful had raised Denrick to be honest, she decided to trust him too. She asked a servant to fetch him from the storehouse, where he worked as a guard.

When Denrick arrived, Gemma scrutinized him. His bulk frightened her. Anger surged in him as he met the woman who gave a love potion to his father.

**"Will you guard me,"** Gemma asked. **"Faithful loved you, so I ask you to honor that bond."**

"Aye," Denrick said. He didn't want to help Gemma, but he wanted to make Faithful proud. "You are my e-ba now. I will not let harm come to you."

**"Though you are an unanimal, Faithful heard something in you."** Gemma said. **"Prove your worth to me and in return, I will free Nyla Woethief from my service in thirty years."**

"Make it five," Denrick said. He hated Gemma's gall.

We pleaded with Gemma with our eyes. We longed for freedom and peace.

**"We shall hear,"** Gemma replied. **"I will pay you for your service. You knew Faithful, so you will tell my children stories of him. They will grow to be like him."**

"I will teach them," Denrick replied. "I will help you to help them," Denrick saluted Gemma. To overcome his rage, he channeled love for Faithful and his new siblings toward Gemma.

Denrick hugged us, relieved that we were safe, and joy permeated our soul.

"We did it. We for-gave her," both halves of us said. "We bowed to the great Woe-thief."

Gladness filled Denrick's heart as we recounted Gemma's visit.

"I have prayed for this for so long," Den's lips turned up. His aura of happiness made us smile. "I will forgive her too."

Den turned to Gemma and held out his hand. As a sign of goodwill, Gemma squeezed his fingers with her own trembling claws.

*'Embrace him,'* Woethief whispered to Gemma's mind. *'Denrick will love you as a mother. Make it easy for him.'*

Gemma awkwardly hugged Denrick and felt his bulletproof vest. **"I love you, son,"** she forced herself to say. She inwardly recoiled from him. She despised humans.

"I will make you proud," Denrick replied.

We and Den occupied an apartment outside Gemma's bower, so no enemies could approach her unnoticed. After we left Gemma in her room, we slumped to the floor.

Den locked the outer doors. Violent sobs shook us as memories of murders attacked us. Den embraced us. Our face rested on his shoulder.

"What's wrong?" Den asked.

"We feel the guilt of bad men, who killed good men. The guilt came with no warn-ing," Woethief wept.

"The memories are not yours," Denrick said. "You are a kind person. You would not harm innocent people."

"How do you know that we did not kill them?" Woethief asked. Our users' shame mingled with our own memories.

**"I know your mind,"** Theila's dress thrummed. **"The shame from these murders does not match your real memories. You are a victim, not a murderess. We'll find a way to gain your freedom and your throne."**

*'I want a family more than the throne,'* Woethief thought.

*'You might think that, Woe, but I want the throne,'* Nyla retorted.

**"Gemma destroyed our family, but Faithful rebuilt it,"** Theila said. **"You have the family you long for."**

"Vow that you will not kill Gem to free me. I will not let a child of Faith be hurt," Woethief said.

"I want the throne, but like Woe-thief, we do not want Gem hurt by you," Nyla added.

"Of course," Denrick replied. "She is my father's wife. Her well-being is my well-being"

We kissed Denrick's forehead. "We love you."

"I love you too, WoeNyl. I suppose you're my aunt now," he kissed our cheek tenderly. Despite the lack of blood connecting us, Faithful's love wove a family web between us.

"Sleep now, aunt. Theila and I will guard Silver while you rest." Denrick said as he stood by the door.

*'WoeNyl,'* Theila thought. *'May I give you a pleasant dream about Crystal? I don't want you to think I am manipulating you into liking me. I can give you a terror-dream about me if you would rather not be my friend.'*

*'Theila,'* Nyla thought back. *'You've obviously infiltrated our minds already. If you want to give us a happy dream for a change, we won't object.'*

*'I didn't infiltrate. I've felt connected to your life, since you created me. I think your connection to me is what makes you come back to life. Whenever you've died, I felt like your soul was resting in my body for safe-keeping. It's such a relief when it returns to you. I've never been able to disconnect. I'm sorry that I vex you,'* Theila replied.

*'We want to know you better before you give us your gift,'* Woethief thought. *'Gemma has hurt our minds so much that it is hard to believe someone can be kind.'*

As night fog entered the room, we closed our eyes to sleep. The dream relaxed us. In it, Theila played tag with Crystal and Faithful's three children, who never caught her.

## 18 Coronation Day

**"Get up!"** Gemma's gruff thrumming dragged us away from a lovely conversation with Crystal.

We rubbed our eyes. When we saw Gemma, we bolted upright in a salute before she could hit us. Her comeliness aroused bad memories of Faithful's childhood.

**"I'll disguise you as a bat-girl for my ceremony,"** Gemma said. **"Having a human servant is embarrassing?"**

Our head drooped.

**"I got a dress for you to wear,"** Gemma said. **"It's finer than you deserve, but I need to keep up appearances."**

We crawled out of bed. Our hair cascaded down our shoulders in springy ringlets.

*'I wish she would not cover our hair,'* Nyla thought. *'Why can we not display our one lovely feature?'*

**"I'll dress you,"** Gemma stated.

"We do not need help," Nyla said.

**"I know that, stupid!"** Gemma said. **"I want to watch so you don't hide any weapons from me. Now, do as you're told."**

Den left the room. We slipped off our gown. Then Gemma made us remove our under-things. We concealed no weapons, only scars.

After we stepped into a pool, Gemma turned the water into fur to fill our gaunt frame and make us slender rather than skeletal. New wings emerged from our back. Fangs concealed our broken teeth and pointed ears replaced our curls. While Gemma morphed us into a bat-spider, Theila wove herself into the dress, which Gemma had brought. Its open back accommodated our new wings. With Gemma's aid, we donned Theila, the gown. Gemma did not realize that the fabric was alive.

**"You're half-pretty now. Pity Faithful didn't hear you like this,"** Gemma said. **"Well, pity for you, but not for me or I wouldn't have got him. The better woman won. Though, perhaps Centin wouldn't have left you if your other disguise hadn't dissolved."**

*'We told Centin of our disguise when we married him. He knew we were a hideous monster underneath, but he chose to give us a love potion! Why did you make our disguise dissolve?'* Nyla thought to her, but Gemma ignored our mind-speech.

"You made Faith wed you, Gem…" Nyla began.

**"What did you just call me?"** Gemma interrupted.

We did not reply. She wished to be called 'Silver', which we could not say.

**"Answer me brat!"** she wrenched our wrist. We reeled into a stone pillar. Blood oozed from our right temple. We shrieked.

At the sound of our cry, Den burst into the room with his pistol drawn.

**"You can't shoot me. I'm with child,"** Gemma cowered behind her offspring.

Den holstered his pistol and glared into Gemma's eyes.

"I am your child," he said. "I thought a foe would hurt you. Pray, heal her." He pointed at us.

**"Why should I?"** Gemma asked.

"E-bu would want you to," Den put his hand on Gemma's shoulder. "We will help you learn to be kind."

Gemma humphed. She led us back to the pool and placed her hand on our wounded head. Her touch soothed us. She surprised us when she used her powers to heal us instead of covering the wounds with a façade.

We hugged her. "Thank you," Woethief said. "That felt good."

Gemma's lip turned up for a moment, then her smile faded.

"We will keep you from harm," we promised as we concealed a pistol, knife, and baton beneath folds in our gown.

After Gemma healed us, a dozen slave-girls entered to dress her. First, they wove silver cords into her fur, then fine silk from their wrists around her to form a magnificent gown. While they beautified her, we ensured no one harmed the Princess. Den remained in the next room, for obvious reasons.

After Gemma's transformation into a beauty that exceeded Natia's, we joined the procession on its way to the palace.

One hundred full-blood virgins and 500 elite soldiers escorted us through the streets. We had never seen so many rifles in one place. We were adjacent to Gemma to protect her from her "protectors."

"You are good to hear," Woethief complimented Gemma.

**"Flattery doesn't become you Nyla,"** Gemma said.

"We tell the truth," she was gorgeous in her fine raiment.

*'Why does she heap scorn upon our kindness?'* Woethief wondered.

*'We shouldn't waste kindness on her,'* Nyla thought.

**"Shut up, slave,"** Gemma said. **"I don't want you spoiling my celebration with your addle-brained muttering. Be grateful that I allowed you to be with me today, you good-for-nothing oaf."**

She derided us, not because we irritated her, but because she would not grieve over Faithful in the honest form of tears. She vented with spiteful words instead.

*'Why does she not give me her grief about Faithful?'* Woethief wondered.

"We ache for Faith too," Woethief placed our hand on her shoulder. "Let me take the pain from you."

**"No, I don't want you to take it."** Gemma snapped.

"Give up half of it, dear," Woethief tried to soothe her.

**"No, that's final!"** Gemma said.

We did not reply, just walked on and scrutinized the crowd. Any of the thousands of people thronged at the edges of the streets might draw a pistol or throw a rock or something worse. If they threatened Faithful's children, they would feel our baton!

**"Why are you being so kind to me?"** Gemma asked as we paraded in front of butcher shops and grocery stores.

"We love you. You are our twin," Nyla said.

**"But after all I've done to you, still you protect me?"** Gemma asked.

Nyla wanted to run from her or tell her that she hated her, but Woethief quelled Nyla's anger. We prayed for relief from our hatred.

"We for-gave you," Nyla said.

We marched on in silence. Gemma smiled and threw fragrant mushrooms at the crowd from a large basket carried by two little girls. The left girl's straight, black fur contrasted with the right girl's curly, brown fur. The sweet odor of the fungus permeated our clothes. Auras of malice and tension mingled with the crowd's excitement.

At a narrow part of Herald Street, a surge of gawkers separated Den from us. Soldiers pushed them back with their shields. More evil auras filtered into the crowd.

*'Something's not right,'* Woethief thought to Gemma.

Gemma swooned. Before her head could hit the ground, we caught it. We examined her body and gritted our fangs.

![Gemma Swooned](../Illustrations/WoethiefBook21.png)

<!-- Alt Text: A gorgeous woman, with bat-wings and pointed bat-ears, swoons. She is wearing a bejeweled dress with a wide skirt. A young bat-woman rushes behind her to catch the falling  princess. The second woman has a simple, silk dress and round bat-ears.-->

"Den, where are you?" Nyla yelled as we extracted a poison dart from Gemma's arm.

Den did not answer. Clay bottles shattered around us. Smoke filled the air. Only desperate criminals used firebombs. Soldiers formed around the King and Queen, but walls of flame kept them from reaching Gemma.

We healed the area where the needle hit her and Gemma opened her eyes. We shrouded ourselves in twilight fog.

**"What's happening?"** Gemma asked as we dragged her away from the fire. She gasped in clean air.

"No time, Gem" Nyla coughed. In our fatigue, we called Gemma by her old name, instead of 'Silver'. Gemma did not notice. "If we look…" Woethief panted. "like you…"

**"Why?"** Gemma asked.

"We will be killed by them," *GASP.* "You will go free." *Gasp.* "They will think we are you," Woethief said.

She stared at us. We slapped her to jolt her into action. We felt guilty for enjoying the smack. To make us a replica of Silver, she morphed our disguise. We transformed her into her old form: Gemma the spider.

"We will draw them off," *Wheeze.* "You will run," Woethief said.

Gemma nodded.

We threw a cloak, discarded in the confusion, over Gemma and fled through a gap in the flames. While the crowd dispersed, we followed the parade's planned route into an ambush, so Gemma could escape. Our shrieks attracted the mob's attention.

*'How long can we run before they catch us?'* Nyla wondered.

The exertion cleared our lungs from the smoke.

*'Was it right to endanger Crystal for Gemma's sake?'* Woethief wondered.

Bullets whistled past our ears.

*'Gemma holds three tiny lives,'* Woethief thought. *'We only hold one.'*

Our muscles screamed from running.

*'Four lives in exchange for two or three?'* Woethief thought, not knowing whether to count ourself as one or two lives.

We dodged the lunges of several attackers, but a rioter collided with our arm. We sprawled into a cart of loaf-plant and our pistol skittered out of reach. We whirled to face our enemy and broke his wrist with our baton before he could bring down his upraised axe.

We ran. Despite our new wings, we could not fly, since Gemma had not created the necessary muscles in our back.

A pellet whistled past our right ear. We veered into an alley, where we met three bat-men with weapons drawn. Hatred emanated from them. A blow from our baton felled the first.

*'Where is Den?'* Nyla wondered.

We formed a wall of twilight behind us. It stopped our pursuers but not the men in front of us.

A knife plunged toward our chest, but our dress turned the blade. Our knee drove into our opponent's groin. He collapsed and we kicked the knife from his hand. His vomit soiled the fur on our bare feet.

The third man lunged with a short sword, we sidestepped and knocked him out with our baton.

Because enemies chased us, we fled down the tunnel until we collided with a web. The threads' sticky embrace restrained us like a wrestler's headlock.

*'Stupid,'* Nyla taunted. *'We have ears and a mouth to keep us from making mistakes like this.'*

*'It is as much your fault as it is mine!'* Woethief replied. *'We know we cannot effectively use this new body.'*

We struggled to free ourself from the entanglement before its creator returned. Our clothes smelled like smoke.

*'Stop struggling, you're making it worse,'* Woethief ordered.

*'I would like to see you free us,'* Nyla retorted.

Flame, Ankor's lover, crawled toward us on her eight legs with a gun in her hand. An egg sac bounced on her back.

*'We must escape, but we cannot hurt her child,'* Woethief thought.

Flame crept closer. Pain overwhelmed us. Our twilight deafened Flame as we wriggled free of her web. We stole her anger over us defeating Ankor in a duel. We left the young mother alone and confused.

Death neared us. We knew we could not evade our attackers forever.

*'Den, where are you?'* Nyla wondered.

We emerged from the alley into a mob of Ankor's fans, who lusted for the Princess' death and by misidentification, our own. They hurled us to the ground, and lacerated our façade.

**"The Princess is an unanimal!"** one shouted angrily. Others repeated the scandalous news.

"God, Den, Thei-la, where are you?" Nyla asked.

## 19 WoeNyl Saves Theila

When we consigned ourself to death, our dress separated from the gown that Gemma gave us. Theila used her power as a pure-blooded twilight-person to transform into an exoskeleton. Our attackers faced the new threat of the insectoid girl whose shell contained us. Clawed hands ripped away their swords. Stones bounced off her carapace like pebbles hitting a boulder. She knocked a man out with her fist.

![Stones Bounced Off Theila's Carapce](../Illustrations/WoethiefBook22.png)

<!-- Alt Text: A young woman, with a short slip, charges ahead. She is covered by a semi-transparent exoskeleton that resembles a beetle. Her hair flies behind her and antennae peek out from her head. Stones bounce off of the armor. -->

*'God, let her save us,'* Woethief prayed.

A thrown brick ruptured her chest armor and exposed our soft flesh beneath. Theila faltered and fell. We veiled ourselves with twilight and silence. More rocks landed around us.

*'Theila, are you still alive?'* Woethief asked.

We tried to move, but her exoskeleton pinned us.

*'Yes, I'm not hurt too badly. It just stings so,'* Theila thought.

As projectiles showered down, she shielded us with her shell. Small arms and rocks battered her carapace. Shudders reverberated through us as each fist-sized stone splintered her armor. Death neared Theila. Theila's twilight flickered out as did her mind.

*'Theila, think to me?'* Woethief pleaded.

Theila gave no response.

*'God, this can't be her end,'* Nyla prayed. *'She has served you so well. Please, save us.'*

Life and blood seeped from Theila's body.

*'Theila, wake up!'* Woethief thought.

"Ungh," she groaned.

*'You are bleeding out,'* Nyla said. *'But we will not let that happen. We have plenty of blood left in us. Drink ours.'*

*'If I do, your minds will be under my control. I know that possibility terrifies you,'* Theila thought to us.

*'We don't care about that right now,'* Woethief and Nyla thought. *'We want you to live.'*

*'But you might lose control of your minds,'* Theila argued. *'I am your sister. How can I enslave you?'*

*'Theila, time is scarce. We order you to drink our blood,'* Nyla thought to her.

Theila's tongues flitted out of her exoskeleton. She missed when she tried to embed them in our wrist, where the armor was broken. On her fourth try, Theila's tentacle-like tongues siphoned blood from our veins.

Agony seized us. We clenched Theila's shoulder. As searing venom replaced our blood, our head grew faint. Theila consumed our Ildylian-disguise to heal herself. The pain ceased.

Our minds linked with Theila's in indelible sisterhood. Theila's tongues remained in our flesh, but now saturated us with peace and healing. We loved Theila with all our soul. When she extracted her tongues, contentment lingered.

Thrown stones slammed into Theila, but our blood had healed her armor. She rose and charged through the streets as our enemies scampered away. To her foes, she was terror incarnate, but to us, our valiant sister.

A row of stone-throwers checked our progress. Missiles battered Theila's head. Despite her armor, she collapsed. As she fainted, her armor opened. We slid out from her shell and clothed ourself with twilight and the gown from Gemma.

We shrouded ourselves in twilight fog. As Theila's life dimmed, so did her twilight. We dragged Theila backward as we crouched.

*'Please, spare us even if you take Theila. We cannot lose Crystal!'* Woethief prayed to God and hoped for Denrick's aid.

As a stone crushed her head, the life of Theila and her twilight extinguished at the same instant. Our twilight muffled our screams.

*'God, why?'* Nyla asked. *'We have begun to serve you and now, you take our friend. Is our Crystal next?'*

## 20 Den's Rescue

Denrick flanked the stone-throwers and pistol-whipped their commander's head. His fists battered his enemies' faces as they had done to Theila. Pain from him and those he brutalized washed over Woe. Bullets slammed into Den's bulletproof vest, but he ignored them. Nothing could separate him from the girls he loved.

His massive hands encircled our waists, and he threw Theila and us over each shoulder. As he fled, a hurled rock bruised our ankle. Den's twilight fog deafened anyone who challenged our flight.

![Den Threw WoeNyl and Theila over His Shoulders](../Illustrations/WoethiefBook23.png)

<!-- Alt Text: A large man, with a simple tunic and belt, carries a girl and a woman over his shoulders. Both females are limp. He has dark skin, short hair, and large muscles. The woman has a simple dress and curly hair.  The girl has clear, insectoid wings, and three-toed feet. -->

After hours of travel, we arrived in a hidden cavern, provisioned for emergencies. Denrick placed us beside one another and his anger vanished. He threw himself on Theila's diminutive corpse and wept as no other man weeps, unless he is alone. We placed our hand on his shoulder.

"I loved you, Theila." Den mourned over her. "To-day you came of age, and I want-ed to wed you."

He turned to us. "They took Thei-la from me. I want to die and meet her right now. I don't want to wait a life-time to meet her again."

We threw our arms around him and kissed his cheek. Though he would not let Woethief steal his pain like she did for others, Crystal's presence bore extraordinary power to comfort him. We wept in his embrace.

"We love you Den," Woethief said. "We cannot love how Thei-la loved you, but you are our friend. God will be true."

"Where can God be in my pain?" His question rocked us. Denrick rarely wavered in anything. Now he faltered.

"While we live, we will hope. Our loved one died, but not our God. We have our child," Woethief said. The thought of Crystal Comfort cheered us. In our distress, Comfort wielded her power to make us slumber.

## 21 Den's Despair

We awoke with our head on a pile of clothes. Pain radiated through our swollen ankle. We listened to Denrick's slumbering mind. Doubt tormented his subconscious and sucked him away from the God he served. We clutched his hand. His presence made us feel safe, though our world had shattered.

*'Nyla, Den will mend the tatters of our life,'* Woethief thought. *'He will give us stability and love Crystal like a daughter.'*

*'Den will reject us too,'* Nyla thought. *'We are too broken to be loved! The sooner you realize that, the better off we will be.'*

*'Den will not leave us,'* Woethief replied. *'He will help our minds to heal.'*

*'This argument proves we are broken. Whole people do not exist as two. You will not heal now that Theila is dead,'* Nyla thought. *'Our only hope is that Crystal has a better life than us.'*

Despite her argument, Nyla acquiesced to Woethief's hope. Woethief usually won.

Denrick stirred, so we released his hand. We did not want to embarrass him.

In the glow of our twilight, we scrounged for medical supplies and food. We limped along to collect the ingredients for our breakfast. Despite Den's hunger, he touched no food. We devoured the simple fare. When we begged Den to eat, he ignored us. Grief and fear numbed him. He stroked Theila's broken face incessantly.

We retreated to a corner to nurse our wounds and change our garments. Our foot was purple and swollen. We washed it with stinging ointment to ward off infection.

We crawled to Denrick with a medical kit. When we loosened his tunic and vest, to survey his wounds, he did not object. A bullet was lodged in his right shoulder. Woethief guided us as Nyla revolted against the gory task. We sterilized a small knife and pincers. As we extracted the bullet, Den gave no reaction. After we applied a sterilizing ointment and clotting agent, we stitched each wound closed.

"Den, dear, put it on," we held a blanket out to him.

He continued in his stupor. With difficulty, we lifted him enough to wrap the blanket around his back.

*'God, please bring him out of this daze,'* Woethief prayed. *'We need him. We cannot proceed alone.'*

![WoeNyl Tends to Den](../Illustrations/WoethiefBook24.png)

<!-- Alt Text: A giant man, with short hair and dark skin, sleeps on his right side. A young woman, with curly hair, drapes a blanket over his shoulder. -->

We nibbled our supplies and forced Denrick to eat. He chewed mindlessly, but descended into apathy. Water from the ceiling dampened everything.

Theila's rotting flesh reeked like mold and rancid crab. Denrick grew violent when we tried to remove Theila's body to another cavern. Four days passed. Our supplies dwindled. They would run out in a week or two.

Anxiety, doubt, and loneliness assaulted our minds like water dripping from a stalactite. Infection overtook our ankle. Crystal's mind was quiet and our headache was loud.

By the tenth day after Theila's death, Denrick refused to eat or speak. Our fever broke as Denrick's set in. As sleep enveloped us, we wondered if the mob still sought us.

## 22 Hope

We woke to Crystal's thinking.

*'Eba, when will you bring Aunt Theila back to life?'* Crystal asked.

*'What do you mean?'* Nyla asked. *'She cannot come back.'*

*'Why not?'* Crystal asked. *'Theila brought you back to life. Can't you feel your connection to her?'*

When we concentrated, we felt Theila's faint aura.

*'Is she in the same emptiness we felt when we died?'* Nyla asked.

*'Of course not,'* Crystal thought. *'I'm sorry you haven't felt my mind much, but I've been busy crafting dreams for Theila.'*

*'What are you?'* Woethief asked. *'How do you know so much?'*

*'I'm just your daughter,'* Crystal thought. *'Theila says I'm special because of you and my ebu. You're powerful too. You just haven't learned to use your power, so Theila's had to direct it. She taught me lots of things.'*

*'How can I raise her from death?'* Woethief asked.

*'Finish her design,'* Crystal Comfort thought. *'When you made her, you made a new type of people. You can decide that they come back to life, if they are tethered to a friend.'*

Before we could ask more questions, Crystal fell asleep. Her power thrilled and saddened us. We would have been like her, if Gemma had not hurt us.

We grasped Den's arm to wake him.

"Thei-la will live!" Woethief said.

"You are daft," Den replied.

"No, we will bring her back," Woethief strove to convince him.

"You can't lift her from the dead!" Denrick said.

"Thei-la will not be dead for long." Woethief claimed. "When her kind come of age, they are dead for a fort-night, then come out grown up."

"How do you know?" Den demanded.

"We were told by our child," Woethief answered. "We can pray that we are right."

Den said nothing, so we did not know what he thought of Woethief's plan.

Before we slept, we concentrated on the metamorphosis of Theila's body. Nyla thought it a vain effort, but joined in because she lacked a better plan.

***

The activity of Denrick's mind woke us. We sat up, bleary-eyed. He leaned over Theila's ruined frame and shivered with fever.

![Den Leaned over Theila's Ruined Frame](../Illustrations/WoethiefBook25.png)

<!-- Alt Text: A young woman, with curly, dark hair, leans on her elbows as she looks to her left. A giant man holds a blanket around his shoulders and stares at a dead, insectoid girl. The girl is draped in a blanket and an exoskeleton covers her limp body. -->

"Nyl-a told me you will be re-born in an hour. I don't know if you will, but I love you. You were a true friend. They took your life too soon," Den said.

We placed our hand on his shoulder.

"Thei-la will come back." Nyla declared with newfound optimism.

<!--Inspired by John 11:20-27 -->

"I know that Thei-la will come back at the end of the worlds, but I need her now," Denrick said.

"God can bring her back now or then. Both are not hard. Have faith," Nyla replied.

"Faith in what? You or God?" Den asked.

"In God," Nyla said. Answering questions about our new faith flustered us. "We are on-ly a tool for God." Our role as a world-steward meant we were entrusted with powers, so we could help the world we were responsible for.

"Do you think that I am wrong to doubt God?" Den asked.

"We doubt too, but God can beat our doubt if we pray," Woethief said.

"Then pray," Denrick said.

We implored God to save our beloved Theila, with as much desperation as if we ourselves were dying. We hoped that God understood our lisped prayers. Indescribable moans escaped from Den's throat. Sweat soaked his back.

<!-- Inspired by John 11:35 -->

We wept.

## 23 Rising

Theila's body rustled. We stared and hoped we had not imagined her movements. Bulges formed in Theila's right wrist as a creature probed for an escape.

We limped to Theila's side. In a splatter of filth, the outer shell of the arm burst and a slimy hand groped for a hold on the carcass's stomach. Fingers from the other hand tugged on the armor, but the shell around them interfered. The creature's mind pleaded for help to escape before it would suffocate. Denrick and Nyla would not touch the thing, so Woethief tore at the hole in Theila's shoulder to aid the creature's exodus.

We held our breath until a blood-smeared head emerged and sucked air into depleted lungs. As we pulled the thing out more, a section of exoskeleton snapped at its neck. The creature wailed. Breathing no longer presented a problem.

Since the creature had just come to life, its weak heat signature could not show us its true nature. Woethief probed its mind, but could not comprehend its babbling thoughts. Pain seared its right shoulder as another shard of armor stabbed it. The creature panicked. It thrashed at Theila's body in an attempt to free itself, but merely shredded its hands. To prevent further injury, Woethief concentrated her mind on calming its thoughts. The thing ceased its striving. Fear shone in its eyes as we held up a dagger. Den seized our arm before we could harm the corpse.

"Let go," Woethief said.

"You can't cut up Thei-la bo-dy!" Den screamed.

Nyla recoiled from the creature, but Woethief drew us closer.

"I told you Thei-la would come back," Woethief said.

"How do I know it Thei-la and not a leech?" Den asked.

"Thei-la be one," Woethief replied. We designed her to be a friendly parasite.

"What if it a trap, like when Gem turned to a bride?" Den asked.

"What if it not?" Woethief countered.

Den released our arm. We pried at Theila's shell near the left shoulder. Once both shoulders emerged, the thing rose higher out of the shell. When a flap of skin tangled its neck, the creature flicked a blade from its wrist to free itself. After it retracted the blade, the thing slid through the hole with the lubricant of blood easing its egress.

The thing fled past us into a corner. Den stood to pursue. We pressed our hand against his good shoulder.

"Let us go. We might not fright-en the poor thing," Woethief said.

Den acquiesced and stared at Theila's lifeless exoskeleton. We dragged ourself toward the creature. We opened our hands so it knew we meant no harm.

"Can you hear?" Woethief made thrumming noises. It did not respond.

*'Can you understand us?'* Woethief thought to it.

"Can you hear?" Woethief asked again.

It nodded.

"May we come near?" Woethief asked.

It nodded. We bit a piece of loaf-plant and held some out to the creature.

It wagged its head. We recognized its aura. Theila had returned.

"It will not hurt you," Nyla said.

Theila wagged her head again, so we set aside the food.

Theila scrubbed her arm with her hand, to remove the filth from her body. We soaked a clean rag in water and approached our sordid sister. Theila did not take the rag, but stared at us. We wiped her arm. Nyla recoiled from the stench, but Woethief could not suffer Theila's discomfort. Pleasure emanated from Theila at the touch of cleanliness.

We moved our fingers over her round face and encountered eyes: eyes like ours. Her body-heat increased. As we scrubbed down the neck onto the chest, it became obvious that she was now an adult.

"Den, do not come yet. We need to clean her," Nyla said.

Theila's mind reached out to us. We did not refuse her exploration. Recognition filled her. She kissed our cheeks and buried her tongues in our wrist.

*'WoeNyl, where am I? Why do I feel so strange?'* she asked.

*'We are in one of Den's safe-holds,'* Woethief replied.

*'Who is Den?'* the creature wondered.

*'Who are you?'* Nyla asked, not daring to hope.

We stroked Theila's stomach with our cloth and discovered another peculiarity. She bore no navel scar.

*'I am Theila'* she thought to us. *'I love you with everything I am. You are my stewardess. But who is Den? Should I know this person?'*

*'He is your betrothed,'* Woethief thought.

Sadness filled Theila as she realized she had forgotten who she once loved.

*'What happened to me? Why doesn't my body respond like it should?'* Her arms flailed and spasmed as she tested her new muscles.

*'What is your last memory?'* Nyla thought.

*'Pain. Terror. Carrying you away from something dangerous,'* she thought. *'I don't know why we were running. We couldn't survive their weapons. Am I dead? Did Comfort survive?'*

*'Comfort is well,'* Nyla replied. *'You died, but we designed your kind to always die before they come of age. After a fortnight they emerge as adults.'*

We wiped grime off of her legs.

*'I was dead?'* Theila asked.

*'All we know is that you are now alive. In that we rejoice,'* both of our minds replied.

*'But what if I am not Theila? What if I am false like Gemma?'* Theila asked. *'What if Centin made me to distract you?'*

*'We know your aura. You are Theila. Even if you were not, we would still love you,'* WoeNyl promised. Theila found solace in our reassurance.

With our help, she donned a dressing gown and woolen shawl. She was too small for our other clothes. We stood and beckoned Den.

![Theila Looks Lovingly at WoeNyl](../Illustrations/WoethiefBook26.png)

<!-- Alt Text: Facing away from the audience, a woman, with dark, curly hair, looks at a young woman. The young woman has light, curly hair, two antennae on her forehead, and pointed ears. She smiles lovingly at the dark-haired woman. -->

"Woe-Nyl, you are on your feet," Denrick said.

"You are too." Nyla replied, confused by his remark.

"But you hurt your foot," he said.

We turned to the woman beside us. She smiled and blushed. Her cheeks took on a purple tint.

"We were healed by her bite," Woethief said.

When Den beheld Theila, his heart stuttered. Before him stood a glorious woman. Gray ringlets, which matched Theila's skin, bounced above her shoulders and framed her circular face. Massive eyes stared at his with an uncanny perceptiveness. A small nose pointed to her full lips. A dimple on the right side of her mouth punctuated her perfect fangs and teeth. She was three-fourths of his height, yet perfectly proportioned.

"You're the most beautiful creature I've ever seen!" Den blurted in the Twilight language. Theila's mind translated for us.

"I suppose I should say the same, but I don't remember you," Theila said.

Grogginess shrouded Theila's mind in forgetfulness. We connected to her mind to clear away the confusion.

"Please, tell me who you are." Theila prodded Den.

"I am Theila's betrothed, Denrick," he said. "I brought Theila here after she, uh, was killed."

"So you do not believe that I…I am Theila?" she sounded hurt. "How can I prove to you and to myself that I am Theila?"

"We know you are," Woethief said. "We can feel your mind."

"I don't know how you could prove you're my Theila." Den wept. "I want to believe you, but you don't even know yourself."

Theila rushed forward and wrapped her arms round the crouching giant's neck. He winced when she touched his shoulder wound, but Theila did not notice. Her fingers wiped away his tears. As her head drooped, her ringlets cradled against his sweat-soaked chest. His defenses collapsed.

## 24 Den's Proposal

"I LOVE YOU!" Den's aura overflowed with the feeling.

Den's weeping ceased and Theila released him.

"Theila, is there anything you remember about me?" he asked.

She pondered. Through our connection to her mind, Theila remembered a little.

"You are my friend and you love me more than you love yourself. You grew up with me," she said.

Theila massaged her temples.

"WoeNyl says we are betrothed," Theila spoke aloud in the Twilight tongue with a singsong voice. "Your strength is unmatched, yet you are gentle. You adore me. You think me the most wonderful creature in all the worlds. You are right because you are so wise."

Theila was not bragging or flattering him. She believed everything she said and thought nothing of the strangeness of her candor. "Am I correct?" she asked.

"Yes, we are betrothed," Den replied. "Centuries ago, my foremother entered a pact with you, so…"

"Den, what are you saying? I just came of age," Theila said. "I'm not hundreds of ventings old."

"You are pure twilight. WoeNyl put your soul inside a silkwing body. Because of your pact with my foremother, I became your betrothed husband the moment I came into existence. I love you. I know I don't deserve you, and that you do not remember the pact yet, but will you marry me today?" he asked.

"What?" Theila gasped. She still could not access all of her memories. We felt the same confusion after each of our resurrections.

"Will you marry me today?" Den repeated.

"You're like a brother," Theila said. "We grew up together."

Theila's confusion kept her from responding affectionately to Den.

"But you've said you loved me before," Denrick said. "I've always loved you."

"I have nothing to offer you, Den. I am not pretty and my race makes me repugnant to everyone in this world," Theila said.

We concealed our smirk with our hand. The notion that she was not pretty was ridiculous. Only Gemma paralleled her.

"You're my everything, Theila," Denrick said. "I live to please you. If I am distasteful, tell me. I wish you to be happy."

"I am so confused" Theila said. "I know that you speak the truth, but my head feels hazy. Am I truly desirable?"

"You cannot imagine how perfect you are," Denrick said.

Theila leaned her head on Den's good shoulder and rested in his embrace. Their love blossomed as I cleared away more brain fog.

"WoeNyl is helping my memories to come back," Theila said. "Could you remind me of who I am and what this is?" Her twilight surrounded her body and concentrated at her fingertips. Den's orb hovered over his forehead.

"Your twilight," Denrick said. "For one hundred generations your twilight guided my foremothers. Now you have come to me. You once dwelled with star-people, but you were banished for a kindness shown to a peasant girl. They forbade you from interfering with the lives of mortals. You saved her life."

"I remember," Theila said. "The star-people called me 'Sombermirth'."

"The girl you saved was my grandmother one hundred generations ago. You entered a pact to marry her first male heir, but evil star-people kept sons from being conceived until they lost track of my mother. I am the first male born to her line. I am the promised reward for your kindness," Denrick said.

"You're a good reward." Theila said as she played with her orb. "I remember you too, sister. I know that you personify truthfulness and compassion."

*'I want them to be happy,'* Nyla thought. *'But I wish we had a man like Den to hold. Why do other women win the good men first?'*

*'I don't know,'* Woethief replied. *'Even if we cannot find love ourself, we can find pleasure in helping others find it.'*

Den leaned back against the cave wall. Fever weakened him. Fresh blood, from a reopened wound on his shoulder, soaked his arm.

"Den, why are you bleeding?" Theila asked.

"I took a few shots for you. I'll heal," Den grimaced.

"Let me see," Theila demanded.

"No, Theila, I said I'll be fine." His look told her he wanted to remain strong, but we knew he could die from his fever.

Theila opened her mouth, but we placed our hand on her shoulder. At that moment, he would not listen no matter what she said.

"WoeNyl, help!" Theila cried as Denrick collapsed.

Den lay sprawled across the floor. Blood pooled below his torso. His pulse grew slower as we held our fingers against his neck.

"Save him, WoeNyl," Theila begged.

We examined his shoulder to find a rancid wound. Despite our best efforts, his bleeding did not slow.

"We cannot heal him," Woethief said.

"I'll bite him," Theila declared.

"By Ild law, you will be wed if you do," Nyla retorted.

"I may not remember everything, but Den loves me," Theila said. Her tongues embedded into Den's wound. She cradled her beloved in her arms.

"Thank you for giving me the power to heal," she squeezed our hand. "I will always serve you, Stewardess."

She leaned against a column, exhausted. Den lay motionless, but alive, in her lap.

With the bite, Theila acquired Den's attributes. Shocks of straight hair hung down the center of each ringlet. Her twilight merged with Denrick's. Firm muscles contrasted with her small stature.

Den sat up. He pulled Theila against himself with his hands clasped at her stomach.

"You saved me. I will never forget that," he rested his head on her shoulder. "Your tongues are beautiful."

Theila turned to face Den. When she stared at her reflection in his eyes, she understood why he loved her.

"May Theila and I marry?" Denrick asked.

"Please, Stewardess," Theila said. "I bit him, so I could never be with anyone else."

"Aye," we beamed. "When you bit him, you made him your groom." To this day, Silkwings consider a consensual bite, shared by a man and woman, to create a marriage. Ildylians and spiders also share love-venom as part of their wedding ceremonies.

*'Gemma,'* we reached out to our twin's mind. *'Your son wishes to be wed.'*

*'Why are you inviting me into your minds?'* Gemma asked.

*'We love you, sister,'* we thought. *'We want you to hear the wedding through our ears.'*

We told our friends that Gemma watched. Despite her uncertainty about her new role as a stepmother, feeling Denrick and Theila's auras through Woethief brought joy to Gemma.

We declared our friends to be man and wife. As the Stewardess of Silkwings and Twilight, we had authority over them. Crystal, Gemma, Nyla, and Woethief served as witnesses to the wedding. Den planted his lips on Theila's and cradled her head in his hands.

![WoeNyl Declared Den and Theila to be Man and Wife](../Illustrations/WoethiefBook27.png)

<!-- Alt Text: A giant man, with dark skin and short hair, plays with a young woman's hair. The young woman looks lovingly up at him. She has light, curly hair, glowing antennae, and dark lashes. Behind them, a young woman, with dark, curly hair, smiles slightly. -->

"I've wanted you for as long as I've lived!" he said. "Now you are mine and no one will take you from me."

"I am unworthy of you," Theila replied.

"No, you are glorious," Den countered. "No one is more beautiful. I wouldn't change any part of you."

"But Ildylians think I am ugly," Theila argued. "Do you love me though I look like an insect?"

"Sombermirth Theila, you are mine. I bled to win you. Stop doubting yourself," Den pleaded.

"But what if I fail you?" Theila asked.

"You will fail me and I will fail you. By God's power, we'll forgive. Tell me you promise to fight for our love until forever ends," Den asked.

"I promise, husband," Theila sank her tongues into his neck, her own form of a kiss.

The thunder of marching interrupted their endearment. In silent understanding, we each snatched up our weapons and donned rucksacks. Ankor's men had found us.

## 25 Capture

To escape our pursuers, we crawled into a narrow shaft and covered the entrance with a tarp, which sounded and looked like stone.

We traveled two hundred throses before we spotted Ankor's troops. Terror seized us. We backtracked into our cavern. Each tunnel contained enemy soldiers, so we barricaded both entrances. Each of us focused a pistol on an entrance and prayed we would survive. We needed to break out somehow.

Enemies at both sides swung battering rams at the barricades. The fortifications would not last.

*Bam.*

We glanced at our abdomen and whispered sweet words to Crystal's mind.

*Thud!*

Den turned to Theila.

*Thud!*

Their lips met for an instant, which they wished was an eternity.

*CRASH!*

The barrier at the main entrance cracked open.

*Thwoom.*

Our guns spat pellets into the gap as quickly as we could reload.

The gap widened.

A pellet grazed our side.

We suppressed the enemies with our guns.

They retreated to regroup.

"We might hold them off," Theila panted.

"How can we? Our ammunition won't last forever," Den countered.

We concentrated our twilight into a wall around us. Because it muffled sounds, the enemy could not hear to shoot at us.

*'That will slow them for a time,'* Nyla thought.

Our foes shot in our general direction, but missed us. We shot as quickly as possible. A woman's agonized shriek interrupted our defense.

"Quit the fight," Woethief yelled.

Our guns ceased their delivery of lead.

A soldier of Ankor held a dagger to Gemma's throat. She no longer appeared as Silver, but possessed her old spider body.

**"Lay down your weapons or she dies,"** Ankor demanded. The defeated duelist sought revenge for his lost chance at royalty.

"It's a trick. That's not really Gemma," Den warned us.

"We know Gem when we hear her. Yield," Woethief ordered.

We set our pistols on the floor and kicked them away from us, outside the fog of twilight.

**"Put your hands on your heads"** the soldier ordered.

Ankor's guards frisked us (our twilight could not hide us from their hands) and removed our concealed knives. After they bound our hands with silk, Ankor entered the cavern. We extinguished our twilight, to save energy.

**"We meet again, Nyla,"** Ankor limped to us and smote our face. **"Only, this time you can't cheat your way to victory."**

![Ankor Smote WoeNyl's Face](../Illustrations/WoethiefBook28.png)

<!-- Alt Text: A man, with bat-ears and spider-legs, punches a young woman whose wrists are bound behind her back. Her curly hair flies around  her as she falls. The spider-man is missing two of his front legs and wears a leather kilt.-->

"We did not cheat in the duel," Woethief said through bleeding lips.

**"I know what you did. You bent my mind. How is that not cheating?"** Ankor demanded.

"No rule banned it," Woethief replied.

**"Let's hear you fight now,"** Ankor roared.

Den strained against his captors. Ankor's claws tore our face.

**"That's for crippling me,"** Ankor said. **"They amputated my legs."**

"We did not mean to," Nyla said as we grasped our face.

**"You made me lose everything,"** he lifted us by our collar.

"We did it for our child," Woethief said.

**"What about my children?"** he asked. **"What about my lover, Flame? You disgraced us."**

Woethief tried to steal his rage, but he resisted. Hopelessness welled within Ankor.

"Let go and we will help you," Nyla pleaded.

**"It's too late. Centin pursues us. He'll never forgive me for starting that riot, but I want to hear you suffer before I die,"** he dropped us and wrenched Gemma from his guard by her fur. Ankor's blade tickled Gemma's throat. **"First she dies, then your friends, then you."**

"Yield now and we will talk to him for you," Woethief said.

**"Centin won't listen,"** Ankor derided.

"Yield now!" Den snapped his bonds, tripped his guard, and punched Ankor's face. When Ankor dropped his knife, Gemma stumbled to us.

**"They made me lead them to you,"** Gemma sobbed.

"We will guard you, dear," Woethief said.

Theila cut her bonds and stabbed her captor in the arm.

"Give up, Ank-or," Nyla ordered as we shielded Gemma. We wreathed our family in twilight.

**"It's too late, we might as well fight to the end,"** Ankor said. **"There will be no forgiveness for my men."**

"What did Flame think of your war?" Woethief asked as Den poised to strike any guard that approached.

**"She begged me not to do it. She is so ashamed that she was part of the riot,"** he wept.

"If you love her, yield now," Woethief said.

**"Will she forgive me?"** Ankor asked. **"I got her little brother killed during the riot."**

"Be brave. We will help you in your trial," Woethief promised. "We want you to go back to her."

**"Why would you help me?"** he asked.

"We were helped much by our God," Woethief said as we reached for Ankor's hand.

Clashing steel sounded through the doorway.

**"Centin's here!"** a soldier thundered as a bullet pierced his back. We gulped as our former husband, the usurping steward of Ildylia, approached. He hated us for contradicting him in front of a crowd and hated Ankor for his insurrection.

Madness seized each of Ankor's men. Under the control of Centin's mind, each man slew his neighbor. Ankor's commands did nothing to quell their senseless slaughter. We clutched Gemma, Den, and Theila to our chest and shrouded our family in silence and twilight.

When Ankor sought refuge in our shield, Gemma shoved him away. With only six legs, he lost his balance and struck his head on a stone. Life fled from his body.

*'Why?'* Woethief asked Gemma's mind. *'He asked for our help.'*

*'I thought he would hurt us.'* Gemma replied. *'Besides, he didn't deserve help. He ruined my coronation and Denrick's wedding.'*

We pitied Ankor's girl, Flame. Who would care for her children?

Centin entered the cavern. As he stepped over fresh corpses, not a spot of blood stained his fur. 

## 26 Negotiations

Centin's guards surrounded us.

**"So, this is the Woethief's little family. Just as wretched as she is,"** scorn dripped from Centin's fingertips.

Den's muscles tensed and his fists balled up.

"Do not try. You cannot beat him," Nyla warned Den.

"But I can die while I try," Den retorted as he strained against his manacles.

Nyla chastised Den, "We need you to live. Think; tact will be the way out."

**"You there,"** Centin gestured at Den. **"What is your name?"**

"I am Den," Denrick said.

**"You, what is your name?"** he pointed at Theila, who picked her manacles without Centin noticing.

"Thei-la," Den answered for her.

Centin approached Theila. When he placed his hand on Theila's face to read her thoughts, he found Theila's mind impermeable. Trembling seized Centin with the exertion of his mental attack. Theila spun him around and clenched his furry throat. Theila's right arm locked Centin's right arm and pinned his wings against her. Terror filled Centin. His guards held daggers to our throats.

**"Is this supposed to impress me?"** Centin chuckled with his feet. **"So you can pick locks. So you can subdue me with brute strength. I can destroy your mind with a thought."**

As Theila lessened the protection of her mind, her grip around Centin's throat tightened. Centin took the opening, but found himself mentally trapped by Theila.

*'Let us go,'* Theila spoke to his mind. *'And you will be unharmed.'*

**"Go ahead, kill me,"** Centin said. **"But know that if you do, it will break WoeNyl's heart. Deep down, she still loves me."**

Centin's face contorted as he fought for air.

"Let the man go," Nyla ordered Theila.

*'Lose our bargaining power?'* Theila thought back.

"Do not hurt the e-bu of our child," Woethief said. "You cannot kill an un-armed man. We love him."

Centin had little time before his breath ran out.

"Have pit-y, Thei-la. We are not like him," Woethief pleaded.

Theila released Centin's throat, but not his arms. He gulped air like a starving bat gulps blood. Our twilight wrapped around our enemies' heads. None of Centin's soldiers could hear to defend their master.

![WoeNyl Wrapped Twilight Around Her Enemies' Faces](../Illustrations/WoethiefBook29.png)

<!-- Alt Text: A young woman holds out her right hand toward bat and spider-men. Twilight fog wraps around their heads to confuse them. A woman, with insectoid wings and antennae, wraps her left arm around a man's throat. -->

**"Release me or my guards will kill you all."** Centin ordered.

"Don't listen to him," Theila said. "We have the upper claw."

**"If I die, you will never know where Faithful's statue is hidden. I am the only one who knows,"** Centin said to us.

"WoeNyl, don't listen to him. He could be lying," Denrick said.

"We will guard our kin" WoeNyl said in unison.

"We must kill him," Theila said. "Centin will never stop hunting us."

Centin wrested his mind from Theila's hold and caused Theila's arms to relax their grip. Theila collapsed, with her head in her hands.

**"I will kill her now if you do not release me from your spell of silence,"** Centin boomed.

*'It is not a spell. We must defend our family,'* Nyla spoke to Centin's mind. *'Guarantee safe conduct for us and our family and we will release you.'*

**"I swear by myself and the great goddess, Natia, that you will pass safely to wherever you desire,"** Centin said.

*'Kill him, Woe. You can't let Centin live,'* Gemma thought to us.

*'We will not harm you, but we will free our family first,'* Nyla thought to Centin.

"Run, we will hold him while you run," Nyla yelled.

The guards' blades halted our family's progress.

**"Release me first, then your family may leave,"** Centin said.

More of Centin's men entered the cave. We, Den, and Theila spread our twilight toward them, but exhausted ourselves. When our fog vanished, Centin escaped.

**"No, what are you doing?"** Gemma roared.

**"You are a fool, Nyla. What makes you think I will honor your deal?"** Centin asked.

"Have you no hon-or?" Nyla asked.

**"None,"** he said. **"But I know you will not harm me now. I have your oath."**

<!-- Psalm 15:4 -->

*'How can he use our honesty against us?'* Nyla wondered.

**"Prepare to watch your family die,"** Centin sneered.

**"Centin, this is not what we agreed to."** Gemma said. **"You said they would be banished, not killed."**

It did not surprise us that she had led Centin to us. She bore little loyalty to Theila or her step-son.

**"Are you afraid of having your woes returned?"** Centin interrupted Gemma. **"These monsters are too dangerous to be banished."**

**"Your worshipfulness, I implore you to spare them. They have done you no wrong,"** Gemma begged.

**"Of course I won't spare them, you imbecile,"** Centin boomed.

"Den, Thei-la, and Gem are of no worth to you, let them go," Woethief pleaded with Centin.

**"Yes, my god, allow them to escape."** Gemma said. **"I will sacrifice to you every month if you let WoeNyl, Den, and Theila escape."**

**"Give me all your power. Then I might consider it,"** Centin said.

**"That is too high a price,"** Gemma said.

**"Den and Theila, you may flee, if WoeNyl promises to stay,"** Centin said. **"I will never stop hunting you. Enjoy living with the shame of abandoning your sister to a fate worse than death."**

"We will not flee," Woethief and Nyla said together.

Den wanted to scream that he would never leave us, but he knew he must take his bride to safety. We kissed his forehead and wiped a tear from his face.

"Thank you. We were led to your God and helped by you. You are kind and might-y. We are proud to call you our kin. We love you with all our heart," Nyla and Woethief said in unison.

"I love you too, Woe-Nyl," Den's rippling arms embraced us.

"Take Thei-la away from harm. Your death will do no good," Woethief said. "We will find you when we can."

We turned to Theila, she embraced us and we kissed her forehead.

*'Thank you for giving us your venom,'* our minds thought to Theila. *'You are the most beautiful creature we have ever known. You are the best sister we never knew we needed. We love you with all of our heart.'*

Theila kissed our brow, *You're beautiful too WoeNyl, thank you creating me for Den. You saved Phyla's life and turned me from a scavenger into Den's bride. Only someone lovely on the inside could create beauty from ugliness.'*

While we held our sister, Gemma approached our minds. *'He will torture you, Nyla, are you sure you want it this way?'*

*'Yes,'* Nyla answered.

*'What about Crystal?'* Theila asked.

*'If you are willing, I can transfer her to Theila,'* Gemma suggested.

*'What? How?'* confusion spilled from us and Theila.

*'I can alter your form'* Gemma replied. *'You can alter Theila's.'*

*'How can a mother give up her only child?'* WoeNyl wondered.

For all the tens of thousands of woes she stole, Woethief had never encountered this one. We loved Crystal and never wanted to part from her, yet we knew it was the only way to save her life.

*'We are willing, but only if Den and Theila wish it,'* Woethief thought.

*'Of course we do,'* Theila replied.

We and Gemma prepared for the shapeshifting. In an instant, Theila became the surrogate mother of our sleeping child.

*'Farewell, Comfort Crystal,'* our minds thought to our child. *'May you bring the same joy to your new mother as you did to us.'*

Centin interrupted our farewell.

**"Of course, I cannot let you both escape as you are,"** Centin said. **"I can't let you remember this life."**

He seized Theila's head and tried to replace all her memories with false ones, full of fear and loathing for her family. Because Centin struggled to attack Theila's mind, he placed a blade to her throat.

**"Erase her memories and change her form,"** Centin ordered us.

To save her life, we archived Theila's memories in our mind, but left her mind blank.

With her blank mind, came a new body, lovely, yet dreadful. A gray beauty loomed over us. Straight gray hair spilled over her broad shoulders. Centin let out a lewd whistle. Theila's cold eyes pondered us without recognition or a trace of her usual kindness. Den's desire for her in no way diminished, but she recoiled from a stranger's touch.

Centin grabbed Den by the throat and used his power to remove Den's voice. Centin often used his abilities to cripple and maim. We did not intervene because we would rather Den live in suffering than die in a fight.

**"You are mute now. Carry her away and never return,"** Centin ordered.

Den looked at us, pleading for another way.

"Go!" Nyla ordered.

Den grabbed his bride's hand and pulled her out of the cave.

When Gemma tried to leave, Centin seized her arm. **"You don't get to leave with them."**

Gemma seethed, but dared not oppose Centin.

**"I'm so sorry. I tried to help,"** Gemma whimpered with her head on our bosom. **"I couldn't give up my power. He will find them. It's all a twisted game."**

"You did help," Woethief said.

Woethief stole some of Gemma's grief, but if she stole more, our heart would burst with sorrow. We prayed for our family's survival. We knew Centin would rescind his word, but Woethief thought sending them away was the only chance to save our loves.

*'It was not your only chance!'* Nyla chided. *'You let this monster survive while your family was in danger.'*

*'I cannot resist the opportunity to show compassion,'* Woethief replied.

*'Not even at the expense of our brother-in-law and sisters and daughter?' *Nyla asked.

*'I still love Centin a little,'* Woethief thought. *'I am sorry.'*

*'You should be!'* Nyla screamed into Woethief's mind.

We hated that we warred within ourself. How can two incompatible natures coexist in one body? How could we let Woethief force us to make such foolish decisions?

Gemma pulled away from us.

**"And now, what shall I do with the Woethief and the Spymistress of Stonecourt?"** Centin asked.

We remained silent, like the rough stone around us.

**"Bind her feet,"** Centin ordered.

Guards shackled our ankles.

**"How can I trust you to not run away?"** Centin asked. **"With your powers, these bonds will not stop you."**

*'How can he ask that?'* Nyla thought.

"We will keep our word," Nyla replied.

**"Very well, take them away,"** Centin ordered. **"We will meet again at your trial."**

Soldiers, who jabbed us with the butts of their spears, escorted us to the King's dungeon. As we marched, a vision from Centin filled our mind.

***

> Centin's guards led a man to his doom. We could not help or communicate with the man, but visceral fear rippled through us.
>
> The man was our Ebu, our lost father. Despite our difference in race we felt a connection with him. He marched to the Golden Death. We knew we would suffer the same way.
>
> *'Please do not do this! He is innocent,'* Woethief begged Centin's mind.
>
> Our soul shattered again. Centin's shame for persecuting us and punishing an innocent resurfaced. Centin wondered why he could not stop his own evil.
>
> The vision shifted to another place. Our father drank Gold-Water then stumbled toward the bell at the center of the Golden Death. The faces of golden statues of criminals pleaded for mercy.
>
> Gold coloring slithered up our Ebu's legs as he scampered forward. Before he reached the bell, he froze into solid gold. Our screams rent the air.

The vision ended as guards shoved us into our cell.

## 27 Imprisoned

We shared a cell with Gemma. We wept in the corner. Twilight mingled with our red tears. Despite our twilight's constant temperature, a chill crept into our soul.

![WoeNyl Talked to Gemma](../Illustrations/WoethiefBook30.png)

<!-- Alt Text: A young, human woman holds up a glowing orb and looks at a spider-woman's face. The spider woman has crossed arms, bat-ears, spider-legs, and no eyes. A thick braid runs over her left shoulder and she wears a sleeveless, silk dress that flatters her attractive form. Both women are in a room with a natural, stone ceiling. -->

**"Thank you, WoeNyl, you saved my life from Ankor,"** Gemma said.

"We did it for each child of Faith," Nyla said.

**"I helped the woman who seduced Centin away from you,"** Gemma said.

"We know," Woethief replied.

**"I couldn't resist. She paid a year's wages,"** Gemma said. **"You know how much I needed the money. Besides, when he betrayed you, you were freed from the love potion."**

Woethief probed Gemma's soul and knew that she spoke half of the truth. Gemma destroyed our marriage to regain custody of us, not only to save us from a love potion.

*'How could she do this to us?'* Nyla wondered. *'She helped someone seduce our husband and then stole Faithful.'*

**"I know you're upset but marrying Faithful was the only way to gain power,"** Gemma said.

"If Faith knew you loved him for might, it would break him," Woethief said.

**"How can you blame me?"** Gemma asked. **"You murdered Faithful. He could have changed me, WoeNyl. He could have transformed me into a good person."**

"We did not kill him," Nyla said. "He will come back." Despite Faithful turning to silver, we still bore his woes. If he were dead, we would not remember his past.

**"You couldn't endure my happiness, so you gave him Gold-Water,"** Gemma fumed. **"You drugged him so you could win."**

"A dart hit him," Nyla replied.

**"Why didn't a dart hit you?"** Gemma asked.

"We do not know," Woethief said.

**"You murdered my husband and you have not yet killed me because you enjoy my torment,"** Gemma said.

We knew that what Gemma said was not true, but her soul bore no aura of deceit. We searched our memories and discovered regret for shooting Faithful with the dart. We had recently stolen shame from Centin, Gemma, the man whose goblet we spilled, and a guard.

*'Which of them shot Faithful?'* Woethief wondered.

**"Why did you save me?"** Gemma asked. **"I thought you hated me?"**

*'How could she say that after all we have done for her?'* Nyla demanded.

*'What if we did shoot Faithful?'* Woethief wondered, as the memory of guilt worked its way deeper into us.

"Why do you think we hate? We been good to you." Nyla said aloud.

**"You tried to steal Faithful from me,"** Gemma accused. **"I've wanted him since I met him, after you saved him from the mugging. You tried to seduce him and you wonder why I thought you hate me? I had to force you to transform me into Faithful's ideal woman."**

Before her wedding night, we never knew that Gemma wanted Faithful. She kept pleasant parts of her life from us.

"But we helped him to not fear you on the night when you were wed. We gave you beau-ty," Woethief said, reminded by the constant uneasiness Gemma's loveliness caused us. She was pretty in her spider form too.

**"You have some nerve!"** Gemma said. **"I thought maybe his death meant something to you, but you're as full of gall as ever."**

"We did not kill him," Nyla said. "We love him."

**"You loved him so much that you could not bear to let me have him,"** Gemma said.

"No, we love him like our kin. To help him love you, we took the fear that Faith had," Woethief said.

**"You are lying,"** Gemma accused.

"Gem," We placed our trembling hand on her cheek. "If we had pled, be-fore we gave him your drink, he would have wed. We did not plead. We thought a new life with a new girl would do more good for him."

**"I don't know whether I can believe you,"** Gemma said.

"We beg you to try," Woethief requested.

**"I want to believe you, but how can I?"** Gemma said, though she could not fathom giving up a man for someone else.

We did not answer her question, but closed our eyes in weariness.

**"Why did you not slay Centin?"** Gemma asked.

"We made an oath," Nyla replied.

**"When have your oaths ever meant anything?"** Gemma asked.

*'Always, a lie has never stained our lips, unless Gemma made them say it,'* Nyla thought.

"Now, our word will mean much," Woethief said.

*'What if we are the demon, and she is the saint? What if we really shot Faithful?'* Woethief wondered. The shame of thousands of souls and thousands of years mingled inside of us until we did not remember our true past or identity.

**"One of us is lying about Faithful, and it isn't me,"** Gemma declared.

"If we hurt Faith, why would we guard you?" Nyla asked.

**"You obviously felt guilty or else you saved me because you enjoy hearing me suffer,"** Gemma accused.

"If we hurt you, we will make it right," we did not know of a specific crime but one of our shame memories might be our own.

**"That is impossible,"** Gemma said.

"We will try," Woethief said.

We gazed at Gemma in our twilight. Though her body was the same Gemma as always, she seemed innocent and more beautiful. We bore gray eyes. She bore gray ears. For a moment, we seemed similar: frightened, hopeful that the other spoke the truth, but terrified that we might be the monster.

Gemma gripped the ceiling with her eight feet and fell asleep. We collapsed from exhaustion.

***

In the morning, guards swaggered into the room. Gemma dropped from the ceiling.

**"Follow me,"** a captain ordered us after guards shackled our wrists and legs.

We obeyed. The skin on our face stung each time our thick hair hit it.

After a march through the city's bowels and private tunnels, we entered a court inside the main palace. The King, Queen, and Centin sat in austere silence. Natia observed from the shadows. We felt her pity for us. She knew what it was like to endure Centin's scorn.

We stood before the tribunal with our gazes at our feet and our hands bound behind our backs. Our lives depended on the whims of a royal couple seething over Faithful's fate and an unscrupulous world-steward, who exercised power because it gave him something to do.

With the absence of a jury or defense advocates, the trial's fairness evaporated. We determined to be Gemma's advocate.

We stood with Gemma, helpless to do anything but speak. A priestess of Natia brought a stool for us to sit on. It was made from three beast femurs bound together, with a triangular cloth for the seat.

**"Now that the two of you are here, what do you have to say in your defense?"** Syreeta asked.

"What charge do you make?" Nyla asked.

**"Don't pretend you don't know why you are here!"** Centin boomed. **"You, Nyla, are charged with Faithful's gold-murder."**

"Why are we to blame?" Nyla asked.

**"Everybody heard Faithful turn to silver. There is no doubt that you froze him,"** the King replied.

"We did not give him Gold-draft," Nyla said.

**"Prove it,"** the King said.

"We cannot. A foe hid Faith," Nyla said.

**"How convenient,"** Centin sneered. He pretended that he did not know where Faithful's body was.

"Aye," Nyla shot back.

Centin glared. Our gaze met his ear-gaze.

**"It is obvious to those gathered that her accomplices hid his body so we could not investigate,"** the King said. **"She deserves death. Gemma, you know her well. Did she have a reason to attack or poison Faithful?"**

**"Yes,"** Gemma replied.

**"Please elaborate,"** the King requested.

**"It was because of Faithful's wife,"** Gemma said. **"She slew out of jealousy. She killed Faithful because he did not love her as she hoped. We all know that Silver is missing. Who do you think kidnapped her?"**

*'The filthy little rat! How could she?'* Nyla screamed inside our head.

*'She must protect her children,'* Woethief countered.

Only we and our family knew Gemma and Silver were the same person. We prayed no one else found out.

Queen Syreeta turned to us. **"Nyla, is it true that you loved Faithful?"**

*'That's none of your business,'* Nyla thought.

"Aye," we said.

**"And since you loved him, you would never wish harm or death upon him?"** Syreeta inquired.

"Aye" we replied.

**"And in the duel, you had no intention of killing him?"** Syreeta asked.

"We did not," Nyla declared.

**"So, you are saying you had no reason to kill Faithful even after he chose another woman instead of you? I find that hard to believe,"** the King balked.

"We love him. We could not harm him." Woethief paused to rest our throat. "We did not beg him to wed, but led him to joy with a new wife and healed the mind of Faith."

**"There is no evidence that this girl froze Faithful,"** Syreeta said. **"She had no compelling motivation. As we already know, he offered her a nine-tenths-share of all his winnings and the promise of her freedom, so money was clearly not a motivation. She loved him enough to let another girl marry him and even helped the marriage along…"**

**"How do we know that?"** Centin interrupted the Queen.

**"The great goddess, Natia, told us, my lord,"** the Queen declared to Centin.

**"What about Princess Silver's disappearance?"** Centin asked.

**"Nobody knows anything. She vanished during the parade and our patrols haven't found her yet,"** the King said.

**"Then, Nyla, tell us where she is,"** rumbled Centin.

"Not dead. We will not tell where. The girl would not want that," Nyla said.

Relief washed over Syreeta. She believed us because she loved us.

Gemma shuffled her feet and the royal couple consorted each other in tense whispers.

**"What about all her crimes of espionage? She accused me of trying to kill her,"** Centin roared from beside their high-backed thrones.

**"That is not why she stands trial today, my lord,"** Syreeta said. Centin cringed as she challenged him. **"Please do not hold her accusation against her. We mortals are weak and jump to conclusions. Nyla, tell us about the crimes Centin mentioned."**

"We did them un-der the mind-con-trol of a foe," Woethief said. "We could not fight back at all."

**"What other? Who controlled you?"** Syreeta asked.

Gemma turned toward us. Worry etched her brow. We remembered her torture. Woethief remembered the shame she bore that weighed more than all the stone in the world. We remembered our promise to forgive. We remembered three infants, who deserved life.

"We will not tell," Nyla said.

**"Probably Gemma,"** Centin said. **"She made Woethief steal people's pain for money."**

*'Please stop!'* Woethief thought.

**"Gemma could steal Nyla's body, and make it do things Nyla would never have done,"** Centin sneered. We regretted sharing so much about ourself with him.

**"Is this true?"** the Queen asked us.

"We do not want to tell," Woethief said.

**"Do you realize it is in my power to send you to the Forges or the Golden Death if you do not defend yourself?"** the King demanded.

"Aye," we replied. Both halves of us loved Faithful's children, so we protected their mother.

**"You have it all wrong,"** Gemma lied. **"She controlled me. I am the victim. She won't tell you who made her do it because she made herself do it. She has two spirits. One is very wicked. It makes her do things."**

**"Is this true?"** King JaReel asked.

Centin knew the truth. Both of us knew nearly everything about the other. Neither of us let on that we knew.

"What do you mean?" Nyla asked.

**"Don't play stupid with us!"** the King raged. **"Are Gemma's accusations true?"**

"No," Nyla said.

**"So you do not have two minds?"** asked the Queen.

"We have two," Woethief said. "They are each the prey of the con-trol."

<!-- Inspired by Matthew 26:63 -->

**"This is the last time I will ask. I put you under oath by the God you serve. Tell me who controlled you,"** the King pointed at us.

Gemma's ears pleaded that we not tell. We knew we must clear our names to save her.

"Gem did," Nyla said.

Queen Syreeta whispered something to her husband, then the King listened at us.

**"There is insufficient evidence to show that you gold-murdered Faithful. Duels are inherently dangerous and betting leads to underclawed dealings,"** the King declared. **"You will be tried later when we have gathered evidence about your espionage. I have little doubt that the Forges await you. You are free for now, but do not flee the city."**

"Thank you," we bowed as low as our wounds allowed. "We will prove that we have no guilt."

## 28 The Other Prisoner

After a door at the chamber's rear opened, a messenger glided to the King and whispered to him through his hands.

![A Messenger Whispered to King JaReel](../Illustrations/WoethiefBook32.png)

<!-- Alt Text: A woman with thick, gray hair and bat-ears, stands next to a human woman, with  dark, curly hair. They face two thrones. The left throne holds a queen with bat-ears and a dress made of jewels. The right throne holds a king, with bat-ears and a sleeveless tunic. He faces a messenger, who holds his hand on the king's left wrist. Behind the messenger, another bat-person faces away from the women. -->

The king's gaze turned to Gemma. She wrung her fingers.

**"Gemma, we know of your activities in Stonecourt"** the King said. **"You are found guilty of espionage and forgery. The information you've sold has aided rebels and thugs."**

At the verdict, Gemma bowed low and pleaded for mercy.

"What proof do you have of her crime?" Nyla asked.

**"We have proof,"** the King spoke.

"Give it now. How do you know her guilt?" Nyla asked.

**"We have no need of presenting anything to you. You are merely a peasant with no concern in the matter,"** Centin boomed.

**"Gemma has committed too many crimes to count,"** King JaReel said. **"We have been documenting them for ventings. More pressing matters require us to end this trial now."**

## 29 Sentencing

"Give a fair trial to Gem," Nyla shouted.

**"She had one, we compiled a list of her crimes before she arrived, and now we will sentence her. Gemma's guilt is indisputable,"** the Queen said.

**"She will be publicly flogged and sent to the Golden Death tomorrow, once the night fog lifts,"** the King pronounced.

"We beg you, wait for Gem to give birth," Woethief pleaded.

**"Gemma will be gold-executed when the fog lifts. Whatever demon-spawn she carries deserve to die,"** King JaReel declared.

"May Gem have a death at night?" Woethief sought to spare Gemma from the humiliation of publicly turning to gold.

**"Gemma must serve as a public warning against spying on behalf of our enemies,"** the King declared.

"May we wait in the cage with her?" Woethief asked.

Before the King could refuse, Syreeta spoke. **"Yes, I do not know why you are so loyal to her, but I will entertain your folly."**

***

We found ourself again in a cell with our criminal sister. Chains secured Gemma to the wall.

**"You and your grand 'fair trial'!"** Gemma screamed. **"I knew you didn't want me to have justice. I wish the mob had killed you."**

"We did all we could." we lowered our voice. "We did not tell them who you are."

**"Then why am I in a death cell?"** Gemma asked.

"Your end of life need not be full of rage. Hope re-main," Woethief said.

**"Hope in what?"** Gemma asked.

"Hope that God will for-give," Nyla answered.

**"I'll hope in your God when he gets us out of this cell,"** Gemma retorted. **"You must think of a way to free me or I swear I will kill you before I die."**

"Pray," we knelt and held out our hand to Gemma, she clasped it.

**"Take my worry, so I can sleep,"** she ordered. **"Your God is not worth praying to."**

Care and turmoil surged into us. Fur grew on our arm.

"We will not turn in-to you," we jerked our hand away before Gemma could transform us into a replica of herself.

Gemma slept like a stone as we begged God to spare her and her children. Dread filled us when we realized what we must do.

<!-- Inspired by Matthew 26:36-46 and Luke 22:41-46 -->

*'God, if there is another way to save them please show us.'* Nyla prayed.

Our soul filled with trouble enough to slay us.

*'We cannot save them unless we drink the Golden Cup, but it is so awful a trial,'* Woethief thought.

Red tears gushed from our eyes. Den had told us of how the Savior suffered before his execution. Similar terror filled us as we prepared to sacrifice for Gemma and her children.

*'If there is another way, please do not let us drink the cup,'* Nyla begged.

We agonized in prayer as Gemma's shame and vitriol swirled inside us.

*'Please take this cup from us, we do not deserve it,'* Woethief prayed.

If we saved her body from destruction, perhaps God would save her soul. If Gemma died tomorrow, she had no chance to repent. Would watching a broken "savior" sacrifice herself lead Gemma to the real Savior? We could take her shame, but only God could take her guilt.

*'We are your maidservants, work your will instead of our own,'* WoeNyl prayed as one.

Anguish accompanied us. We kept vigil all night while our sister slumbered beside us.

***

Before the fog of night lifted, a procession of JaReel, Natia, and Centin unlocked our cell. We roused Gemma.

**"Good morning,"** Centin's lips twisted in an ironic smile. **"It's a wonderful day for an execution isn't it?"**

Gemma's hair stood up and her ears straightened.

"We will drink the cup, not her," WoeNyl said together.

**"What do you mean?"** Natia asked. **"You are not the one to freeze today. After your trial, Syreeta petitioned me, so I exonerated you of your suspected crimes. You are free now."**

We relished the idea of freedom.

"Thank you, but we take her death to help Gem," WoeNyl said.

**"But surely you can't mean it? No one would take her place after all she did to you,"** Natia asked.

"We will," WoeNyl said.

**"How did she force you to do this?"** Natia asked. She suspected Centin or Gemma of controlling our minds.

"We do it of our own will," WoeNyl said.

**"Very well, WoeNyl, you may take her place. I declare Gemma innocent of all transgression and sentence Woethief Nyla to death by gold for espionage and forgery."** Centin said. Natia reluctantly nodded.

**"You will become silver and ring the bell,"** Natia whispered to us. **"Queen Syreeta will not have long to mourn for you and Faithful."**

Guards chained our arms and legs together and prodded us out of the dungeon at spearpoint.

As we walked, we spoke to Gemma. "We keep you from death, but on-ly God can help your guilt. Turn to him, dear. We love you. Take our gift to aid your new life." We transformed Gemma to give her the gift of twilight, which allowed us to shapeshift her body remotely, as if water connected us.

We touched Gemma's mind. *'You will turn back into Silver once it is safe. Syreeta will be a good mother to you. Enjoy being Princess.'*

*'Why did you not give me your gift before?'* Gemma asked.

*'You did not ask for it,'* Nyla replied. *'Thanks to Theila, we are finally learning its power.'*

*'If I transform you into someone else, you will not have to be executed,'* Gemma thought.

*'Gemma,'* Woethief thought to her. *'We must keep our word. After we take your place, it will be as if you have never committed a crime. Besides, Centin would find out about the trick.'*

Gemma nodded.

*'Will you honor Denrick's request to free us in five years?'* we asked Gemma.

*'No,'* Gemma said. *'You are too valuable to us. We will free you in thirty years as we already promised.'*

*'Then,'* we said. *'We will serve you faithfully.'*

As we marched toward the Golden Death, Gemma left us. As she maneuvered through side passages, we reached out to our twilight. Once she was alone, we transformed her. With a flash of gray, her disheveled garments disintegrated, replaced by a fresh, silk gown. In the form of Silver Nectarfang, she appeared in every way to be Princess. We felt the delight and relief of Gemma and Syreeta as they reunited. Syreeta loved Gemma for Faithful's sake. We had failed to become Princess. At least Faithful's wife had succeeded.

***

Centin's men prodded us into the amphitheater of execution. Thirty men surrounded us with their rifles and bayonets trained on us. The crowd hissed disapproval as announcers described our appearance. The King placed his hands on the rumble machine.

**"Today is a great day for our kingdom,"** the King announced. **"We thwarted the spying of the Woethief to prove once again that this kingdom is unshakable."**

We pitied JaReel. Centin would devise a plan to eliminate the puppet-king and steal Syreeta.

**"Through the kind providence of Natia, we now have the loveliest girl for our daughter. After we humiliate our enemy, the Woethief, we will make our hearts merry at the celebration of your Princess,"** the Queen gushed with pride. We rejoiced for the exaltation of our sister.

The King turned to us.

**"This, the queen of villainy and architect of calamity,"** the King rumbled. **"stands before you for her cowardly espionage against this kingdom, which aided Ankor's insurrection. By the grace of our father, Centin, and mother, Natia, Ankor failed in his assault on Princess Silver. The Woethief spawned from the Forges' belly. May Natia show no pity on her soul."**

At the King's signal, soldiers encircled us. They shoved a silver shawl over our gown, bowed before us, and taunted us with the name "Princess." After removing the shawl and our gown, they stretched our legs and arms across two pillars to tighten our back.

**"This girl's crimes are so grievous that Natia demands thirty lashes rather than the usual fifteen,"** Centin approached us with an acid-whip.

Two guards seized our head and sheared our hair. The beautiful part of us piled at our feet. Without our hair, coldness crept around our head.

Centin raised his hand and snapped the cords into our back. We shrieked over the crowd's din. A second blow struck our back. The crowd cheered. Two, three, four, we lost count.

With each strike, more of our flesh stung. The whip tattered our slip and deposited vinegar. We felt violated with thousands of ears listening at us.

"God, let it end," WoeNyl screamed.

The lashes stopped. We sucked in air. Our skin split in long rivulets of blood as they tightened our straps.

**"Now, for the next fifteen,"** Centin bellowed.

Again and again, the acid gnawed our back and legs.

"Forgive him, God!" Woethief gasped.

When the beating ceased, soldiers unbound us and we slumped before Centin.

"Be-fore we die," Woethief begged. "Vow that Cry-Com will live?"

**"I will not kill her before she is old enough to hold a sword,"** he said.

"Vow that you will not try to find her," Nyla pleaded.

**"At first, I thought her mental powers must be a figment of your imagination, but she is as powerful as Natia. She threatens me. It's a pity Crystal is tainted with human blood. She would have been very attractive and possibly useful otherwise,"** Centin said.

"Why did you let Cry-Com go if you knew of her?" Nyla asked.

**"I haven't decided whether I hate her yet,"** he said. **"It depends on how similar she is to you."**

"If Cry-Com be-came like we are, what would you do?" Woethief asked.

**"Oh, I will find her and teach her to be like me. You are pathetic, it is rather entertaining to feel how much you love me. Your pitifulness almost convinces me to marry you again,"** he laughed.

"We will for-give," Woethief said. "But not wed a-gain."

**"I do not need your forgiveness,"** Centin said.

"En-ter the Gold Death first," Woethief lisped. "We will re-deem you for your crime."

**"That I will not do,"** he laughed.

Despite our pain, we relished the memories of our life with Centin before he betrayed us. We longed to be innocent again, instead of lovelorn. Centin's betrayal stung more than the whip's acid, because we feared he would never repent.

Centin leaned forward and kissed our forehead. We were too weak to resist.

**"One more thing, WoeNyl,"** Centin said. **"Gemma has enemies. They will find her. Gemma will not survive long enough to meet her children."**

Centin did not know she was now disguised as Silver. Our sacrifice was not for nothing. Our twilight protected Gemma. We loved her because of Faithful and his children.

"We love you. We pray that you find the hope we found. We pray that you learn to love Cry-Com," WoeNyl said to Centin.

Our executioner's claws dug into our shoulders and shoved us toward the Golden Death. Centin swaggered beside us.

We stumbled toward the executioner's cup. Only an innocent person, who rang the bell, could redeem those who turned to gold inside.

*'God, please provide another way,'* WoeNyl begged.

The executioner filled the chalice to the brim with Gold-Water. When we took the cup, our quivering hand spilled none of the vile liquid.

*'God, we do not want to do this,'* WoeNyl prayed.

We brought the cup to our lips.

*'Only the innocent can redeem those inside. Since we go in place of Gemma, are we innocent?'* Nyla wondered.

We drained the dregs of the chalice. The Gold-Water scalded us more than the acid whip.

While we stumbled into the Golden Death, we knew that within a minute we would either become cursed gold or living silver.

We tripped over a statue. Ancient fabric tore away from the body as we grasped for support. We stood, despite the great weight of our stiffening limbs.

We struggled toward our vindication and at last reached the bell. A frozen person blocked our path. As we scrambled over the obstacle, we recognized the statue with our fingers. Our Ebu stared up at us in horror.

Despite our shock, we clambered over our father to ring the bell.

We failed.

Our stiff body crashed into the bell, which should have freed us and Faithful. It did not. As we froze against it, a dull thud reverberated throughout the cavern.

![WoeNyl Rang the Clapperless Bell](../Illustrations/WoethiefBook34.png)

<!-- Alt Text: A bald, young woman, with wounds covering her body, touches a silver bell. The bell is almost as tall as her and engraved with depictions of tall mushrooms and an overflowing goblet. Behind the woman is a statue of a man with bat-ears, bat-wings, and spider-legs. The woman cries as she holds the statue's arm. In the background, other statues are frozen in unusual positions. -->

Our body became silver and twilight. In our own light, we beheld our father's body. Centin's vision had lied. Our father was untarnished silver. He was innocent but had failed to reach the bell.

We struck the bell one last time. Our right hand was the only part of us that had not frozen yet. The bell did not ring. Someone had removed the clapper and now we would freeze.

We didn't die. We hung in the air paralyzed, everything around us illuminated by the twilight of our body. The last thing we saw before our consciousness crumbled away was our Ebu's terrified ears. The last thing we felt was Syreeta's love for Gemma.

## Appendix

### A. Pronunciation Guide

<!--
I consulted these resources:
- <https://dictionary.cambridge.org/help/phonetics.html>
- <https://dictionary.cambridge.org/dictionary/english/lacerate>
- <https://dictionary.cambridge.org/dictionary/english/attacker>
-->

- **Centin:** /ˈsent.ɪn/ | Sent-in
- **Denrick:** /ˈden.rɪk/ | Den-rik
- **Eba:** /iː.ˈbɑː/ or /iː.ˈbə/ | Ee-bah or Ee-buh
- **Ebu:** /iː.ˈbuː/ | Ee-boo
- **Gemma:** /ˈdʒem.ˌmə/ | Jem-muh
- **Ildylia:** /ˈɪld.ɪl.iː.ˌə/ | Ild-ill-ee-uh
- **Ildylian:** /ˈɪld.ɪl.iː.ɪn/ | Ild-ill-ee-in
- **JaReel:** /dʒɑː.ˈriːl/ | Jaw-Reel
- **Lacerator:** /ˈlæs.ə.reɪt.ɚ/ | Lass-uh-rate-ur
- **Natia:** /nɑː.ˈʃə/ or /næʃ.ˈiː.ˌə/ | Gnaw-shuh or Nash-ee-uh
- **Nyla:** /naɪ.lə/ | Nigh-luh
- **Syreeta:** /ˈsaɪ.riː.ˌtə/ | Sigh-ree-tuh
- **Theila:** /ˈθiː.lə/ | Thee-luh
- **WoeNyl:** /woʊ.nɪl/ | Woe-Nil

### B. Discussion Guide

1. Given the choice, would you steal a friend's pain or shame if you knew you could never be rid of it? What about an enemy's?
2. What does it take for a person to sacrifice their well-being for others? Does the power to love come from within themselves or from an external source?
3. In the story, Theila appeared to be WoeNyl's enemy, but was really her friend. Have you ever misjudged someone as an opponent, when he or she was really a friend?
4. Centin betrayed WoeNyl and hurt her family. If you were WoeNyl, would you have asked Theila to spare Centin?

### C. Glossary

**Throse:** A unit of measurement, equivalent to 25 inches or 63.5 centimeters.

#### Animals

- **Lacerators:** Dumb scavengers, which feed on garbage and carrion. Their claws shred anything that interferes with their meals.
- **Venom-bats:** Small, dumb bats, which bite if their territory is entered.
- **Wool-deer:** Domesticated deer, which are raised for their bones, meat, milk, and wool.

#### Characters

- **Ankor:** A rebel from the Gateway Region of Ildylia. He duelled WoeNyl and lost. Flame's lover.
- **Centin:** Usurping world-steward of Ildylia. Husband of Natia. Divorced from WoeNyl.
- **Crystal Comfort:** Daughter of WoeNyl and Centin.
- **Denrick (aka Den):** Son of Strongweb. Guard at storehouse. Husband of Theila Sombermirth.
- **Strongweb:** Deposed Duke of the Gateway Region. Husband of Phyla.
- **Faithful:** Denrick's father. Gemma's husband. WoeNyl's friend.
- **Flame:** Ankor's lover.
- **Gemma (aka Silver Nectarfang):** Rightful world-stewardess of Ildylia. Slave-owner and twin of WoeNyl. Spymistress of Stonecourt. Wife of Faithful.
- **King JaReel:** King of Ildylia. Husband of Syreeta.
- **Luma:** Denrick's mother. Empress of Light.
- **Natia:** Usurping world-stewardess of Ildylia. Wife of Centin. She sold a love potion to Gemma.
- **Nyla:** The Grayling part of WoeNyl. She is a human unanimal from another world.
- **Queen Syreeta:** Queen of Ildylia. Because she was childless, she hosted a national tournament, the winner of which would become her heir. Wife of JaReel.
- **Theila Sombermirth:** A banished twilight-person who swore to marry the first male heir of the Empress of Light. She was born as Phyla's daughter. She is a Silkwing, who was implanted in Phyla and ensouled by WoeNyl. Wife of Denrick. Surrogate mother of Crystal Comfort.
- **WoeNyl:** A chimera of Nyla and Woethief. Under the control of Gemma's mind for most of their lives.
- **Woethief:** Rightful world-stewardess of Ildylia. The other half of WoeNyl. Steals pain and shame from other people.

#### Locations

- **Barren Caverns:** Sparsely populated land on the outskirts of Ildylia.
- **Forges:** Prison colony below Ildylia, just above the vents. All metal products originate in the forges.
- **Gallows Lane:** Most dangerous street in Stonecourt.
- **Gateway Region:** Southwest border of Ildylia. Formerly ruled by Strongweb.
- **Golden Death:** A cavern dedicated to execution. Those sentenced to death must drink a liquid that will turn them to gold if they are guilty or turn them to silver if they are innocent. A large bell hangs at the center.
- **Ildylia (country):** Land of the Ildylian. Ruled by Centin and Natia.
- **Ildylia (city):** Capital city of Ildylia. Ruled by King JaReel and Queen Syreeta.
- **Herald Street:** A street in a rich neighborhood of Ildylia (city).
- **Skinners' Crater:** Street near Stonecourt. Headquarters of the fiercest gang.
- **Stonecourt:** Slum in the upper Northwest of the city of Ildylia. Gemma's home.
- **Temple of Natia:** Natia's dwelling place and home to the great statue of Natia.
- **Vents:** Thermal vents below the inhabited regions of the world. The steam from these vents heats the caves above. At night fog from the vents reduces visibility and muffles noises.

#### People Groups

- **Celestial:**
  - **Star-People:** Star-people are people of light who watch each world from afar. They are forbidden to intervene personally in the affairs of each world on pain of banishment from the heavens.
  - **Twilight-People:** Twilight-people are celestial beings, made of gray light and silence.
- **Grayling:** Graylings are gray-skinned humans, who cry red tears, have excellent hearing, and see heat.
- **Ildylian:** This larger group includes bats, spiders, and full-bloods.
  - **Bat-People:** Bat-people are ensouled bats. They are blessed with wings which allow them to soar through their tunnels. They come in a multitude of colors, including white, black, and brown.
  - **Spider-People:** Spider-people are ensouled spiders. The blessing of the web allows them to weave all manner of useful objects with their silk. They possess three types of venom, one to nourish their children, one to show affection for their spouse, and one to slay their enemies.
  - **Ildylian Full-Bloods (Ilds):** The Ildylian are the perfection of beauty in Ildylia. Their possession of bat and spider blood renders them the loveliest creatures. Most Ilds despise those who do not possess the gifts of the web and of the wing.
- **Silkwings:** Silkwings are descendants of Theila Sombermirth, the first ensouled lacerator. 

