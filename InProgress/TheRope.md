<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# The Rope

A man is goven a special rope, which will always be the length he needs. To make fast money, he cuts it in half. He and the angry buyer now each have a rope that is always half the needed length.