<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Just a Drop

1. A man wants to revitalize his failing marriage.
2. He buys a love potion.
3. His wife drinks one half of the love potion.
4. His rival drinks the other half.
5. He has to win back his wife without help from a potion before the rival steals her.
