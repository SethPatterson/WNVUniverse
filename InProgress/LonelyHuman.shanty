// SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

// SPDX-License-Identifier: CC-BY-SA-4.0

!!TITLE Lonely Human
!!AUTHOR Seth Patterson
!!COPYRIGHT 2024 Seth Patterson
!!LICENSE CC-BY-SA 4.0 :link https://creativecommons.org/licenses/by-sa/4.0/
!!RATING AA
!!KEYWORDS Crystal Comfort,fantasy,romance,wish
!!DESC A human refugee lives in a city of songrats, bird-like people. Can he find a human to love in Roost City?
!!LANG EN
!!SMALL

#Glimpse

Jaren existed alone. Each day, he explored Roost City, which boasted a population of three million people. The inhabitants mostly ignored him, but some shook their heads at the "alien" refugee. He knew that one day, he would find another human to marry. He had been looking for four years.

Songrats and occasional aliens bustled past him on their way home from work. Their glittering tuft-rings showed that they had found spouses. The happy tweeting of children greeting their daddies reminded Jaren of his cold apartment.

"There must be someone here for me," Jaren muttered. As he ate kettle-corn on a park bench, a head of brown hair caught his attention. He craned his neck to catch a glimpse of her face, which pigeons took as an opportunity to snatch his snack from him.

A carriage pulled in front of the Brunette. By the time it left, she was gone.

"I'll find her," Jaren promised himself. "She must feel lonely like me."

He searched the park for an hour, but caught no more glimpses of the woman. The pigeons gobbled the rest of his kettle-corn out of the paper bag.

With a hanging head, Jaren returned to his apartment and trudged up the stairs. On his way, he bumped into Key-ley-ley.

"Did you find anyone?" the white-tufted songrat asked. Hope glimmered in her eyes like always. "I know she's out there."

"I think I saw someone, at least her hair," Jaren said. "I'll go back to the park tomorrow, just in case she comes back."

Key-ley-ley smiled, at least Jaren thought moving her beak that way counted as a smile. "I'm praying for you. This came in the mail." She pressed an envelope into his hand, then hopped up the stairs, trilling a hopeful melody.

Inside his apartment, Jaren read the bill for a matchmaking service, which had not found a single, interested human. He wanted to cancel his subscription and stop putting out lonely hearts ads in the newspaper, but Key-ley-ley's hope kept him going.

"God, I'm so lonely," he prayed, with a sigh, as he fell into bed. His mother's paintings reminded him of a homeworld he would never see again.

***

The next day, he caught no glimpse of the Brunette. The pigeons swooped toward him, then fled when they saw he had no popcorn. A pair of blue warblers worked together to build their nest. Soon, ugly, helpless chirpers would beg for worms from the park lawn. Jaren wanted that, a woman to build a nest and raise a family with.

He kicked a pebble down a path. Sorrow filled him. Since his parents died in a gas explosion when he was seventeen, he had been alone. Key-ley-ley's father gave him work and a warm place to sleep.

As he pondered, Key-ley-ley padded up behind him. "Daddy sent me to find you," she panted. "A pipe burst and he needs help now."

Jaren stared into space.

"Jaren," Key-ley-ley tugged on his hand. "The basement might flood."

Jaren snapped out of his trance and raced after his friend. Her unadorned head tuft bounced on her way to the apartment complex. Jaren struggled to keep up as she hopped along the tops of bicycle racks and rubbish bins to avoid the crowd. Her friends called her "Nimblepaws".

All night, Jaren, Run-run-run, and Key-ley-ley hauled water out of the basement in buckets. A blue-feathered renter berated them for shutting off the pipes to the water closet. The landlord apologized, but it took Key-ley-ley's silver voice and reassurances to get the renter to leave.

Plaster peeled off the walls and paper bags of grout burst open. Water hid in nooks and crannies, ruining food and trunks that belonged to renters.

Despite working all night, Key-ley-ley cooked a wonderful breakfast for the renters. The new pipe that Jaren had installed allowed her to use cooking water. Jaren fell asleep at the table, with his spoon of baked oatmeal in his hand.

#Aftermath

Despite their efforts, everything stored in the basement was destroyed. Angry renters demanded restitution, which their honest landlord dutifully offered. Repairs took Jaren weeks. To save his landlord money, Jaren shared his room with another renter instead of living in it for free.

After each workday, Jaren returned to the park. To the pigeons' chagrin, he could no longer afford kettle-corn. The rainbow of tuft feathers overwhelmed him as he searched for the human woman. "She was so much taller than the songrats," he muttered. "She must have been human. I wonder if she's a refugee like me."

On some days, Key-ley-ley accompanied Jaren. She taunted the pigeons and blue warblers with perfect imitations of their calls.

"The chirpers are gone," Jaren said as he examined the empty blue warbler nest.

"Don't look so sad," Key-ley-ley said. "Blue warblers mate for life. There will be more chicks next spring."

"My parents never got to see me leave the nest," Jaren said. "I still miss them."

"I miss your mommy's cookies," Key-ley-ley said. "She gave me her recipe, but I can't make them like she did. They never have the right crunch."

"Yours are still good," Jaren said. "You do a lot of things like she did. Red was her favorite color too. Sometimes, I think you do it to cheer me up."

Key-ley-ley looked away with her hands folded at her waist.

"You do, don't you?" Jaren asked.

"I know what it's like to lose a mommy," Key-ley-ley said as she smoothed her ruby dress. "I don't like to see you sad."

"You're my best friend," Jaren squeezed her shoulder. "You do so much for me. I feel like you and your father are family."

"I'm glad," Key-ley-ley said. "We'll find your mystery brunette some day. It's fun to help you search."

The friends walked home, chatting about the funny headdresses that rich women had started to wear and the new art exhibit mentioned in the newspaper.

#Visitor

The next day, I met Jaren at the park. I was a gray human with bat-wings and antennae, which marked me as an alien.

"Jaren," I called. "Please come here."

He turned to me, surprised to see a half-human girl.

"Who are you?" he asked.

"My name is Comfort Crystal," I said with a curtsey. "I'm a wish-granter."

Jaren wrinkled his eyebrows.

"I can grant one wish per person. I interpret wishes literally and grant them in an unexpected way," I said. "Please think carefully about what you want."

"Where are your parents?" he asked me.

"In other worlds," I replied. "I visit worlds in my dreams to grant wishes. I'm being serious about my offer. It won't hurt you to make a wish, unless you ask for something bad." I laughed nervously.

"Like what?" Jaren demanded.

"Like when a woman asked to be the most beautiful woman in the world and all the other women became ugly," I looked at my feet. "I didn't know how to use my gift back then. Now that I'm twelve, I'm a little better at it."

Jaren thought about finding the Brunette, having a family, or returning to his homeworld. Then he thought about Run-run-run and his financial problems. He wanted his boss to get the break he deserved.

"I wish for my landlord, Run-run-run, to be out of debt," Jaren said. "He's a great guy. It's not his fault a pipe broke."

I smiled. "Your mother painted lovely wildlife scenes from your homeworld. The Roost City Arts Curator will visit tomorrow and buy them for his museum. Your mother's art will be displayed for thousands of people to enjoy."

"How do you know?" Jaren asked.

"He wished to see the best painting in the world. The Curator has good taste, like me," I smiled. "I know about the paintings because your mother showed them to me five years ago. I like how she painted a cookie into the corner of each one. Her wish is the reason you made it to this world in safety."

Jaren pondered me.

"Yes, I was seven years old," I said. "People say I have advanced understanding for my age. Have a nice day."

I disappeared into another world to grant another wish.

***

While Jaren swept the hallways, a man visited. The man's spectacles gave Jaren the feeling that he noticed every detail around him.

"What can I do for you?" Jaren asked. The man's tailored clothes said he was too wealthy to be a renter. Jaren thought he might be a moneylender.

"I was told that a man, named Jaren, is interested in selling beautiful alien artifacts," the man announced. "Of course, I don't mean to imply that beauty can be purchased, but the necessities of this world force us to stoop to assign numbers to transcendent realities."

"I'm Jaren," Jaren leaned his broom in a corner. "Are you looking for paintings?"

"Certainly," the Curator said. "I was told that their excellence is unrivaled."

"I'll show you," Jaren motioned for the Curator to follow. "My mother painted them before she died. You can still smell the grass and flowers. She mixed her paints with  plant extracts, so they would be more lifelike."

In Jaren's room, the Curator wept as otherworldly beauty overwhelmed him.

"It is an honor," the Curator sniffled. "To find the pinnacle of artistic achievement, the strokes, the use of color, the sheer depth of emotion. To think that the public has been deprived of such a treasure for years, it's inconceivable and unjust!"

"I'd love for more people to see them," Jaren said. "My mother tried to teach me, but I never understood what she was talking about. I felt it though."

The Curator wrote a check with a calligraphic flourish. "I know it is not enough," the Curator said. "Our budget is tight this year, but please consider selling the paintings. They must be displayed."

Jaren looked at the document. Seventy thousand florins was more than fair. "I'll sell them," Jaren said. "As long as I'm allowed to visit them."

"Of course," the Curator said. "I want you to speak at their unveiling."

"How did you find me?" Jaren asked.

"An angelic being, named Crystal, gave me my quest," the Curator gloried. "She truly has led me to the great artistic work of our time."

The men arranged financial details and for workers to pick up the paintings. Jaren insisted that the check be made out to Run-run-run and delivered it to his boss.

Run-run-run stared at it. "I cannot take this, son," he said.

"I was forced to leave my home," Jaren said. "I would never want that to happen to you."

"You could buy a house with this money," Run-run-run said.

"I could, but my home is with you and Key-ley-ley," Jaren replied. "For her sake, please pay off your debts."

Jaren left the dumbstruck Run-run-run and returned to his sweeping.

#Finding

After work, Jaren felt elated. Blessing his boss and Key-ley-ley made him feel alive, the same way I feel when I grant a good wish.

On the way to the park, Jaren indulged in a bag of kettle-corn. Pigeons flocked to him, but he warded them off. As he sat on a bench, his heart fluttered. A breathtaking human sat on the bench opposite him.

"Greetings," she called. "I haven't seen you before."

Jaren mustered the courage to walk over to her.

"I saw you once, weeks ago," Jaren said. "I haven't talked to another full-human since my parents died."

"I've only been in Roost City for three months," she replied as she twirled her brown hair around her finger.

"I've been here for five years," Jaren said. "This might seem sudden, but would you have dinner with me?"

Before the Brunette could answer, a child ran up to her with his own bag of kettle-corn. An orange tuft stuck up from his brown hair.

"Mommy," he said. "Daddy took Lilly to the pond, but I didn't want to go, so I came back here."

"That's fine, sweetie," she said. "I'm just making a new friend."

"He's human like us," the boy said.

"I'm sorry. I didn't know you were married," Jaren stammered.

The Brunette held out a braid with a jade ring at the tip. "I don't have a tuft, so people don't always notice I'm married," she said. "I'm sure my husband would love to meet you."

Jaren covered his face in embarrassment and fled.

#Candlelight

On his way home, Key-ley-ley intercepted him. "I saw what happened," she said. "Before you went over to her, I was going to ask if I could walk with you." The normally hurried girl, took Jaren's hand to slow him. "I'm sorry, Jaren," she said as she hugged him. "I know how much you wanted to find her."

"I've been saving up," Jaren said. "I planned to take her to a nice restaurant and ask her to go steady with me. I would never have asked her out if I knew she was married."

"I know that," Key-ley-ley said.

The friends walked home in silence. At the lobby, Key-ley-ley had an idea. "What if I could give you a date?" she asked.

"How?" Jaren asked.

"I know I'm not human, but we can pretend," she said. "It can be practice for when you find the right girl. Meet me at my apartment at dinnertime."

Without giving him a chance to answer, Key-ley-ley hopped up the stairs. In her home, she explained what had happened to her father. He pitied the lonely, young man.

I appeared in their kitchen in a flash of gray light.

"Hello," I said. "I'm Crystal Comfort. I'm not an angel, or a devil, but you can call be a fairy if it makes you happy."

"How did you get here?" Run-run-run asked, more curious than threatened. As a twelve-year-old girl, people often trusted me automatically.

"I travel while I'm dreaming," I said. "I've already met Jaren and the Curator. I grant wishes. Here are the rules..."

"I wish Jaren would fall in love with a woman who would marry him and make him happy," Key-ley-ley blurted.

To fulfill her wish, I wove a dress of silk and twilight with the spinnerets in my wrists and a floating loom. In her room, I helped Key-lee-lee don the dress and preen her feathers.

Run-run-run and I prepared dinner as Key-ley-ley tidied the house in preparation for her "practice" date.

"I wish for Jaren to recognize the correct woman for him to love," Run-run-run said.

I smiled at him. "He won't be able to miss her."

"How do I look?" Key-ley-ley asked when she heard a knock at the door.

"Like a song from a dream," I replied.

Jaren held a bouquet of crimson carnations and wore his best clothes, namely the only trousers without oil stains.

"Greetings," As Key-ley-ley curtseyed, her dress shimmered, showing scenes from the paintings that Jaren's mother made. Her glossy, white feathers reflected the vibrant colors of the pink-grass and wiffle bushes of his homeworld.

"You look like art," Jaren stammered. The friends stared at each other for five minutes.

"Come in, Jaren," Run-run-run offered.

Jaren closed the door behind him and pulled out Key-ley-ley's chair in the candlelit corner of the kitchen, which held the table.

"I've never dated before," Jaren said. "I don't know what I'm supposed to do, but you're beautiful. I've never noticed it before."

"That might be too on the beak," Key-ley-ley laughed. "Maybe, you should start by asking what music she likes or where she grew up."

"But I already know that you love folk music and grew up here," Jaren said.

"But, for practice, ask me," Key-ley-ley said.

Run-run-run and I exchanged knowing glances. We served vegetable soup and fresh biscuits while we tried not to giggle at their absurd "practice" questions.

"You've been such a good friend to me," Jaren said. "I would want to be in love with you if I could."

"Why can't you?" Run-run-run demanded. "Why can't you see what's as plain as the beak on your face?"

"What do you mean?" Jaren asked.

"You two love each other," Run-run-run said. "You have since you were kids, but you didn't notice because you were so focused on finding a human."

Jaren looked like he was about to cry.

"I think I know what the problem is," I said as I took Jaren's hand. "Jaren, there's nothing evil about you marrying a songrat."

"There isn't?" he asked.

"Of course not," I said. "Songrats are descended from humans. They just have some cosmetic differences. You and Key-ley-ley both have souls, so loving her is right."

Hope filled Jaren's eyes.

"Yes, you will have children together," I anticipated his question. "Just like the Brunette did with her husband. They'll have your height and their mother's lovely singing."

"May I ask her?" Jaren turned to Run-run-run.

"I've been waiting for you to, son," Run-run-run said.

"Yes," Key-ley-ley said. "I'll marry you." She threw her arms around him.

"I was going to ask you to go steady," Jaren said.

"Oh," Key-ley-ley said. "I'll do that to. Sorry, I'm not meaning to rush you."

"It's fine," Jaren said. "If your father approves, there's no need to delay. We know each other well and this angel seems to think it's a good idea."

Jaren placed his mother's ring on Key-ley-ley's finger to signify their betrothal. They giggled and discussed their plans over dessert.

After Jaren left, I hugged Key-ley-ley. "You two will be happy together," I said. "Just remember, I'm a dream-traveler, not an angel."

***

Five weeks later, the museum hosted the unveiling of the paintings. Jaren spoke of the love that flowed from his mother's heart into every stroke. He teared up as good memories of someone he would never see again in this life swirled inside him.

Before a crowd of art-enthusiasts, family, and friends, Jaren and Key-ley-ley exchanged vows, rings, and kisses. The sea of red flowers around them was small compared to the ocean of love in their now united heart.

The plants in the paintings swayed in another planet's wind as the vitality of the couple brought them to life. As Jaren's mother had used a paint portal to escape to the safety of Roost City, now a paint portal opened to the beauty of another world.

This time, Key-ley-ley had made the cookies with the right crunch. The guests, including the Brunette and me, enjoyed them as Jaren enjoyed the sweetness of his wife's hand in his.