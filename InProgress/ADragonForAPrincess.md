<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# A Dragon for a Princess

1. Whoever slays the dragon may marry the princess.
2. Jean is a poor hunter who takes on the challenge.
3. Jean sets traps and stalks.
4. Traps are sprung with no dragon in them.
5. Rival dies. Dragon buries him with a cross on the grave.
6. Jean confronts dragon. She refuses to fight back.
7. Jean follows her to her den to escape the storm.
8. The den is home-like and organized.
9. Jean plays a pan-pipe for her.
10. Jean defends her against hunters.
11. They flee to the castle.
12. The king is furious that the dragon isn't dead.
13. Jean is imprisoned.
14. Dragon escapes.
15. The princess pursues the dragon.
16. Dragon talks for the first time because it is the only way to convince
    the king to spare Jean.
17. The dragon takes on human form because the princess used a wish from
    Crystal that allowed her to make one thing humanoid.
18. The dragon makes the princess more beautiful, intelligent, and athletic.
19. Jean is freed after the dragon proves she is a person.
20. The king offers the princess to him.
21. Jean asks to marry the dragon instead even though she is plain with drab
    coloring.

The dragon has power to make good physical things abound. She improves crop
yield, health, and equipment longevity. She can only choose one specific person
to bless with amazing physical well-being. All other blessings are vague and
general.
