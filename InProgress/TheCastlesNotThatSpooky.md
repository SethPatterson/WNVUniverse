<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# The Castle's Not that Spooky

0. The military is on a mission to destroy the Duchess because the radius of
   hideousness from her castle is expanding and destroying land.
1. An artillery caravan loses a few porters to desertion.
2. The soldiers force a village store clerk and a few other civilians to be
   porters.
3. The clerk is too scared to protest.
4. At the edge of the twisted woods, the soldiers don gas masks. There are
   none for the porters to wear.
5. They make the clerk help them dig in pits for the artillery to hide it from view.
6. Screams, which are actually strange birds, sound from the castle all night.
7. The clerk asks if they will give the Duchess a chance to surrender.
8. The clerk volunteers to go in and talk to her. They grudgingly give him a gas mask.
9. The clerk passes a multi-headed guard dogs and is admitted by monstrously
   deformed former-humans.
10. The clerk's knees shake the whole time he asks her to surrender.
11. She refuses to surrender and orders her people to leave the castle.
12. None of her people leave, but defend the castle from battlements with
    muzzleloaders.
13. The Duchess begs the clerk to leave before she corrupts him.
14. He refuses to leave.
15. The Duchess leaches beauty from the creatures around her involuntarily.
16. The artillery destroys the castle.
17. The clerk drags the wounded people away from fires.
18. The artillerists launch gas shells.
19. The Duchess was widowed 60 years ago and gained her corruption.
20. Her people stayed with her anyway, despite how the Duke cursed her.
21. The clerk puts his gas mask on the Duchess.
22. That act of true love breaks the corruption. Beauty surges through the
    castle, clearing away the gas.
23. Vines overtake the artillery and flowers sprout from the soldier.
24. The clerk becomes hideous so the Duchess does not need to.
25. The clerk gets to be the manager of the castle storeroom.
