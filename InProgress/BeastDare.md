<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Beast Dare

A man gets drunk and swears to confront a dangerous beast.

The beast had killed many soldiers who threatened it in her home. The beast gave them three options, fight and die, run to safety, or kiss it. Most had chosen to fight.

The man goes unarmed to the beast and kisses its forehead.

The beast takes on a humanoid shape. She asks the man for his hand in marriage, so she can keep her line alive.

The man is already married, but asks the beast to wait for his son to come of age.

The beast agrees but the man dies shortly after leaving the beast's lair.

The son learns of and fulfils his duty to the dragon.

<!-- Inspired by Damsel. -->

***

"You did it?" the beast asked.

"I have no weapon and I will not tell my son that I ran in fear," Captain Ichabod said.

"Blessed are you. I have waited centuries for this opportunity," the beast said. "Now, I pray you to wed me. I am the last of my kind, but you can save me from extinction. I trust no other man."

Ichabod rubbed the back of his neck and looked away. "I am happily married. Frankly, my wife is a lot nicer to look at too."

Tears formed in the beast's eyes. If she did not wed today, she would return to her beast form until kissed again. "Forgive me for asking. I am happy that a good man like you has a wife. Pray tell, how old is your son?"

"He has endured ten winters," Ichabod said.

"Is he pledged to anyone?" the beast asked.

"No," Ichabod shifted his feet. "We are still saving for a dowry."

"Pray, pledge him to me," the beast said. "I can wait another eight years for him. We Brwathk live milennia."

"I must discuss the arangement with Claudelle," Ichabod said. "I will return in a week with my answer."

"Thank you," Drexel said. She winced as she removed something from her forehead. "As a token, take this scale for your wife. It will make her feel young for the rest of her life if she places it on her forehead. Tell no one but her where you got it."

The irridescent hexagon glinted in the light from Drexel's eyes.

"I am honored, your excellence," Ichabod saluted and strode out of the cavern.

***

On his way home, Ichabod met ten highwaymen, who stole his armor and left his body in a ditch. The pried the Brwathk scale from his cold fingers and fled.

***

_Sixteen Years Later_

Halbert pressed his body lower atop the ridge, hoping the bandits would not see him. The few who survived the bandit's captivity told dreadful tales and bore more dreadful scars.

Halbert counted twenty-nine men, heavily armed and armored. Their two hostages were tied to stakes in the center of the camp, with hounds guarding them.

Twigs cracked to Halbert's left. He tightened his grip on his dagger and tensed. When the person came into reach, he pounced and pressed his dagger to her throat.

Her palm shoved into his eye, blinding him with green and pink light.

"Let me go," she whispered as she saw Halbert's badge under his cloak. "I ain't no bandit."

"What are you doing?" Halbert hissed. "Get that light out of my eye."

"Gathering herbs and berries," she said as she closed her palm to hide the light. "That aint illegal. These woods belong to the people, not the crown."

"Are you armed? Will you hurt me if I sheathe my knife?"

"O' course I's armed. I ain't a fool," she said. "I won't hurt you. Healer's honor."

Halbert sheathed his knife.

"I will help you get the hostages free," she said. "You promise none of our womenfolk or children will get hurt when you soldiers come."

"Why would you help?" Halbert whispered.

"The spider on my shoulder told me to," she said. "She wants to stop the war, but the bandits are too leather-brained to listen. Not me, she got to me when I ws a tyke. Learned me about being a proper lady."

"I'll untie them at midnight if you swear on a soldier's dagger to spare our women and children," she said.

She slowly drew a dagger and held its hilt toward Halbert. The hilt bore an inscription.

"Ichabod son of Pious"

"Where did you get this?" Halbert asked.

"Me ebu took it from a poor bloke he killed afore I was born," she said. "I won it in a footrace."

"It was my father's," Halbert said.

"How do you know?" she asked.

"The inscription says his name," Halbert said. "Can't you read?"

"O' course, but not Common," she said. "The spider taught me Ildylian and High Darkish though. Tough business for you though. I'm sure your ebu was a swell chap," she said. "After you swear, you can keep it. I aint no thief."

Halbert swore. He didn't trust the girl and expected an ambush.

Fifty men crept through the moonless night with dark blue tunics covering their armor. They killed the bandit sentries and encircled the camp.

The girl waited in the appointed meeting place with the hostages.

"Where are the others?" Sir Huron wrapped his gauntlet around her throat.

"There are no others" the girl wheezed.

"We know you bandits took six hostages," Sir Huron said.

"They could be at other camps," the girl said.

"Nonsense," Sir Huron punched the girl in the side, breaking her ribs. "Tell me."

"Sir, I swore that none of their women and children would be harmed," Halbert said. "I only saw two hostages when I scouted. Let go of her throat."

"There are no women or children here, just devils," Sir Huron said. "Attack."

Crossbowmen poured bolts into the camp, aiming by the light of the campfires below. Anything that moved was hit.

Halbert held the girl back, unable to stop the attack, but not wanting her to be harmed.

When we, the spider, spread a fog of twilight over the camp, Sir Huron smashed his fist into the girl's face. Our twilight vanished as she silenced out.

Halbert stood over the girl and roared, "How dare you? She was trying to protect innocents."

Sir Huron broke Halbert's jaw and drove his knee into Halbert's stomach.

Halbert crawled to the girl and checked for a pulse. Her heart beat steadily.

Screams and shouts sounded from below as flaming bolts hit the tents.

Sir Huron ordered his squire to take Halbert's badge and weapons, then lash him.

After the battle, Sir Huron bound Halbert and the girl's wrists together, with their backs to a tree.

"Thus shall it be done to cowards who turn away in the face of battle," Sir Huron announced. "The bandit vermin must be exterminated, yet these fools tried to thwart us."

A paper, nailed to the tree said:

"These traitors helped to destroy the bandits, then tried to destroy the Crown's soldiers."

Sir Huron ordered that Halbert be branded with the upside down image of his badge on his right cheek.

***

At dawn, the stench of burning flesh greeted Erisebet's nostrils. No man, woman, or child survived from the camp. Seven soldiers had died in the night too.

"We need to go," Erisebet wheezed. "Other bandits will kill us if they find us."

We calmed Erisebet's nerves as she worked to saw the ropes against the rough bark of the tree.

*'You can trust Halbert,'* Woethief, a spider of twilight, one of our minds, whispered into Erisebet's mind. *'He did what he could to help you.'*

As Erisebet worked, Nyla, our other mind, healed Erisebet's wounds. We wished we could gnaw the ropes for her, but we were only semi-corporeal.

Halbert's jaw throbbed, so Woethief stole the pain to allow him to cut the ropes. When they freed one pair of wrists, bandits from another camp approached.

Enraged yells escaped their lips as they saw the carnage and spotted Erisebet.

"Run," Erisebet yelled as she dragged Halbert behind her by his wrist.

He staggered, but Woethief kept stealing his pain. Erisebet knew the wood better than her pursuers, so they lost them after an hour.

"They'll bring the hounds soon," she said. "We need to get somewhere too steep for them to reach."

With a sharp stone, Erisebet sawed the second rope. Halbert's pain returned as soon as he stopped touching Erisebet's arm.

"There's a ravine a league from here," Erisebet said. "You can rest there."

She knelt and scooped pure water from a muddy puddle. Our power of orderliness purified it instantly. She drank, then offered it to Halbert.

Halbert was pale from the stripes on his back. He needed medical attention, but there was no time for it.

"Hold my hand," Erisebet said. "The spider will ease the pain."

They raced through the brambles, not caring about the tracks they left. Erisebet half-carried Halbert, who was much bigger than her.

With hounds sniffing behind them, they reached the ravine. Halbert scrambled halfway down, then slipped, breaking his left femur at the bottom.

Erisebet shoved her glowing hand in a dog's eyes as it lunged for her. The dog blinked in confusion and she deftly climbed down the wall of tangled roots and jagged stones.

"They don't like my light," she said as she held her palm open to them.

Erisebet hooked her hsnds under Halbert's arms and dragged him toward a crevace in the wall, too frightened to check for a spinal injury first. The crevace led into a tunnel, littered with bones and blocked by spiderwebs.

Woethief, the spider, sat on her shoulder and gave directions as she dragged her friend. A fist-sized spider dropped onto her head.

"Sorry," she held out her hand for the venemous creature to climb onto. "Please make a web behind us to cover our tracks."

The spider looked into Woethief's eyes, bowed, and obeyed, making a perfectly symmetrical web across the tunnel.

After traveling a bowshot further, Erisebet rested to examine Halbert's leg. The femur protruded from the side. Our order sterilized our surroundings and we prayed for a miracle. The bone fragments returned to their proper place, but we could not heal Halbert completely.

A swarm of spiders wrapped the wound in a silk bandage. Erisebet tore strips of her hem to tie a splint of animal bone to his leg.

Voices and brays sounded from the tunnel, getting close enough for Erisebet to see their torchlight.

"They must have lowered the dogs with ropes. Kill the dogs," Erisebet ordered the spiders.

Yelps of agony escaped the hounds as venom overtook them. Bandits cursed and crawled over the corpses.

An inhuman cry sounded from deeper in the tunnel.

"We need to leave, now," Erisebet put Halbert's arm over her shoulder and he hobbled with her until the tunnel narrowed so he could only fit sideways. His splint caught on a jagged rock, so Erisebet fumbled to free it.

When the bandits saw six eyes leering over her head, they turned and fled.

"Stay back," she held up her palm, but the creature with the hexagonal eyes, grabbed Halbert by the shoulder and tugged him free. He screamed and silenced out.

"You can't hurt him," Erisebet yelled as the beast clamped her jaws around Halburt's injured leg.

When the wound disappeared, Erisebet dropped the stone she had been carrying.

"What did you just do?" she asked.

"With my teeth and tongue," I heal, the beast said. "With my claws I hunt."

The beast rolled the unconscious Halbert over and licked the stripes on his back until they all disappeared without leaving scars.

"I did not think you would ever come, Claudelle," the beast said. "You are ten years late, but I assume this is the son you pledged to me. You have not aged a bit."

*'Be careful,'* Woethief warned. *'This creature conceals anger and resentment.'*

"I aint Claudelle," Erisebet said. "I be Erisebet the spider-fool."

*'That was a nickname, not your real name,'* Nyla thought. *'You can be honest without being blunt.'*

"Then why do you carry the scale I gave to her husband?" 

"Me ebu stole it from a man he killed," Erisabet looked sideways. "When I was a suckling, I grabbed it and it stuck to my hand."

"How dare you come here, thief?" the beast roared from the mough between its eyes.

"I aint no thief," Erisebet said. "I came because thieves are chasing us. This man saved my life, so I owe him."

"We shall see what he says when he awakens," the beast said. "Follow me," she lifted Halbert with two tentacles connected to the top of her head.

Erisebet memorized the labyrynth of passages on their way to the "prison room".

"Lean against that wall," the beast ordered. as she pointed to thick slime.

"What will you do to me if I don't?" she asked.

"Chase you back to where I found you," the beast said.

"Will you eat me?" Erisebet asked.

"Of course not, but you intruded into my home, wearing a stolen scale, so can you blame me for keeping you imprisoned until I know youvare no threat?" the beast asked.

"Promise to take care of him," Erisebet said.

"Of course," the beast picked Erisebet up and stuck her to the wall.
