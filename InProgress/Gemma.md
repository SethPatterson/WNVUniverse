<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Gemma

## 0 Introduction

My name is Gemma. At least it was Gemma.

Now they call me Silver Nectarfang. My husband named me that. At least they did call me Silver once, before I needed to hide.

I'm trying; I really am.

They keep coming. It seems like everyone is against me, or dead.

My husband, Faithful, oh you don't know him. He is the most wonderful man who ever lived. He died. I still feel empty after our minds untethered.

And Nyla Woethief. She died too. Because of me. I only learned to love her after she died. I owe everything I am now to her.

Centin, he’s against me. He is hunting me always. I can never rest, not really. He has spies and assassins everywhere.

There’s Queen Syreeta. She adopted me, but for political reasons, she disowned me a short time later. She would be a kind mother except she’s a coward. I don't like cowards. I should know. I am one.

I suppose Flame loves me. She is a lovely girl. Her mother loves me too. I don't know what Ankor thinks of me. Oh, Ankor, you don't know him either. Well, he’s Flame’s man. Or at least he wants to be. He just needs to keep out of trouble long enough to prove that she should marry him. Their kids are adorable.

My kids are adorable too. They look so much like their father. I pray that they take after him.

I'm not as good of a storyteller as WoeNyl. I'll try. Maybe if she reads it, she'll understand me better.

## 1 Gemma Ordered to Assassinate Woenyl and Faithful

I received an unusual request. Clients rarely contacted me directly. I read the raised writing with my fingertips and memorized the instructions. The payout was larger than normal.

I dropped the paper in a container of acid. Paper trails led to a shooting squad, or worse.

My mark was a man from Stonecourt, ugh. I wished the King would raze the whole city. Their thugs made our criminals look like saints. I killed people, but I didn't make them suffer… for long… usually.

I skulked outside his flat, disguised as a peasant. Normally, I sent my associates on errands like this, but for the amount of silver my contact offered, I could not afford any slip-ups. Besides, my only trustworthy operative was in the duels. The fool thought she could win the duels and become "Princess". Why did I let her participate when she had so much important work to do?

I crept outside his window, my eight legs making no sound. The man sat in a corner reading with his fingers. His sword hung on a peg on the opposite end of the wall. His shrine bore no figure in it. Most people kept a statue of Natia or Silence or Granite to protect their house, but no one had nothing.

*'Too bad my contract won't let me kill him yet. He would be so easy to take right now,'* I thought.

I appraised my mark. One could never tell when you might have to fight your mark. A bandage obscured much of his face. They said he got punched hard in a duel yesterday. He deserved it: Stonecourt trash.

As I crept away from his window, he heard me. I froze.

**"Are you hungry?"** he asked.

I turned as he opened the door. Playing dumb typically worked in these sorts of situations.

He glanced at my shabby clothes and rumpled hair.

**"Yes,"** I stared at my feet.

**"Come, I have enough for both of us,"** he said.

I followed him to an outdoor table. If he tried to attack me, I could plunge my knife into him before he could react.

I rested my abdomen on a stool as my legs sprawled across the patio. He poured a glass of wooldeer blood and set a bowl of grubs before me. The meal was simple
and of poor quality.

**"Thank you for inviting me to eat with you,"** I said. **"I haven't eaten since yesterday."**

Of course I lied to him. I had a wonderful breakfast of raw steaks, loaf plant, and duck eggs that morning.

We ate in silence. He never smiled. I detected an aura of fear in him. WoeNyl was better at reading people. I wished I could tell what scared him.

**"What is your name?"** he asked.

**"Ree,"** I lied as I skewered another slug on my fingernail. I preferred mine dead, not wriggling like some people. **"Just wandered here from Chalk Temple."**

**"I'm Faithful,"** he said. **"Please tell me if you need any help adjusting to this city. I am a stranger too, but I might be able to help a little."**

**"I would like that,"** I said with no intention to see him again before I killed him.

**"Do you mind if I pray now? I always pray after meals."** he said.

**"No, but who will you pray to?"** I asked. **"I noticed that your shrine is empty."**

**"I worship the True God,"** he said.

**"I suppose we all think our gods are true,"** I said. **"Pray to whomever you wish."**

He prayed a strange prayer to his strange God.

**"God, thank you that I was able to help this girl today,"** he prayed. **"Please help her to adjust to this new city and find friends who will lead her to you. Amen"**

He offered to walk me home. I refused, but he insisted that I have an escort as the fog of night approached.

I bowed to him to thank him for the meal, but my dagger peeked out from my tunic. He recognized the assassin’s emblem on the hilt.

## 2. ~~Gemma convinces Natia to give her a new form in exchange for wealth.~~

## 3. Gemma Changes Form

## 4. Gemma Marries Faithful

## 5. Faithful Dies in Duel

## 6. Gemma Coronated

## 7. Gemma Almost Assassinated

## 8. Gemma Imprisoned

## 9. WoeNyl Gives Orderer Stone to Gemma

## 10. WoeNyl Dies

## 11. Gemma hires Flame and her mother as servants

## 12. Syreeta Disowns Gemma

## 13. Gemma returns to slums

## 14. Treblets Born

## 15. Gemma’S Gang Thwarts Syreeta’S Assassination

## 16. Gemma Promises Syreeta that She Will Have a Child Who Shares Woenyl’S Eyes

## 17. Centin Sends Assassins After Gemma

## 18. Gemma is Forced to Leave Her Children With Flame and Hunt Assassins

## 19. Gemma Finds Out that Assassins were Hired by Centin

## 20. Gemma Confronts Centin

## 21. Centin Tries to Hurt Syreeta, so Gemma Tackles Him

## 22 Centin falls over a railing, hits his wing, and dies

## 23 Syreeta’s Announcement

<!-- Syreeta tells Gemma that she is pregnant -->

Syreeta leaned against a pillar with her chest heaving rapidly. I approached slowly.

**"Highness, are you unharmed?"** I asked.

Fear radiated from her face.

**"Yes,"** she thrummed.

She eared her dead guards.

**"Before you kill me, there’s something you must know."** she said.

**"I just saved you, majesty. You are my Queen,"** I said.

**"I'm pregnant."** she threw herself down before me. **"You must spare me."**

I stooped and held her face in mine.

**"That’s wonderful, highness,"** I said. **"I prayed for this news."**

**"Why would you pray for me?"** she asked.

**"Even though you disowned me,"** I said. **"I haven't been able to stop thinking of you as my mother."**

**"But how did you know I would have a child?"** Syreeta wondered.

**"The Orderer’s power wanted me to tell you,"** I said. **"It is a son by the way."**

**"But why would you perform this kindness for me."** she asked. **"You healed me, didn't you?"**

**"Eba, I love you. And yes, the Orderer did channel her power through me to heal you."** I said.**"God gave the Orderer that power. You should thank him."**

**"Which god?"** Syreeta asked.

**"The God of Nyla Woethief. He healed you,"** I said.

**"Then bless him!"** she exclaimed. **"Will you teach him of that God?"**

I wished I could. I wanted to see her son grow and prosper.

**"I wish I could. But I have my own family to protect."** I said. **"Everyone will want to kill me now that Centin is dead."**

I reached a trembling hand into my pocket. My claws closed around my most prized possession: a fragment of the holy writings. Could I give it up.

**"This will guide him to worship my God,"** I handed her my priceless gift. **"Promise that you will raise him to serve WoeNyl’s God."**

**"I promise,"** Syreeta said.

**"I must go,"** I said.

**"Wait, I can help you,"** she said.

**"No, you can't,"** I said **"There are dangers you know nothing of."**

I kissed her forehead.

**"I love you,"** I said.

**"I love you too,"** Syreeta replied. **"You are the only person I can trust. I am sorry that we were forced to replace you."**

I rose and scanned my environment for threats. I flew over the edge that Centin fell from.

## 24 Gemma is Banished to the Forges
