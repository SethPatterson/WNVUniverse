<!--

SPDX-FileCopyrightText: 2023 Riley Duffield <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Invincible Victim

"Anything beautiful, you want to pervert. Anything true, you want to twist. Anything good, you want to stamp out," Brannigan said.

"Anything that does not serve the Movement must be swept away," the Captain said and punched Brannigan.

"Is it frustrating," Brannigan spat blood. "Fighting a losing battle?"

"We will not lose. The Movement will progress through the arc of history," the Captain announced and lifted Brannigan from his chair.

"You cannot kill goodness, truth, or beauty. They are where God is, and he's everywhere," Brannigan said.

"I can kill you," the Captain heaved Brannigan through the stained-glass window. Broken bits of red glass drew drops of red blood.

"It looks like the Movement won," the Captain sneered through the fractured window. A spiderweb of yellows and blues distorted the image of Brannigan's crumpled body.

As the Captain walked away, light from a street lamp filtered through image at the top of the window: a rising sun. Holy poetry adorned the splintered pulpit and lupine grew from it toward a hole in the ceiling.

<!-- This is inspired by the 1969-01-24 episode of the Twilight Zone, "He's Alive" and by Episode 726 of Adventures in Odyssey, Push the Red Button. -->

