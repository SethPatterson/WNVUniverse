<!--
SPDX-FileCopyrightText: 2021 Seth Patterson

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Killing Long

1. WNV sneaks aboard an enemy airship at a narrow pass.
2. She plants a time-bomb on the ship.
3. She disembarks after the ship docks.
4. She is caught.
5. The airship explodes along with the other four docked airships, destroying the entire fleet.
6. She is forced to heal Long's daughter because she was injured in a secondary explosion at an ammunition depot.
7. She is interrogated without revealing any information.
8. While she hangs from her hands, she musters enough strength to kick Long in the face when he enters her cell.
9. Long dies when he hits his head.
10. When a guard rushes in, WNV steals his revolver with her hair and shoots him.
11. A second guard kills her.
12. Her corpse is transported to the temporary morgue.
13. When she wakes up, she calls for help.
14. She convinces the chief medic to allow her to help heal the burn victims.
15. The people in the nearby village revolt during the chaos.
16. WNV tries to negotiate the surrender of the survivors while they wait for the Cambrian army to arrive.
17. After the Cambrian army arrives, the revolting villagers calm down.
18. WNV helps smuggle Long's daughter away since she was a non-combatant and had nothing to do with her father's coup.
19. Long's daughter, who looks like WNV because of WNV's healing, settles in the village with the blue bees.
