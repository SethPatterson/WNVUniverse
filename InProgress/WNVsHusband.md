<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# WNV's Husband

Since WNV was unable to marry Tearborn and Syreeta's son, she eventually found herself in a world where she was required to go through a series of grueling test.

She passed all the tests and at the end was required to marry a tyrant. The tyrant resembled a bat, except he had eight eyes and no way of using echolocation.

WNV discovered that a Centin was controlling his mind. Centin had created the alternate Ild worlds. The other Centins were duplicates of himself. Most other people in the Ild worlds were original people, except for those that Centin thought were most interesting.

An assassination attempt left Faithful in a coma. WNV helped to keep the government stable while he was unconscious. Her speeches encouraged the people and her inquiries helped reveal corrupt officials.

As Centin, the acting emperor, ran the country, WNV detected his plans to take control permanently.

She heard rumors of a woethief operating in the slums and found a woman in her forties named Gemma. With her help, Gemma was freed from slavery and became WNV's assistant. Gemma had been forced to steal Centin's woes, which made his treachery more difficult to detect.

Together, she and the newly discovered Gemma defeated Centin, who had taken Faithful hostage. WNV killed him to save her husband but felt guilty for receiving Centin's powers when she thought the new Gemma deserved them.

Since WNV offered to give her new powers to Gemma, she made a request before WNV could. The newly discovered Gemma asked for part of her to be given to Silver, so Silver could be complete. Transferring part of the new Gemma to Silver across dimensions was so exhausting that WNV fell unconscious after the transfer.

When she woke up, a pile of Gemmma's clothes remained, but WNV did not know where Gemma went. She discovered that Woethief was now whole. Woethief explained that the Gemma of this world was the same person as Woethief all along but was not powerful enough to reunite with herself before.

When Silver had split off what she thought was Woethief, she did not give it to Nyla. She actually gave it to Gemma in another dimension, who lost the part of herself that became known as Woethief.

The now-complete WNV had the spider-attributes she longed for in her natural form. She could spin webs with her wrists, thrum, and had fangs. She also had nine eyes (two of the eyes were donated to Silver's Gemma form).

When WNV claimed the throne as the true steward of Ildylia, all versions of Ildylia were merged along with the Darkish Lands and Ella's forest.

After Faithful was freed from Centin's mind control, WNV discovered that he was a Faithful. Without the mind control, Faithful fell in love with his wife and they held a second wedding since the first one had no ceremony.
