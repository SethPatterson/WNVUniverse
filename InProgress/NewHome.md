<!--

SPDX-FileCopyrightText: 2022 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# New Home

## Journey

Den and I emerged from the portal, into a forest with trees, much smaller than those of a previous world, which I couldn't quite remember.

I clutched my daughter, Crystal Comfort, as I leaned against the back of a cart. Den helped the other men pull the vehicle for the three-and-a-half day journey to the nearest town.

At night, by the campfire, Den held my hand and stroked Crystal's furry ears. She looked at Den and I with intense affection and seemed to understand everything we said. Her coos made us smile.

To ease his fatigue, I shared my love-venom with Den. Since I was half-spider, my fangs had three types of venom. Love-venom, I shared only with my husband. Family-venom, I shared with Crystal to pass on immunity to sicknesses I had before. I saved my death-venom for self-defense.

Because of my spiderness and Enna being a mink, our traveling companions feared us. My eight legs stood out like a fox in a hen-house, but Den liked how they looked.

"When we get where we're going," the group leader said. "Where will you stay?"

"There must be some lodging to be had in exchange for work," I said.

"What are you good at?" he asked.

"I don't know," I replied. "We all lost our memories a few weeks ago."

"We have too many unskilled workers. You won't find work unless you have a special skill," he said.

"We'll do whatever it takes to take care of Crystal," I said.

## Arrival

At the entrance of the town, the Sheriff and his deputies detained us for immigration processing. As a clerk asked us questions, I didn't know how to explain my past.

"What's your place of birth?" she asked.

I didn't know.

*'Ildylia,'* a voice in my head said. *'Den was born there too.'*

Despite her benign words, the voice terrified me. I rocked Crystal as she fussed.

"Ildylia," I said.

"How do you spell that?" the clerk asked.

"I don't know," I replied. "I don't even know where it is."

After we tried to answer questions all day, the deputies locked metal bracelets on our wrists.

"Wear these for six months, without breaking any laws, then you'll become a citizen," the clerk said.

Our stomachs growled and our eyes drooped, but we had nowhere to turn. Because of our alien appearances, no one trusted us. We slept in a clearing outside of town, without tents or a meal.

## Finding Work

In the morning, Den and Four went to look for work. They found none. We were hungry.

When I entered the city to look for work, children ran from me and hid behind their mothers. I earned a loaf of bread by scrubbing soot out of an oven. I felt faint from hunger and I was still bleeding from giving birth.

Den and Four hauled stones to a construction site, but the next day, someone else took the job.

We scraped by for weeks, with no shelter and little food. I wove a carrier and diapers for Crystal.

"Can you sell your silk?" Ella asked.

"The things I make with it are clumsy," I replied. "Who would pay for them?"

"Try it," she said.

I couldn't spare much silk (I needed pads for myself and diapers for Crystal), but I rolled a little more around a stick each day.

***

At noon, a man visited our camp with a job offer.

"I need a beehive from the top of a tree," he said. "I'll pay one silver ducat if you get it for me."

"Why can't you get it?" I asked the athletic man.

"Only she's light enough," he pointed at Enna. "I can't send a child because it's illegal. They don't want children falling out of trees."

We followed him to a tree, which smelled sweet. Insects buzzed at the top, 40 meters above us. We had never heard of bees.

"You're pregnant," I said to Enna. "Is it safe to climb that?"

"We'll get a week's worth of food. I can't afford to refuse."

She scurried up the tree with ease. To my horror, spikes poked out of the trunk below her as she climbed. She would be cut on the way down.

At the top, Enna clutched the hive, but discovered that bees sting. She dropped the hive, which cracked slightly on the ground. In her agony, she scrambled down on the spikes.

After the bees stopped stinging her, she turned to the man to ask for her ducat.

"You cracked it, so it's only worth half as much," he said.

He gave her coins worth half a ducat, wrapped the hive in a bag, and left. [^1]

I helped Enna limp back to camp and used all the silk I had saved to bandage her.

When Den and Four returned, they fumed. They saw the man sell the hive for 50 silver ducats, 100 times what he paid Enna.

Four clenched his fists and stomped toward town.

"Den," I said. "Please stop him."

Den subdued Four and carried him back to camp. When they returned, I spoke, "We can't make any enemies here. If you start a fight, you'll lose your chance to become a citizen."

"He cheated my wife and caused her to get cut up," Four said.

"Then, get justice the right way," I replied. "Tell the Sheriff. The man said it's illegal to pay children to climb. Since Enna's pregnant, the law probably applies to her."

That calmed Four down, so I "sat" down to play with Crystal. She had been babbling since she was born, but that evening, she said, "Eba".

I wondered what the noises meant to her. When Den picked her up, she said, "Ebu". To prove it wasn't babbling, Crystal called Enna and Four by their names. My jaw dropped.

"She can't really know our names," Enna said. "She's too young."

"Enna," Crystal said.

"Who's he?" Enna pointed at Four.

"Four," Crystal, who was six weeks old, smiled.

"What is an 'Eba'?" Enna asked and folded her arms.

Crystal tried to point, but her wild flailing showed she had normal motor control for her age.

"Is she 'Eba'?" Enna pointed at me.

"Yeah," Crystal said.

"That's not her name," Enna said. "It's Thee'."

"Thee' my Eba," Crystal exclaimed.

"Is she an Eba?" I asked about Enna.

"Yeah," Crystal said.

Den knew what "Eba" meant, but of course, he couldn't tell us. Crystal fell asleep, so she couldn't tell us either.

## Seeking Justice

The next morning, Four, Enna, and I went to the Sheriff to report the beehive man.

"Were there any other witnesses besides Theila?" he asked. "You need at least two to file a formal complaint."

"Bad man hurt Enna," Crystal shouted.

The Sheriff stared at her.

"Did you see it?" he asked.

"Yeah," Crystal told him what happened in simple language.

"I'll have one of my deputies bring the man over. Meet at the Town Hall tomorrow at noon for the hearing."

"We'll be there," Enna said. "Thank you."

As we walked, we asked around for work. No one had any. I went to both tailors to show them samples of my silk.

"I have more thread than I know what to do with. I don't have any use for cobwebs. The spider in the corner gives me plenty," the shop owner's assistant, Abigail, pointed to an eight-legged pest in the corner.

*'Am I based on those tiny creatures?'* I wondered. *'Who designed me anyway?'*

Crystal had ears and fangs like mine, but only two legs. Her legs had claws instead of feet. Knobs of skin and callouses were forming on her body. I didn't know if that was normal for whatever kind of creature she was.

Despite the rejection of my silk, I worked to weave a shawl on an invisible loom. They only disliked silk because they had never seen it before.

Enna felt terrible because of her scratches and bee-stings. Crystal prayed for God to heal her. I enjoyed her simple faith.

## The Incident

That afternoon, I visited a tavern, to look for work. Enna watched Crystal.

The owner ushered me into his office and asked a few questions. I thought I answered well because he offered me a job. The female voice in my head warned me to be careful.

"Here's your uniform," he said as he pulled clothes out of a wardrobe. "I'm sure you'll need to alter the skirt."

I examined the skirt and an item which would only cover a third of my torso.

"Is there a blouse?" I asked.

"I gave it to you," he said.

"I'm a married woman," I replied. "I can't wear this in public."

"Why do you think the interview was short?" he asked. "Serving food is not the main reason I need you. The customers need something good to look at."

"I refuse the job if it means wearing this," I pushed myself away from his desk.

"Who else will hire you?" he asked as he reached under his desk with his right hand.

"I don't know," I replied.

"It's a shame to waste your beauty. Art like you should be on display," he flattered me.

"My husband is the only man who can say that to me," I threw the clothes onto the desk and turned.

Two thugs blocked my way.

"The job offer wasn't a request," the owner said.

I was glad that I had left Crystal with Enna. As I scanned for an exit, I readied my death-venom.

"I'm leaving now," I announced.

The thugs stepped closer, so I dashed for the door. One of the men grabbed me by my cheeks and squeezed.

"Put this on," the owner shoved the scanty top at me.

"Help!" I mumbled through the clenched hand.

As the thug pressed my head back, one of my fangs pricked his hand. Death-venom entered his veins. He thrashed wildly and the other thug had to restrain him.

*'If one drop causes that,'* I thought. *'What would a full dose do?'*

As I reached for the door, the owner grabbed me by the hair and yanked.

"You'll be sorry you did that," he belted me on the ear with a short whip.

The thug I had bitten howled incessantly.

Under the blows, I fell to the ground and covered my face with my arms.

The door burst open and a deputy with a drawn sword ordered everyone to freeze. After the thugs dropped their weapons, the officer marched us all outside and told a gawking boy to go get the Sheriff and a doctor.

I explained what happened, but they put me in a jail cell anyway. The doctor treated my wounds and the Sheriff let me keep Crystal with me, so I could feed her.

"Eba hurt," Crystal wailed.

"I'll heal, darling," I said.

I worried about the man that I bit on accident. If he died, I would be in trouble.

I prayed for a fair trial. I couldn't afford to lose my chance at citizenship.

As I slept, the fearsome, female voice said comforting things. The kind words scared me, since I shouldn't have a voice in my head. I begged it to go away, so it stopped talking to me.

## Day in Court

Enna's trial happened at noon. I testified in handcuffs, which didn't help my credibility. Crystal gave an impassioned plea for justice, which moved the Judge. He ordered the beehive man to pay ten silver duckets to Enna and spend three days in jail, since he had heard me say that Enna was pregnant.

At my hearing, I explained how I got into a tussle and said it was self-defense. My enemies said the same thing.

"Herr Judge," I said. "How could three armed men need to defend themselves against an unarmed woman?"

"She came in to beat us up," the thug I hadn't bitten said. "We had to use weapons. She used witches' poison against Kern."

"Did you use a poison?" the Judge asked. "That is a serious crime."

"I am a spider," I said. "I prepared my death-venom to protect myself. When Kern grabbed my mouth, my fang punctured his hand on accident.

"If it is illegal to defend myself with part of my own body, how is that any different than telling you that you can't use your hands to protect yourself?"

"Only a witch could have a body like yours," the tavern owner shouted. "You used dark magic to turn yourself into a monster."

*'That's not true,'* the scary voice thought to me. *'You are an alien to them, but spiders are common to Ildylia.'*

"Being an alien should not be a crime," I said.

"You," the judge pointed to Kern, the thug I bit. "You have not spoken. "What happened?"

Kern rubbed his injured hand. "I know she could have killed me with her venom, but she didn't," he said. "She just wanted to leave safely. If I hadn't grabbed her face, I wouldn't have been pricked. I'm sorry."

Kern told the truth about our skirmish, despite the way it incriminated him. The tavern owner stared daggers at him.

As the Judge pondered his verdict, the court adjourned for the day. I hadn't known poison was illegal. How could they punish me for being a spider and defending myself?

Being in jail was actually good for me, since it gave me shelter. I prayed for a just verdict.

## Sentencing

In the morning, I ached from my lashes. I bled heavily, since I had not recovered from childbirth. Thankfully, the Sheriff's wife drew a bath for me. I intended to repay her kindness some day.

*'I wish I were allowed to heal you,'* the frightening voice thought. *'But I can only talk to you.'*

*'Please, go away,'* I asked. *'The trial's already frightening enough.'*

*'I am sorry,'* the voice replied. *'I really do love you. I have been praying for you.'*

At the trial, Enna and Four didn't come. [^2] Den held Crystal in his lap.

"I demand you lock up this witch," the tavern owner shouted.

"Sit down, sir," the Judge ordered. "I will tell you when I want you to speak."

At roll-call, Kern was absent. A deputy hurried from the jail to report that Kern was dead and covered in a silk bag. Things looked bad for me.

*'He'll come back,'* the frightening voice said.

*'I killed him,'* I thought back.

*'I knew you did not want your venom to kill him. You just wanted to escape. My power used your venom to turn him into a Silkwing.'* <!-- Is this change to Thee's venom permanent for her?-->

*'What's a Silkwing?'* I asked.

*'Lovely insect-people who plant two parasites in their best friends. You were one before you became a spider. Crystal is one,'* the voice replied.

The voice explained silkwing biology, but said she wasn't allowed to explain silkwing origins.

The Judge stared at me. My charge was now murder.

"What do you have to say in your defense?" he asked.

"My fang pricked him accidentally. I did not go to the tavern to hurt anyone. My death-venom is a natural response to fear, like adrenaline," I stated.

"The penalty for your crimes is hanging," the Judge pronounced. "If the deputies concur, you will die."

"Please wait 14 days," I pleaded. "Kern will hatch from the silk cocoon and come back to life."

"How do you know?" he asked.

"The voice in my head told me," I told the truth even though it made me sound crazy.

The Judge and deputies conferred. The room felt stuffy, like it was closing in.

The Judge let the tavern owner and his henchman go, due to insufficient evidence to convict them. Apparently, my lashes didn't count.

When the Judge turned to me, Crystal spoke up, "Eba told you the truth. The silver marble in her skin is a piece of a girl turned to silver. It talks to her mind."

"How do you know, child?" he asked.

"I am too smart to be a normal babe. I have gifts passed on by the girl, who turned to silver. Kern will come back," Crystal declared.

"We are required by law to execute a murderer ten days after the sentence. The sentence is already written, so she must hang." the Judge said.

"Hang me instead," my infant offered.

I broke into tears. Den cried too.

"If the man comes back in 10 days, I will rescind the sentence," the Judge said.

Deputies hauled me away in chains. I prayed for deliverance. Who would care for Crystal?

## Deadline

I relished my last 10 days with Crystal. The voice tried to comfort me, but only increased my fear.

My conscience was clean. I knew the Savior had paid for my sins. I would rise at the Last Day to die no more. The thought of the Last Day eased my mind, but today hurt.

Den said nothing, but visited every day. How could I leave my loyal companion?

My appeals for mercy did no good. I needed four more days for Kern to rise. My innocence would be proven too late.

On the morning of my execution, Den held my hand for an hour. I knew he was praying. Crystal wept inconsolably.

"Goodbye, dear," I said. "Find hope in the Savior. He will always be there for you."

At the gallows, the crowd of curiosity-seekers gawked. Den wasn't there because he didn't want Crystal to see my death. His absence comforted and disturbed me at the same time.

"Do you have any last words?" the executioner asked.

"I will be vindicated. Please care for my daughter and husband," I implored the crowd. "I want my family to be proud of this town and know your friendship." [^3]

My legs trembled as the noose slipped around my neck. The executioner shook too. I knew he was kind.

"This is not your fault," I said. "You are following orders. When you are ready, tell my husband and daughter that I want them to befriend you."

I said a last prayer as the trapdoor opened under me.

***

I woke up inside a silk bag. Light filtered through and I clawed toward the sound of my daughter's voice. Fabric burst and I clenched my husband's hand. He helped me out of the cocoon.

"What happened?" I felt my neck, which had no scar.

"You're a silkwing," Crystal said. "You came back because your life is tied to Ebu."

"Who wrapped me?" I asked.

"I did," Crystal said.

"How did you have enough silk?"

"I can't tell you," Crystal said.

Den helped me bathe and don a dress.

Knocking on the room's door sounded. Den admitted the man I had bitten and the executioner. The executioner looked kinder without his hood. Shame distorted his face.

"Why are you so glum?" I asked. "I am alright."

"I keep thinking, what if it were my wife being executed? She is nursing too," he said.

"You did nothing wrong. I don't resent you at all. We can work toward a law to keep nursing mothers from being executed until after the child can eat food."

I hugged him and wiped the tears from his eyes.

"Thank you," he said. "I'm so sorry."

"I am alive. No permanent harm was done." I turned to the man who was now a silkwing. His antennae, wings, etc. looked good on him.

"I told them the truth, Fraulein Sombermirth," he said. "Thank you for sparing my life. Please forgive me."

"Of course, Kern," I said. "I'll show you mercy like my Savior showed me."

He told me that he shared everything he could with the Sheriff about the tavern owner's criminal schemes. I was glad that the villain would get what he deserved.

When we were alone, I asked Crystal how she survived without my milk.

"Spiders can live off of blood in emergencies," she said. "I had to bite Ebu."

Den nodded. I noticed scars on his wrists.

"I'm sorry," Crystal said. "My venom numbed it, so it didn't hurt."

I wept. I couldn't imagine who scary that would be for Den and Crystal.

"Don't cry, Eba," Crystal said. "You came back. I knew you would."

***

In the morning, the Sheriff and Judge whisked me off to the town hall. On the steps, I joined six other people, who wore bracelets.

The Sheriff unlocked a bracelet and the Judge gave a certificate of citizenship to the person. Kern and I didn't get our bracelets unlocked.

Next, three infants came on stage and certificates were handed to their parents. The crowd cheered.

"These two citizens are unusual," the Judge said. "We know they died recently, since our doctors signed the death warrants. But they came back to life, which we can't explain or deny. The closest law is about people being born in our town, being citizens automatically, so they're being given their certificates."

The Judge handed me an official paper after the Sheriff unlocked my bracelet.

"You know there was a trial, but the verdict was wrong. Theila Sombermirth was innocent. She conducted herself with dignity and honor , even though she was not guilty. We apologize for the hardship we caused her and her family."

"I accept your apology, Herr Judge," I said. "Thank you for allowing me to join this town. I will be a loyal and grateful citizen."

"As for Kern," the Judge said. "Since he was legally born after committing his crimes, those crimes cannot be charged against him. He was brave to tell the truth at the trial."

After the ceremony, I leaned over to kiss Den. Before I could, the Judge spoke up.

"I forgot to tell you," he said. "But you're not legally married anymore. Since you died, the marriage ended."

"I suppose you'll tell me that I'm too young to be allowed to marry," I wondered aloud.

"No, our laws allow you to marry if you're physically mature. We can have the ceremony in an hour," he replied.

The Judge presided over our ceremony, with Crystal and his wife attending. At my insistence, Enna and Four came too.

I had no special dress to wear, but Den's tenderness and affection made our remarriage feel special. We shared water instead of wedding mead, since we had no money.

After we made our vows, Enna and Four left immediately. I had hoped my gesture of inviting them would show that I wanted their friendship, though they had abandoned me when I needed them.

The executioner let us stay in a back room of his house. His wife and children were kind. To his wife's horror, her children asked me questions about being a "monster" and what it's like to bite someone. I didn't mind their questions. They came from curiosity, unlike the prejudices of adults.

With my postpartum bleeding stopped, I devoted more of my silk to making shawls. Den toured the streets, removing garbage, sweeping porches, and doing any job that was too dirty for other people. Even though he came home filthy, I was proud of him. All our money went to buying food. Since I couldn't pay rent, I gave my first shawl to my hostess.

I gave the second shawl to Enna, who refused to wear it and shoved it back into my hands. The Sheriff's wife accepted it instead. As my hostess wore her shawl around town, interest in my product grew.

Soon, I sold a shawl a week, for a nice profit. The tailors wished they had paid attention earlier. To show Enna that I loved her, I labored on a flowing dress to give her after her child was born.

One day, the owner of one of the tailor shops, who had just returned from a three-month-long business trip, called me to his shop. The same assistant, Abigail, who called my silk 'cobwebs', scowled as I entered.

"I came to see Herr Bennett," I said.

"He's busy," she sneered. "He doesn't have time for aliens peddling trinkets."

I set my invitation on the counter.

"Oh, I see," Abigail said. "I didn't realize he would make a mistake... appointment this early in the day."

She reluctantly led me into his office.

"Ah, Fraulein Sombermirth," he shook my hand. "I am glad you came. I see you brought your little girl."

Herr Bennett had graying red hair and freckles like mine. Crystal slept in the silk carrier I had made for her.

"The women talk about your 'silk.' I've never felt any material like it," he said. "Where do you get it?"

"I make it, Herr Bennett," I said.

I couldn't sit in the offered chair because of my odd body shape.

"How?" he asked. "Would you be willing to sell me the secret?"

"Some comes from my wrists," I demonstrated by creating a strand from my left spinneret. "The thicker, stronger silk comes from my abdomen." I blushed. "Then I knit it together."

"She's an overgrown house-spider," the assistant said.

"Then, you cannot let me do it too, since it is just a part of your body? How much will you charge me for a shawl?"

"I would rather be your apprentice than sell to you. I make a good profit on my own," I said.

"A bold one," he said under his breath in a language I somehow understood. "She's too forward."

"I have a resource you cannot access another way," I said in the same language. "I have sold 17 shawls with no training. Imagine what I could do under your instruction."

His jaw dropped. The assistant, who had black hair, heard nothing.

"You are a Merchant?" he asked. "Why didn't you say so?"

"I didn't know I spoke another language until now," I said. "I lost my memory a few months ago."

"Well, you're one of my race," he said. "Only Merchants can speak or hear our language. The only way to learn it is if your parent is a Merchant."

He explained that our race was an ancient guild of interdimensional traders and craftsmen. Because I was part of his people, he agreed to apprentice me.

I went to tell Den the good news, but a crowd of angry women surrounded him and the deputies.

"What's happening?" I asked a deputy after I pushed through the throng.

"We've drafted him, fraulein," he said. "The tavern owner joined a bandit outfit. They have guns and hostages. We have to stop them."

My heart sank. The deputies had swords and crossbows. They couldn't fight guns.

"Why Den?" I asked. "I need his help to care for our child."

"We're sorry fraulein. We have to send 30 men and unemployed go first. If he survives, he'll get his citizenship early."

"I understand," I said. "We will do our duty."

Den embraced me and Crystal. Ever since my resurrection, I could feel his emotions, just like I could see his face. He was afraid, but also proud of my calmness.

I told him about my apprenticeship, so he knew I would have food and shelter. Kern walked to the group and volunteered to go. His crush, Estelle, was one of the hostages. She formerly worked at the tavern.

I made a necklace of silk and put it around Den's neck before he marched away to battle.

Women around me wept or grumbled. I stepped onto a porch and spoke to get them to stop pestering the Sheriff.

"Women of Greenplains," I said. "I know I am new, but I am one of you now. Our husbands and sons have been called to danger, so we must do our duty to maintain our town in their absence."

They listened, shocked that I would stick up for the government that had executed me.

"We must trust God to protect our men and free the hostages. Pray with me."

I prayed for divine protection and for justice to win. I thanked God for a government that wanted to restrain evil.

The older women laughed bitterly. For many, their husbands or sons had already died fighting bandits. They had no faith in divine help.

I awkwardly clutched Crystal as the crowd dispersed. The Sheriff tipped his hat to me.

"I don't believe in your God, fraulein," he said. "But thank you for trying to encourage them."

"The others have left," I said. "What is the normal casualty rate and how long will they be gone?"

"50 percent would be good. I'd give them six weeks before the survivors return," he said.

"My husband will either come back in honor for his bravery or he will have died in valor," I said. "If there's a way to rescue the hostages, he will find it."

"I think the other women need your confidence. Just leave out talk of dying," he tipped his hat again and walked away.

"Ebu brave," Crystal said.

"Yes, he is," I replied. "We must be too."

That night, I thanked my hostess and moved into the tailor's shop. His assistant, Abigail, resented me, since I took her old room and she moved to a smaller one.

Uneasiness over Den's mission filled me. He was a strong man, but bullets could negate that.

Crystal Comfort lived up to her name and prayed for me. Her tiny voice quelled my fear.

## Apprenticeship

In the morning, Bruce, the tailor, showed me around the shop and explained his tools. Abigail seethed.

All the terminology of sewing overwhelmed me, but Crystal drilled me on bobbins and spools and distaffs. Her genius amazed me. I felt reasonably intelligent, but her mind was superhuman.

Life in the shop was exhausting. When I wasn't learning to sew, I waited on customers or cooked meals. Bruce gave Abigail the dirty jobs, like laundry.

One day, I refaced the showroom and set up new displays. Bruce was pleased, so he squeezed my arm. It felt like sparks between us. I knew those feelings were evil, but he was kind and attractive, despite being decades older than me.

I didn't tell anyone about my feelings, but I kept Crystal with me whenever I was with Bruce. His pats on the back or touches on my hand grew more frequent. Even though I knew it was wrong, I was falling for him.

Abigail was fed up with being replaced by me, so she went to work for our competitor.

When Bruce touched my cheek, I pushed his arm away.

"This needs to stop," I said. "I am a married woman."

"Den's not coming back, dear," Bruce said. "The posses are never successful. It's time to accept that he's dead."

Though Den was too far away for me to feel his emotions, I felt my tie to his life. He still lived.

"I need to leave," I said. "I would rather be homeless than betray my husband."

"You can't leave," he said. "The apprenticeship contract guarantees that you will live here for five years. You signed it."

"I will not give you what you want," I said, even though I had enjoyed his attention. "I am Den's girl."

"There are things I can teach you about being a Merchant. You'll never find the portal to our home without me," Bruce tempted me. "Den doesn't need to know."

I fled the shop with a napping Crystal in my arms.

*'What will I do?'* I wondered.

I went to Enna's apartment and knocked. She didn't answer, though I knew she was home. Rumor flew around town about my "affair" with Bruce. No one wanted to help me.

I entered the Sheriff's office. He looked up from a blade he was whetting.

"I need help," I said. "My master is pressuring me to grant him amorous favors. I can't leave his shop because of the apprenticeship contract, but I don't feel safe there."

"I've heard otherwise," The Sheriff said. "Someone today reported your misconduct and accused you of adultery. That crime carries a 50 ducket fine," he said.

I suspected Abigail had reported me.

"The accusation is false," I said. "I never gave in to his advances."

Crystal stood up for me. She recited my last conversation with Bruce, verbatim. The Sheriff took notes.

"I will investigate this, fraulein," he said.

I hoped he would.

I went to Abigail, the former assistant, to beg her to retract her accusation.

"Come to rub it in?" she asked.

"I need your help," I said. "I was falsely accused of adultery. Please tell the Sheriff it isn't true."

"You stole everything from me, but now you want my help?" Abigail asked.

"Bruce made advances, but I refused them. I told the Sheriff about him," I said.

"Please help my Eba," Crystal's big eyes melted Abigail's heart. "She is telling the truth."

"Ever since you came, Bruce started treating me differently," Abigail said. "I had given in to his advances. He said he'd turn me into a Merchant and take me to his world."

"He told me that Merchants are either born that way or become Merchants from a mother's milk," I said.

"He lied ot one of us," Abigail turned away.

"Will you please withdraw your accusation?" I asked.

"I can't," she said. "I'll be punished for lying to the law."

"Whatever you decide," I said. "I want to be your friend. I never meant to irk you."

I left and stopped in a secluded part of the park to feed Crystal. She thanked God before her meal, which was overwhelmingly cute. She brought joy to my life, even on hard days.

I returned to Enna's apartment and knocked on the door.

"It's you," Enna said, as she opened the door.

"May I please come in?" I asked.

A rat scurried under my feet.

"Let her in," Four shouted from inside. "Hear what she has to say."

Enna opened the creaky door and I stepped gingerly over rotting floorboards.

"Are the rumors true?" Enna asked.

"No," I replied. "But they are why I am here. I don't feel safe around Herr Bennett. Will you please come to live with me?"

"Why can't you leave us alone?" Enna asked.

"We were friends. I don't want to lose you. I love you, Enna," I said.

"Love me or my husband?" she asked. "I know you're jealous that I have him and you don't."

A vague memory of being betrothed to Four entered my mind.

"I love Den," I said. "He has all my love."

"That's not what the word on the street says," Enna accused.

"If you do not want to help me, I can accept that," I said. "But this place is not safe for your coming child. It reeks of mold and trash. The shop will be healthier."

Right after I said that, the entire building quaked. Boards creaked and the sound of glass shattering in reverse filled the room.

When the dust cleared, the apartment was new. Boards were no longer rotten. Mold and rats were banished.

"We're fine where we're at," Enna said.

"How did that happen?" Foir asked. "Did you do that, Thee'?"

"No," I said.

"It's a sign that we should stay here," Enna announced. "It's best for you to leave."

I opened the door.

"When you want my friendship, I will be ready," I said.

*'Why does she hate me?'* I wondered. *'How did she trigger a memory of loving Four? What happened to the apartment?'*

I dreaded returning to the shop, but had nowhere else to go. The Executioner had travelers lodging with him, so there would be no room for me.

*'God, please keep me safe. Deliver me from temptation,'* I prayed.

Herr Bennett ordered me to cook the evening meal, but said nothing about my absence during the workday.

I felt his gaze on me as I ate in silence. I kept a dagger under my blouse, just in case he retaliated against me.

***

The next week was torment, as I worked with the man who tried to subtly seduce me. I stayed because Crystal needed food and shelter.

Despite his evil, I still burned with passion for him. My waking hours were spent combating my wrong desires. I grasped my emotional connection to Den. I would not betray him.

Though I completed Enna's dress, I could not give it to her. Without making direct threats, Herr Bennett kept me from leaving. His pistol spoke for him. He ensured I knew it was concealed under his shirt.

At night, he locked me in my room. When I tried to tuck a note asking for help into a bundle of clean laundry, he found it.

After my noon meal, I grew violently ill and lost all the contents of my stomach. I suspected that he had poisoned me.

I needed to escape. My attraction to him had vanished. Crystal was frightened.

In the morning, Crystal fainted and didn't wake up for six hours. I begged to be able to take her to a doctor. Herr Bennett refused.

He shackled my wrists with an invisible chain, that made it impossible to leave. He also stole my dagger.

My one comfort was that fear of my death-venom kept him from fulfilling his desire for me.

***

When I couldn't take it anymore, Den entered the show room. Despite the rumors he heard about me, Den appeared calm. I sensed his rage under the surface. Unlike other people, Den's anger did not lead him to sudden outbursts. It was slow, calm, and rational. Would this time be different?

A sword hung at Den's side. Would he use it against Herr Bennett? Would he use it against me? Only his jealousy was stronger than his rage. I trembled.

Den approached me and kissed my lips. His left hand brushed through my hair, while the other pulled me closer.

"I did not betray you," I said.

Den nodded.

"Do you believe me?" I asked.

"She's lying," Herr Bennett said. "I couldn't resist when she threw herself at me. I never knew a spider could be such a wonderful lover."

Den's fingers twitched by his sword, but he did not touch the hilt.

"She wept when she heard you would be returning," Herr Bennett said. "She couldn't bear to spend another day with an ugly, mute fool."

Den fought to stay calm. Herr Bennett was baiting Den to draw his sword, so he could claim self-defense when he shot Den.

Crystal didn't stir from the crib where she slept. I prayed for deliverance.

"Den," I whispered. "He has an invisible chain on my left wrist that keeps us from leaving. Crystal and I cannot run."

Den nodded.

"Just leave, like she wants you to and everything will be fine," Herr Bennett said. "If you ever threaten her again, I will protect her."

"Herr Bennett," I begged. "If you care for me at all, let us go. You are lying to my husband. Den is always kind."

"What changed? You acted like you wanted me when you flirted," Herr Bennett said.

"I did flirt with him, Den. I was wrong, but I stopped. I betrayed you in my mind, but not with my body. Please forgive me," I pleaded.

Tears formed in Den's eyes. His anger morphed to disappointment, which hurt me worse. Sometimes I hated to have an emotional connection to him.

Den gripped the shackles on my wrist and wrenched them apart with his hands. Metal clanked and blood oozed from his cut fingers. Den scooped up Crystal, but kept his sword-hand free.

"If you won't let me have her," Herr Bennett said, "Then you can't either."

Herr Bennett drew and fired at Den. Den took the bullet in his hand, so he couldn't draw his sword.

"Last chance, Thee'. Do you want to come with me to our homeworld or let your lover watch you die?"

An invisible wall kept Den from attacking Herr Bennett. Another wall between us kept me from helping Den.

"I am Den's girl," I said.

Herr Bennett aimed for my stomach and removed the wall as he fired. Pain ripped through me. I collapsed and clutched my abdomen.

Den set Crystal behind him and pounded on the reformed, invisible wall with his sword.

An insectoid creature burst through the door and clawed at the invisible wall. To Herr Bennett's horror, it breached the barrier and tore the pistol from his hand. He choked as it pinned him against the wall.

"Turn off the walls," I ordered through clenched teeth.

"What makes you think I can?" Herr Bennett gasped.

"Do it," the monster pressed harder. Its fangs convinced Herr Bennett to obey.

The walls disappeared and Den rushed to me. I fought to stay conscious.

"I don't deserve," I gasped. "May I bite you?"

Den held out his wrist.

I sank my fangs into him and healed myself by taking his blood. My wound closed, but Den and I were both weak. Crystal was still unconscious.

The monster bound Herr Bennett with silk and turned to us.

"Who are you?" I asked.

"We are KernEstelle," they said. "We heard a gunshot, so we came to help." the monster had two voices at once.

"Wait," I said. "Are you Kern?"

"Yes," they said. "Den helped rescue the hostages. We found each other. I, Kern, am Estelle's armor now. I bit her."

"Then, why can't I be armor for Den?" I asked.

"We don't know," they said. "We discovered our ability on accident. We don't know how to separate."

"Thank you," I said. "I'm glad you were able to escape the bandits."

"We'll go get the Sheriff," KernEstelle said.

They stepped around toppled furniture and left us with the bound criminal.

"I didn't mean it," Herr Bennett said. "Let me go. I wasn't really going to kill you."

Den handed our child to me and rested his less injured hand on the hilt of his sword. Crystal and Den needed a doctor.

When the Sheriff arrived, my blood and testimony from the three speaking witnesses gave ample evidence for an arrest. I was allowed to take an apron from the crime scene to cover the blood stain on my dress.

I wrapped Den's hands in silk and shared my love-venom to help him heal.

***

When we went to the doctor, he praised my bandaging job on Den's hands and said they should heal if he didn't use them for a week.

When I asked for him to help Crystal because of her fainting spells, he said, "The veterinarian is on the other side of town."

"Doctor," I said. "She is my daughter. I know we are aliens, but if there is anything you can do to help her fainting spells, please do it."

He examined Crystal's eyes, nose, heartbeat, etc. By the time he finished, she still had not stirred.

"I don't see anything wrong with her," the doctor said. "These growths are strange," he pointed to hard armor forming on her head. "But may be normal for her species. I suggest she wear a helmet once she is old enough to walk. A fall could seriously injure her head."

We left with no answers. I slumped on the boardwalk.

"What will we do?" I asked Den.

He put his hand on my shoulder. His calming presence reminded me why I loved hem and made me feel rotten for wanting to betray him. I sobbed.

Since the deputies wouldn't let us enter the shop, we traipsed over to Enna's apartment. I knocked.

"What do you want this time?" Enna asked.

"Can we stay with you tonight, please?" I asked.

Enna shut the door in my face.

I knocked again. After a few minutes, Enna opened the door again.

"Please," I begged. "Crystal is sick. We don't have anywhere else to go."

"You'll have to pay," Enna said. "You got a sword from the raid on the bandits."

Den swung his pack off his shoulder and withdrew a second sword. It was worth two month's wages. Brass accented the beautiful sheath.

Enna snatched the weapon and led us in.

"If you want to stay," Enna said. "Promise you won't tell four where the sword came from. I want to give it to him as a present."

"We promise," I said.

I sat on a footstool and tried to wake Crystal. She didn't stir until my tears showered her cheeks.

"Why Eba cry?" she asked.

"You were asleep for a long time. Are you hungry?" I asked.

"Yes, if it is not too much trouble for you," she said.

It wasn't. I ached from not being able to feed her for so many hours. Enna let me use her bedroom, where Crystal drank ravenously.

When Four came home, he eyed the extra bowls of soup with surprise.

"Some beggars will eat with us," Enna said. I overheard with my large ears.

"Hello, Uncle Four," Crystal said, as we exited the bedroom. "How was work today?"

"Hard, but the foreman said I have a spot on his team at least until the project is finished."

"Yay," Crystal shouted. "Enna's baby will be glad you're taking care of him. What will you name him?"

"Adin," Four said. "Enna picked it out."

After we seated ourselves, I prayed ot thank God for the meal. Enna scowled. She disapproved of our faith.

"Why aren't you at the shop?" Enna asked between spoonfuls of soup.

"There was a crime, so the Sheriff closed off the building."

"Where is Herr Bennett?" Four asked.

"In jail," I said. "He shot me."

"I knew I smelled blood," Enna said. "Was it a lovers' quarrel?"

"The rumors are false," I said. Den clenched his less injured fist under the table. "He shot me because I wasn't his lover."

The meal was tense and awkward.

When I tried to help with dishes, I grew faint and had to sit down.

Despite Enna's misgivings, Four played with Crystal. She giggled at her "uncle" as he made funny faces. Enna glowered from the corner.

Nightmares plagued me. Though Den tried to show his forgiveness, shame for flirting with Herr Bennett, while my husband rescued hostages, overwhelmed me.

*'God, forgive me. I do not deserve Den. Please keep me faithful to him or kill me and give him a more worthy wife,'* I prayed. I knew our Savior had forgiven me, but the stench of my infidelity repulsed me.

***

In the morning, Den and Four went to the work site. Enna lent me a dress, which fit poorly, while I laundered my blood-soaked gown.

"Why do you keep coming back to me?" Enna asked.

"I think we might be family," I said. "We arrived here together. We knew each other before we lost our memories."

"Why did you let me take the sword? You knew you were overpaying," Enna said.

"I want your friendship," I replied. "If I have wronged you, please tell me, so I can make it up to you."

"You just seem to attract trouble," she said. "I want Adin to have a good life. I need citizenship."

Knocking on the door startled us. Enna retrieved the sword she had hidden.

"Sheriff's business," a man shouted. "Open up."

I locked through a peep-hole. It was a deputy.

When I opened the door, the deputy pushed past me.

"Did Bruce Bennett come here?" he asked.

"No," Ennna said.

The officer scoured the apartment, but found nothing.

"What's going on?" Enna demanded.

"Bennett escaped. We're searching house-to-house," the man said as he bustled away.

"Get out," Enna ordered me. "I knew you'd bring trouble. If he comes looking for you, I don't want to be a victim too."

She shoved Crystal and I out and locked the door behind us.

I didn't know where the work site was, so I couldn't go to Den for protection. I hurried toward the Sheriff's office.

On the way there, a hand clamped on my mouth. As I struggled, a leather glove kept my fangs from penetrating the hand. Herr Bennett overpowered me and dragged me toward his shop.

Crystal wailed in terror. As we neared the shop, deputies were forced to drop their swords because of the knife held to Crystal's throat.

In the upper room of the shop, Abigail held a bundle and gave it to Herr Bennett.

"I have what you asked for," she said. "Now, take me with you."

Herr Bennett threw a switch to activate a portal.

"Only Merchants can come through," he said. "Sorry, Abigail."

He stepped through he portal with Crystal and I. Rust-colored light surrounded us. Crystal stopped crying in the portal's warm caress.

***

At the other end, we emerged on a loading dock. Herr Bennett kept his knife hidden from the Merchants around us.

"The location's compromised. Destroy the portal," Herr Bennett ordered. "We had a Z-12."

Technicians sprang to a control panel and demolished my portal to home. Crystal fainted.

As technicians flooded the room, a security officer, with a compact rifle, sauntered up to Bruce.

"What happened?" he asked.

"We thought it was a primitive world, but they hid their technology. They experimented on my wife and daughter to make chimeras. They want our portals."

Herr Bennett's knife tickled my back.

"Is that what happened?" the officer asked me.

"Please," Herr Bennett said. "She's too traumatized to speak." He removed his gloves in the hot room.

"I addressed her," the officer said.

"I didn't know swords and glass windows were considered high technology," I said.

"What do you mean?" the officer asked.

"Ask the people of Greenplains about Herr Bennett and whether he is a reliable source."

"Enough," Herr Bennett smacked me in the face.

The security officer leveled his rifle at Herr Bennett.

I dashed away. Herr Bennett writhed in agony. My venom had touched his blood when he smacked me.

"He brought us here against our will. He escaped jail and forced me through the portal at knife-point," I said.

"Who are you?" guards demanded as they surrounded me.

"Theila Sombermirth, wife of Den," I replied. "This is Crystal Comfort."

"What did you do to him?"

"Me venom entered him when his hand hit my fang," I said. "All I want is to go home to Den."

"We need a doctor in here," the first officer said.

"No need," another guard replied after he felt for a pulse. "He's dead."

"I don't know who you are or where you came from, but you're under arrest," the officer said.

***

In a drab room, with a table and two stools, I explained Silkwing biology and that Herr Bennett might resurrect because of my altered venom. I hoped it would work the same way it had for Kern. I didn't want blood on my conscience.

Crystal did not stir. I showed a nurse my gunshot scar to prove my story.

"Your story doesn't make sense," the officer said. "But neither did Bennett's. There's no way its a Z-12. I think a Z-13 is mor elikely."

"Does that mean I can go home?" I asked.

He laughed. "It means we'll need to shut down the whole sector. Worlds connected to Greenplains aren't safe anymore.

Gray light filled the room as Abigail appeared.

"You dirty kidnappers," she yelled.

The officer called for backup, but Abigail touched us and we returned to Greenplains.

"How did you get there?" I asked.

"I wished to rescue you and I showed up there," Abigail said. "Crystal gave me the chance to undo my crime."

"I grant wishes," Crystal said.

"You were asleep," I retorted.

"She was with me," Abigail put her hands on her hips. "I don't know how she made it through the portal before the shop exploded."

"Where do you think I am when I dream?" Crystal asked. "I travel to other worlds and grant wishes."

"You're asleep when you're dreaming," I explained to the child. "You must have dreamed about the shop."

"Of course I did" Crystal said. "How else would I get there? Don't you travel in your dreams?"

"No," I said. My head hurt.

"I'm sorry you can't travel. Do you, Abigail?" Crystal asked.

"No," she said. "Unless this is a dream."

Deputies handcuffed Abigail for aiding Herr Bennett's escape.

***

At the Sheriff's Office, we explained what happened. The day's events exhausted me.

Though Abigail believed Crystal's story about dreaming, I did not. It was as if another mind willed me to disbelieve.

"Where will we go?" I asked Den. "The shop is ruined."

Den held Crystal and me close to his chest.

"I'll ask around to see if anyone has a spare room," the Sheriff said. "You can stay in the office tonight."

"There's no need," Enna said from the doorway. "You can stay with me."

I raised my eyebrows in disbelief.

"I went to the shop after the explosion. I found this in the street," Enna held up the silk dress I had made for her. "I know its for me because it has a sleeve for my tail."

"Yes," I said. "I've been working on it for weeks. I was going to give it to you after Adin was born."

"You did this after I was a horrid friend? I've done nothing but push you away."

I hugged Enna. "I do not know if we are really related, but I feel like we're family.

Enna and Four wept.

"Can you ever forgive us?" Four asked.

"My Savior forgave me, so I forgive you," I said. "There is nothing between us anymore."

Den nodded. I remembered his forgiveness of my infidelity.

I slept soundly in the safety of Den's arms. His sword comforted me. If I were were ever in danger, he would protect me.

***

At the trial, Abigail told the whole truth and I was acquitted of the charge of adultery. Publicly explaining my evil attraction to Herr Bennett stung my pride.

Since Herr Bennett was banished, his shop was put up for public auction. Enna sold the sword and used most of the money to buy the shop. No one had offered a higher price for the "cursed" place.

With the rest of the money, we rebuilt the shop (at a fraction of its former glory). Enna and Four helped us to establish a profitable business.

When he was born, Adin looked just like Enna. She looked stunning in the dress I created.

I visited Abigail in jail and we became good friends.

Crystal's fainting spells and stories of dream-travel continued. She loved to stare at the silver marble embedded below my throat.

I wondered if Herr Bennett resurrected as a silkwing or if my venom killed him for good. However, Den proved himself infinitely superior to other men. He was the sweetest father, bravest fighter, dearest friend, and most selfless lover. I did not deserve him.

[1]: Malachi 3:5, James 5:1-6
[2]: Matthew 25:43
[3]: Inspired by *Billy Bud*.
