<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Love is Chaos

An "assassin" is a cupid for hire. He matchmakes for hire. Sometimes the
resulting marriages stabilize political relationships. Other times they cause
war.

He gives half of the love potion to one of his targets. To prevent guards
discovering the other half of the potion in his possession, he drinks it.

He becomes madly in love with the princess.

He is the victim of his own game.
