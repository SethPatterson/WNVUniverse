<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Loving Subject 18

A program creates half-human/half-alien hybrids to combat aliens. THe program is unsuccessfull. If Subject 18 cannot succeed, all of hte younger subjects will be killed.

A woman agrees to go to a far-off space-station to pay for her twin sisters' operation.

The woman did not know that her mission is to fall in love with Subject 18 to give him motivation to be loyal and protect the human fleet.

They both respect each other because they realize that they are both motivated by protecting their siblings.

The woman's sisters are being held hostage to ensure her cooperation.

After an accident injures the woman, Subject 18 uses his telepathic talents to help her recover from her coma. He initially distrusted the woman, but after reading her mind becomes angry when he realizes that she is as trapped as him.