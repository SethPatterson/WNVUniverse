<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Trench Angel

## Birth

We gasped for breath as water trickled down our face. Something like extra limbs protruded form our back. Darkness surrounded us and we stood in a ruined building.

*'Where are we?'* Woethief, our kind-hearted personality, wondered.

*'Your guess is as good as mine,'* Nyla thought.

We waddled stiffly away from the water above us. The ceiling was crying.

A rattling noise sounded in the distance, followed by a human scream. We crept toward the noise, thankful that we had a generally human body.

Outside the building, a man thrashed as blood pooled below him. We cloaked ourself in twilight fog and knelt by him.

The man pleaded with us in a language, which we did not understand. As we applied pressure to his wound, we begged God to help him. Healing surged through us, replacing his wound with a gray scar.

The man grabbed a rifle and crawled behind the building we had left. We followed. After he caught his breath, he ran into the night. We followed, with the glow of our twilight lighting our path.

After running a few kilometers through rough terrain, we entered a trench and soldiers aimed at or bowed before us. Fear and fatigue filled the place.

We held up our hands as the man we had saved talked with an officer. Woethief felt his emotions and knew that he trusted us.

The officer reached out his hand and we shook it in the Ildylian way, which baffled him.

"We're WoeNyl," we said in Low Darkish. He did not understand us, but ushered us into a bunker. Having a ceiling over our head comforted us. The outside world seemed to had a ceiling of puffy gray, which didn't seem very solid.

Weary soldiers eyed us with awe. As we focused on our back-limbs, we realized they were feathery wings. Our proportions seemed childlike and we wondered how we had arrived in such a strange world.

Hunger siezed us, so we pointed at our stomach. A man offered us a metal rectangle, which we stared at in confusion. He took it back, twisted a knob, revealing small, scaly animals inside. We revolted at the smell, but ate them anyway. Their heads crunched as we chew.

Next, someone handed us salty, dry bread. It tasted better than the animals. We smiled at the men as they asked questions in a foreign tongue.

A commotion outside distracted us. Men approached with a stretcher between them. A young soldier clung to life. Bleeding stumps stood in place of his legs.

*'What if we can heal him to?'* Woethief wondered.

*'We can try,'* Nyla replied.

We placed our hands on the casualty and prayed for healing. Power surged from us and gray legs, like ours in our past life regrew on the man.

Soldiers shouted and chattered around us. They took our hands in theirs as if their inspection would show something extraordinary. Our hands were meaty, but not special.

The officer ordered two soldiers to take us somewhere. Though they carried rifles, they did not frighten us. Awe and hope danced in their souls.

After we traversed a maze of trenches, we entered a building with rows of groaning men. Overworked doctors and nurses tended to the dying. Filth filled the ground and air.

We knelt beside the first wounded man and prayed for him as the soldiers talked to a doctor. The man smiled at us and fell into a deep sleep.

After healing two more men, our lips were cracked and bleeding. We felt dehydrated, so we charaded the act of drinking. After a drink, our lips healed.

*'Water must be what heals,'* Woethief thought.

*'It makes sense,'* Nyla replied. *'The mass to replace limbs has to come from somewhere.'*

We shook our empty canteen to demand more water. With a steady supply, God worked the miracle of healing through us.

Each soldier's wounds were replaced by part of Nyla, our human form before we came to this world. We could not heal some injuries on the torso or abdomen, since the soldiers were all male.

We collapsed face-down in an empty cot and slept until morning.

## The Tongue of an Angel

A hot light streamed through the entrance of the tent. We rubbed our eyes and gazed about in confusion.

"Where are we?" Nyla asked aloud.

A nurse in her forties handed us a canteen. Her smile reassured me.

Despite the squalor of the tent the night before, everything looked pristine and new. We drank eagerly and signed that we wanted more food.

"Can you understand us?" we asked the nurse in High Darkish, Low Darkish, Ildylian, and twelve other languages. She did not understand any of them.

Because we protected our sister, Theila Sombermirth's, memories, we knew the languages she had learned. She was a wise celestial being.

Despite our language barrier, everyone treated us with respect. Healed soldiers shook our hands or hugged us. One kissed us on the lips, but we shoved him away, embarrased.

After a breakfast of scale animals, sour green stuff, and bread, four soldiers and the nurse escorted us to another hospital. We eyed the ceilingless expanse above us with fear and wonder.

*'Are you learning any of their words?'* Nyla asked Woethief.

*'No,'* Woethief replied. *'I can feel that they like us, bit I do not know what their words mean.'*

*'How are you doing?'* Nyla asked. *'Is their pain hurting you?'*

*'Yes,'* Woethief thought. *'But their gratitude outweighs it for now.'*

*'Let me know if you need to talk,'* Nyla said. *'I love you. Thank you for being there for me.'*

We hugged ourself. Despite our differences, both halves of us loved each other.

Before we reached the next tent, a scream to our left siezed our attention. The soldiers raised their rifles and trained them on a man, with a different uniform. He grimmaced as he tried to pull his leg out of a hole.

We approached, despite the soldiers' yelling. The man's ankle was shattered from stepping in a hole.

"Shh," we stroked his face with our thumb. "We're going to help you."

We poured water onto his ankle and touched it.

"God, please heal his leg," we prayed. His broken bones straightened as twilight danced around him. We helped him out of the hole.

The soldiers stole his pistol and messenger bag. Fear filled him as one of my escorts put his muzzle to the enemy scout's head. The scout wished to see his wife and mother before he died.

"Do not dare pull your trigger," Nyla roared as she pointed our finger at the offender. "He was hurt and he surrendered to you."

Despite not knowing what Nyla said, the soldier decided that angering a healer was a bad idea. He fixed a bayonet and prodded the scout along.

"I am sorry you were scared," Woethief said. "I do not know if I will see my family again either."

Our voice soothed the scout and we continued on our journey. Craters and twisted skeletons of forked life-forms pocked the landscape. Death filled the air.

***

At a checkpoint, soldiers took the prisoner away. Woethief prayed that he would be trwated fairly.

Throughout the day, we prayed for the wounded and drank liters of water. No one understood our speech. Despite our best efforts, some men died before we could reach them.

The fear and torment of the hundreds of men overwhelmed Woethief. We fell to our knees and wept.

*'It hurts too much,'* Woethief thought. *'Why are they doing this to each other?'*

*'I do not know,'* Nyla thought. *'God is using us to fix some of the pain. I am proud of you. I am here for you.'*

A soldier jerked us up by the arm and shoved us toward more wounded.

"How dare you shove Woethief?" Nyla shouted. "She feels the pain of everyone in this room. She's doing the best she can. Show some gratitude."

The soldier squeezed our arm tighter and dragged us onward. Instinctively, Woethief prayed for his healing. A cut on his hand closed and he let go.

"I am sorry I yelled," Nyla said as we put our hand on his shoulder. "I just want to protect Woethief. I love her and hate to see her in pain."

We helped more men, but only healed 73 before we collapsed from exhaustion. Terror dreams plagued our sleep. The horrors of the mens' experiences mingled with sorrow over separation from our family and exile from Ildylia.

## An Understanding

The nurse who brought us to the hospital led us to a private room. A shabby cot, chamber pot, and paper card with markings of colorful objects were the only furnishings.

A yellow dress, which matched the nurse's, was draped against the bed. It was altered to accomodate our wings. The nurse heled us to dress. She motioned to herself and said, "Ruth". We smiled and said, WoeNyl.

After breakfast, we embraced Ruth and thanked her for her kindness. Our hug filled her with longing to see her daughters again.

When Ruth tried to bring us back to the hospital's main room, we shook our head and crossed our arms. We motioned walking with our fingers and charaded layong hands on the wounded. Ruth stared blankly.

Through a series of gestures, we made Ruth understand that we wanted the wounded brought to us. Woethief knew that we could heal more people if we did not have such a cacophany of emotions surrounding us.

That day, God healed 200 men, brought to us on stretchers. We felt alive with the miracle flowing through our hands.

Many soldiers called us, "illum". We did not know what it meant, but it was clearly a compliment. One soldier even showed us a tattoo of a winged woman on his forearm.

A man in long, red robes came to us. He carried a censer and bowed before us. A glowing orb hovered above his forehead.

"Stand up," we said in High Darkish as we lifted him to his feet. "We are only a stewardess, not God."

"Forgive me," he replied in High Darkish. "I've never met an illum before, so I don't know what to do."

"We do not know what an illum is, but we are glad to pray for the wounded," we smiled. "What is an illum?"

"A messenger from the gods," the priest said. "They work miracles among men. Some people call them angels."

"I do not think there are woman angels," Nyla laughed. "We serve the only God. He is kind to work miracles among us."

"How can I help you?" the priest asked.

"First, please put out that smoking, smelly stuff in the censer," Nyla said.

Embarrasment filled the priest as he stifled the censer with its lid.

"Sorry," Woethief said. "In our home, Ildylia, we do not use fire."

"We want to treat wounded prisoners too," Woethief continued. "Our gift cannot be limited to one side in this war. Why is there war anyway."

"We disagree on who owns this part of the land," the priest said.

"So you're ruining it for everybody?" Nyla asked.

The priest looked away.

"Does anyone else speak High Darkish?" Woethief asked.

"No," the priest said. "I can because Ednabelle taught me."

"May we meet Ednabelle?" we asked.

"I will call her, if she answers, you may meet her tomorrow," the priest replied.

"Do you know where we came from?" we asked. "We woke up in a stone building."

"You look like a statue from the temple I served at before the shells destroyed it," the priest set his teeth.

"What was the statue made out of?" we asked.

"Silver," he said.

"A star turned us to silver, so it makes sense that we would be able to bring silver to life," we said. "We are sorry, we did not mean to rob your temple," Woethief added.

"Have no fear, child," the priest said. "We count ourselves blessed to be visited by an illum."

After we ate dinner, we laid in our cot. Nightmares of shellfire and barbed wire haunted us. We wondered how the men endured months of brutality and mud.

***

In the morning, Ednabelle burst into our room. We shielded our eyes against the star.

"Who are you?" Ednabelle thundered. "Where did you come from?

We rubbed our eyes. "Nyla Woethief," we said. "We came from Ildylia."

"That can't be your name," she crossed her arms.

"A star, named Kin, called us Twilight Order," we said.

"Are you the Twilight?" the star woman asked.

"We have twilight around us," we covered ourself in fog. "Our sisters and brother-in-law do too," we frowned. We missed our family.

"Is your sister Theila Sombermirth?" she asked.

"Yes," we said.

The star knelt before us, which looked strange because she had a tail instead of legs. "I swear myself and my line to you. I am your servant to the death."

"We are not illum," Nyla said.

"I know," Ednabelle said. "You are the Twilight though. All stars and Eclipse must bow to you. You are our queen, who will unify the celestials."

We pondered her words. They explained why Kin feared us.

"Your majesty," the star folded her hands together. "May I please have a piece of your twilight?"

"We gave it to our sister, Silver Nectarfang," we said.

"As the Twilight, you have no limit to celestisls you can give gray to," she said.

We held out our hand and a gray orb hovered at Ednabelle's forehead.

"Thank you," she said. "I've seen our deliverence with my own eyes."

"Our twilight is from God," we said. "People think we are special, but we are just a servant of God trying to do what's right."

A look of horror spread over Ednabelle's face.

"You're the Twilight," she rushed out of the room and dragged the priest in.

"Please, give my husband a twilight orb," she begged.

I gifted a gray orb to the priest.

"We must go before the other stars find out I talked to you.The Eclipse will take us in. Beware of Kin."

An explosion of light filled the room. Ednabelle and the priest were gone.

## Prisoners

With the priest gone, we could not talk to anyone. Ruth brought food and we resumed our task of healing. Futility overwhelmed us. We healed only for the same men to be shot again.

<!-- Heal head wound.

Remove knowledge from Theila.-->