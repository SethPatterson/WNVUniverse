<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Basilisk Bride

1. A robber fled through the desert with stolen silver. He had a girl and a wanted poster in every town.
2. The robber hid in a cave that held 10 bottles.
3. Despite a pool of water, he decided to quench his thirst with a bottle, which he assumed held alcohol.
4. A label said that any man who drank it must marry the bottle's occupant. He thought it was a joke.
5. Drinking the bottle freed a female basilisk.
6. The cave entrance collapsed.
7. The basilisk drank all the bottles to free their occupants (females become mothers of bottle occupants).
8. She laid nine eggs. One held a shard of WoeNylVal.
9. The basilisk tried to get the robber to love her as she built a nest and warmed the eggs.
10. He fetched water for her, so she could keep the eggs warm.
11. Kin convinced the robber to leave with the shard egg, while the basilisk slept. Kin promised pleasure to the robber (she had no intention of following through).
12. As the robber left, he glanced back at the waking basilisk. Her teary eyes killed him.
13. The Seeker, his nixie friend, and a posse rescued the basilisk.
14. The Seeker and nixie stayed with the basilisk to help her hunt and care for her hatchlings
15. The nixie stole the basilisk's lethal breath and eyes.
16. Ruby arranged for unstars to bring the basilisk to her home.
17. Because the basilisk considered WoeNylVal her child, Phyla gladly called the basilisk her sister.
18. The basilisk drank strong water, in an attempt to become human.

