<!--
SPDX-FileCopyrightText: 2021 Seth Patterson

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Statue

1. A fragment of WNV lands in a statue, which is vaguely human.
2. The statue tells WNV that it can become any type of person she wants it to be.
3. One of the early concepts the statue learns is "mother", so it asks WNV if she is its mother. WNV says, "Yes".
4. The statue is in love with a girl in the village because she is kind and lonely.
5. When WNV explains the concept of a "wife" to the statue, it thinks she will be disappointed that it decided to call her "mother" earlier instead of "wife".
6. In the village, to prove a man should marry a girl, he must travel through a cave full of monsters and emerge before daybreak.
7. WNV transforms the statue into an athletic man so that he can win his fiancé in the contest.
8. At the beginning of the contest, two men compete to be able to marry the same girl.
9. The statue helps the underdog after his foot is chewed on by a monster.
10. Near daybreak, a monster blocks their path.
11. The statue stabs the fragment from WNV into the monster, who is personized.
12. She tells the statue to race ahead and she will help the wounded man.
13. The statue emerges to kiss his fiancé.
14. The monster drags the wounded man out of the cave and since his rival won the contest, she asks the wounded man if he will marry her. If he was not married, he would have been banished for failing to complete the contest on his own.
15. The monster has a flat face but is able to change her coloration like a chameleon to give the illusion of having a human face.
