<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# You Know You Want Her

<!-- Highroad Squad is traveling through a remote forest. They run across a girl caught on a tree branch in severe rapids. Ralph saves her life.

A yearly rainstorm starts and Highroad Squad is forced to take refuge in the girl's village. The storm is so severe that they must stay under the shelter of the cliff-overhang above the village. They will die if they venture outside.

WNV allows her troops to treat their stay like a furlough. They love the chance to rest. WNV orders her troops to help with the village chores. The villagers are followers of the Way and have a simple democratic form of government with two village leaders serving one year non-consecutive terms and a third leader serving one ten year term.

The girl Ralph saved is enamored with him, but he resists her advances. He doesn't want to break Highroad Squad's "no romantic attachments" rule. WNV tries to get him to understand that he is on furlough, so the restrictions are relaxed. He doesn't get it.

WNV loves Ralph, but knows that her place as leader of Highroad Squad is too important for her to step down and start a family. Ralph has been fighting for years and WNV hopes he retires before he burns out.

WNV and Alish are apprehensive and standoffish because they are not used to peace. Alish is paranoid about the weapons rusting. WNV is the translator for her team because her telepathy allows her to learn languages in a matter of hours.

The girl confides in WNV even though everyone else fears her.

Ralph realizes that this village is the embodiment of what he has been fighting for. The girl makes a necklace out of two braided strands of her hair and places it around Ralph's neck during a village celebration. This act makes her his wife unless he rebuffs her. He does rebuff her.

The girl runs out into the rain on her own. All of the villagers are upset at Ralph. The girl's sister tells WNV to translate and explains what just happened. WNV tells Ralph that if he wishes to stay in the village he will not be a deserter. He has served with the squad long enough to deserve retirement.

He goes out into the rain alone and comes back alive with his new wife.

Highroad Squad leaves two weeks after the wedding with one empty spot in their ranks.-->

## The Proposal

Unity approached the pastor, who worked in his garden.

"Brother," Unity said. "May I talk to you."

He looked up from his bush, which he trimmed with an obsidian knife.

"What is it, Unity?" he asked as he sheathed his knife.

"I need your help," she reached her top, right hand to him and helped him to stand.

She towered over him even though he was not a short man.

"If I can help you, I am willing," he said.

"I want to help my people to be accepted in your culture," she said.

"As do I, how can I help?" he asked.

"I want to have your children," she blurted.

The pastor looked confused.

"What I meant," Unity recovered. "Is that I want to marry you. You're respected by everyone here. If you married me, it would help my kind to be accepted. And if we had children, it would prove that I am just as much a person as you."

"You want to use my influence to change their minds?"

"Yes," Unity said. "I'm sorry if it sounds selfish. I want all of us bees to be treated like the people we are. It's my mission in life."

"You do realize that I already plan to use my influence to help your kind join us?" he said.

"I know you will," Unity stared at her twelve toes. "It's for more than just political reasons. I like you."

"Do you know what you're saying?" he asked.

"I know I'm young, but I know what I want. When my mother showed you her face, you weren't repulsed like most other people. You believed her when she said the accusations were false. You can believe the best about people."

"There are other kind men in this village, ones closer to your age."

"I've only had a soul for a week. All of them are older than me. Do you think I can't love you because you are older? My mother loved an older man and has fond memories of him. He was much older than you."

"I am sure you can find someone better. There is a reason the girls in this village are jealous of you."

"They don't need to be. I want you. You defended us bees even before I was created. You've been a kind friend to my mother when she needed one. I want a man with virtue. I won't be wasting my life if I marry you. I will be giving it away to a man who loves the Way and his people."

"You were created only days ago. Are you certain you want to make such a life-changing decision so soon?"

"Dumb bees only live five years. I don't know if it will be the same way with us. I am an adult. I have many of my mother's skills and her knowledge. I know I am naive but I want to do this for my people."

"Are you saying that I will outlive you?"

"I don't know. I am partially human, but we won't know the lifespan of my kind until we start dying. I wasn't trying to sound desperate. I just wanted you to stop feeling bad for being older than me."

"You have given me much to consider."

"Consider this, that I love you and want to aid my people. If you say 'No,' I will accept that. I know you are attracted to me. I felt your heart-rate increase when I started talking to you," Unity said.

"You certainly are beautiful."

"Then marry me," Unity said. "I know that no girl has ever asked you to marry her, which is why you are afraid. I love you," she threw her necklace of flowers around him and then realized what she had done.

"I am sorry," she said. "I didn't mean to do that. I had it for if you said 'Yes' right away."

"You have given me the flowers, which I accept, not unwillingly," he said with tears in his eyes. "Of course, you will give me a day to pray and seek council with my friends."

"Yes," unity said. "Only we know that I gave it to you. You may give it back and we need not speak of it again."

"Unity," he said. "I cannot break the custom of our people. I know you are a follower of the Way like me. I know you sincerely want to help our people. Meet me at this time tomorrow and I will give you my answer."

She curtseyed and ran away, nearly dying from embarrassment.

## Seeking Advice

When he visited his sister's house to ask her advice. She was ecstatic at the news. She had spent decades praying that her brother would be chosen by a wife.

"She asked me to marry her," he said.

"Why didn't you say 'Yes' on the spot?" she asked.

"She's so young. I don't think she knows what she wants," he said.

"Do you doubt that she actually loves you?" his sister asked.

"She think she does, but how can she really know. She said she wants us to have children so people will believe that her kind are people too."

"She is just being direct. Her mother is the same way. If you were created suddenly in a world where everyone looked defferent from you, wouldn't you want to help your kind? The love she has for the bee-children is the same love she will have for your children."

"You say it as if I will marry her," he said.

"It is our custom. You have a day to find a reason to say "No", but you won't find any. That girl is a treasure. She's only a week old, but already sees needs and fills them. She helped Frau Carpenter patch holes in her house's walls. She reads to the children. You would be foolish to not marry her."

"I will consider what you say."

He talked to other friends, who each encouraged him to marry the girl. In a few days, her kindness and love had won over many people. The ironic thing is that she didn't need to marry a human to win the people's favor. Being herself was enough.

He prayed all night and searched the holy writings. He never imagined that a blue girl with four arms would be in his village, let alone propose to him.

In the morning, he asked to speak to me.

"Captain," he said. "Your daughter has proposed to me," he motioned to the floral wreath. "I wish to know what you think of it."

"My daughter is an adult. Do not think she does not love you because she said she wants to have children to prove that her people are people too. She thinks this is her mission in life."

"Is she old enough to know her mission? I didn't know mine until I was 25."

"I am sorry if it is frightening, but when I personized her, I intended for that to be her life mission. In this place, I've relaxed. I didn't realize how much I had held my powers in until I started accidentally using them here."

"Are you saying you created her to help her kind?"

"I did not create her soul. God did that just like he does with any other child. I've been able to turn dumb animals into people before. I don't know how it works, but I consider them my children."

"What about the other bee-children? Are they your children?"

"No, they look nothing like me. If you look closely at them, they have characteristics from their mothers. I've never personized a group before, but that felt different. My energy made them match their parents."

"Who gave you this energy?" he asked.

"I don't remember not having that. Are you afraid that I am a witch?" I asked. My tattoo's incessent burn intensified. "I use my powers to serve God and my friends as well as I can. They are innate like your ability to speak."

"I must admit that your powers are frightening. Before you came to this village, I didn't think anything like this was possible. I thought they were all stories."

"They probably are. People have vivid imaginations when they create myths. I did not choose to be born powerful, but I do choose to use my powers for God."

"If you chose, how would I answer your daughter's request to marry her?"

"You should embrace her. She was created to unite the bee-children with your village. I can sense people's character. She genuinely trusts our Savior. Her child-like faith is a gift. I know you are a genuinely kind man. You've served your church for twenty years. You'll shepherd my daughter just as well."

"You are not concerned that she is too young?"

"No," I said. "I shared as much of my knowledge and wisdom with Unity as I could. She is a fully capable adult and I am confident that she made the right choice when she chose you."

"That is reassuring," he said.

"You and I both know why you are hesitant though," I said.

"We do?"

"You are captured by her beauty. You are afraid that you might say "Yes" to her for that reason only," I said.

"Yes," he said. "I've wanted a wife for so long. I've wanted the joys of marriage that it seems everyone but me has had."

"God made those joys for a reason," I said. "Unity is a gift from God to you. Please accept that gift joyfully. I know you are too mature of a many to make a decision based only on her beauty, but I made her beautiful for a reason, to give joy to her husband."

"I don't deserve her," he said. "There must be something wrong with me. No woman has chosen me. I have been an adult for three decades and no girl has every wanted me."

"One wants you now," I said. "She loves your character and virtue. But do not underestimate how much she will be in love with all of you. Your baldness won't turn her away from wanting you."

He made an awkward face like people normally do when I know what they are really afraid of.
