<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Silkwing Rescuer

1. After the situation where the silkwing is hunted in the woods, not realizing he is the feared "monster", a man guides him out for a price.
2. The silkwing heals the man's wife of a lingerong sickness.
3. The man sells the silkwing for a bounty, solving his financial probpems.
4. The silkwing comes back to life, because the wife is his silk-friend, and meets the man again.