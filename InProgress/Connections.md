<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Connections

He sought another silver shard. So did Kin, his beautiful, unlovely rival. His rifle waited in his backpack, but his short-sword rested at his hip.

Purple crags and rock surrounded te valley. Camps of exiles dotted the landscape. The Republic of West Argyle sent the worst criminals ad all magic-users into the valley as punisment.

Seeker's feet aced from marching over gravel. He hoped the shard would heal him wen he found her. He needed to find a shard before Kin, at least once.

Nyla Woethief, a woman from another world, was turned to silver, melted, and scattered across worlds. The star, Kin, callenged Seeker to search for the silver shards. If he found a shard first, he returned it to Ruby Centina, WoeNyl's daugher in te hope of te pieces reuniting. Otherwise, Kin kept the shard captive.

As Seeker moved toward the first camp, black clouds rolled in. Wind blew purple dust into his face, stinging like salt in a wound. Hailstoes pelted his head and arms. As the hail intensified, he ducked into a cave, past orange and blue plants.

Two meters into the cave, a numbig web embraced him. He reaced for his sword, but the trap immobilized him. As his arms numbed, a monster slithered toward him.

"Stop struggling," Cara hissed with her forked tongue. "It will be easier for you if you relax."

The monster was a terrible cross between snake, spider, and woman. She writed on eight tails and her spider :abdomen" looked like a human head.

"Wat do you want with me?" Seeker asked.

"Do you not know a soul-snatcher when you see one?" she asked. "Te guards always warn exiles about us,"

"I'm not an exile," Seeker slurred trough numb cheeks. "I'm visiting to look for somethig. Please let me go."

"Why whould I??" Carna asked. "I've been stuck in this cave for 19 months, alone. You're my way out."

Seeker wated to speak, but couldn't. He pleaded with his eyes.