<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Sewer Monster

A boy was pursued in a city and wound up in the sewer where he met a weeping,
middle aged. woman, with reptilian eyes, seemingly made of filth. He overcame
his fear and rather than attack her with a loose pipe, he freed her foot which
was stuck in a grate and offered to help her find her way out. She was impressed
by his compassion and allowed him to accompany her through a portal so he could
hide from his pursuers. Once on the other side, she changed form dramatically.
He was not allowed to return to his homeworld for fear that he would reveal the
location of the portal, so she committed to mothering him, despite the stigma of
allowing an outsider to breach their defenses.
