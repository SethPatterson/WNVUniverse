<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Tea With a Dragon

A boy has a sick mother. The dragon is able to cure her. All previous guests but
one have not returned from teatime. Only the most desperate go to meet him.
