<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Lovely Stepsisters

A plain girl, Elasha, has a lovely stepmother and two lovely stepsisters. Their eyes mesmerize anyone they meet.

Elasha's stepmother insists that Elasha go to the noble school too when her daughter is selected. The family did not know that Epasha is going to learn to be her step-sister's handmaid instead of a noble.

Her step-sister graduates and marries an untrustworthy man despite Elasha's warnings.

Elasha stays at the school to help the next sister.

The bodyguard falls for the youngest sister and works with Elasha to prove that the oldest step-sister married an unfaithful knave.