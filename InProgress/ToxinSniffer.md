<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Toxin Sniffers

<--There should be a world where all people have animal companions who detect
toxins and warn their person of danger. These animals can heal people from
poison, but the animal will die as a result.

There are malicious monsters who hunt the people of this world with a deadly
toxin. The animals are the only way to be protected from the monsters.

In a story, an animal companion is personized (turned into a human) and dies for
their human spouse. Or WNV gets an animal companion.-->
