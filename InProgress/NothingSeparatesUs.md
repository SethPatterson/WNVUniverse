<!--
SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWoethief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Nothing Separates Us

## 0 Prologue

"Prepare for boarding and inspection," Officer Clark ordered over the radio. The
battered freighter looked harmless as he docked to its airlock.

His revolver weightlessly floated in a holster on his belt. He showed his badge
to the spacer, who handed over the manifest, which listed vacuum tubes and
luggage.

"We can start in the cockpit," the spacer said.

"I'll need to see your equipment inspection records," Clark said. "Since you're
hauling passengers, I want to see that everything is safe."

As Clark read logs, counted spacesuits, and analyzed everything else, Captain
Rusty glanced at his watch.

"For a ship this size, my inspections normally take an hour," Clark said.

Captain Rusty gulped.

When Clark reached the passengers, Rusty twitched his mouth. Rusty's eyes
glanced at the end of the passenger seating.

*'He's a bad smuggler,'* Clark thought. He ran his hand along the wall and felt
a hidden latch. Click. A panel floated away, revealing three cowering forms.

"What is this?" Clark asked.

"Please don't hurt my babies," a silkwing mother pleaded from the hidden room.
"We just want to be left alone." Her fangs peeked through her lips as she spoke.

"Captain," Officer Clark turned. "Take the other passengers and go to another
room. I need to talk to these folks alone."

"Yes, sir," Captain Rusty stammered.

"Are you hurt?" Officer Clark asked the mother. "Are you here by choice?"

"We're not slaves, if that's what you mean," the mother clutched her
three-year-old daughter and five-year-old son. "Captain Rusty is an old friend."

"Good," Clark said. "Do you have money for when you get to where you're going?"

"Not much," the woman said as she tucked a floating strand of hair behind her
ear.

Clark pulled 200 points from his wallet and gave them to her.

"Aren't you going to arrest us?" the mother asked. Her antennae bobbed back and
forth in the zero-gravity environment.

"Did you do anything illegal?" Clark asked.

"Besides being silkwings?" the mother asked.

"Yes, besides being blessed," Officer Clark replied. He found the silkwings
endearing, though the Royal Star Police did not. Silkwing fangs struck fear in
the hearts of the people, though silkwings did not use them for violence.

"We're honest people," the mother said. "We just need a chance to prove it."

"I hope you do," Clark said. "One day I want to see you in a badge." He handed a
badge for a "Junior Customs Officer" to the little boy. The boy liked the shiny
sticker.

Clark replaced the panel and thoroughly searched the ship, but found nothing
else illegal after two more hours.

"You keep a tight ship, Captain," Clark said and signed the manifest. "Next
time, don't let your shifting eyes give them away."

"You aren't going to arrest me for hauling monsters?" Rusty asked.

"I only saw people," Clark said. "You're licensed for passengers, so there's no
problem."

## 1 Dilemma

*18 years later.*

Josiah Clark and Stardance Wonder fled down a corridor as Royal Star Police
shouted, "Halt!". They ducked into an open room and barred the door behind them.

"What now?" Stardance panted. "We're trapped."

"Sorry, I took a wrong turn," Josiah replied.

"You should leave," Stardance said. "They aren't after you."

"I'm not going to abandon you," Josiah crossed his arms. "You didn't do anything
wrong. You're my navigator, not a criminal."

"There's no sences in both of us being arrested," Stardance's antennae drooped.

Josiah wondered what his dad would do. He missed him. A shank cut his
life-sentence short.

*'Why would they put a cop in a common prison?'* Josiah wondered. *'All dad did
was help silkwings.'*

"I have an idea," Josiah's heart fluttered as he looked at the brown-skinned
girl who had insect wings. "I've always thought you were kind of cute and I've
been afraid of you and this is awkward..."

"Just say it," Stardance said.

"Will you marry me?" Josiah asked.

"I'm about to go to a camp and that's what you ask?" Stardance threw her hands
up.

"If you bite me, you'll become my armor and we can both escape together," Josiah
said. "You said you're really good at shapeshifting."

"I was, but I haven't been able to since Wendy died," Stardance turned away from
Josiah.

"You could make your face just like hers when you were her armor," Josiah said.
"You can be my armor and we'll look human."

"There are lots of problems with that plan," Stardance said as police searched
each room in the corridor.

"Do you have a better plan?" Josiah asked.

"No, but I don't know what kind of silkwing I am. What if I'm the kind that
can't detach? Even though I connected to Wendy, I don't know. Someone shot us on
the first day, so I never tried detaching. She died inside my armor, so I was
released from her."

"We don't have much time," Josiah said. "I'm willing to save your life, no
matter the cost."

"I don't really want to marry you," Stardance said. "Are you sure you want me?
If I bite an unwilling man, I'll lose a wing."

"Stardance," Josiah said. "It was my idea."

Police opened the next-door room.

"Alright, I'll bite you," Stardance said. "Unbutton your collar."

Josiah obeyed.

"When I bite your throat, my tongues will give venom that hurts more than
anything else you've felt," Stardance warned. "I will become your wife and your
armor."

"Hurry," Josiah said.

Stardance cupped her hand over Josiah's mouth and leaned toward his throat.
"Don't scream and don't bite my fingers off."

Her tentacled tongues embedded in Josiah's veins and he gritted his teeth. It
felt like his body was under five gees of force. He regretted his choice.

The pain subsided, replaced by peace.

"Close your eyes," Stardance whispered into his ear.

"Why?" Josiah asked.

"Your clothes ripped from the armor, so I need to change," Stardance said.

Josiah complied and let the armor, Stardance, move his limbs. Stardance stuffed
the torn clothes into her duffel bag.

Police burst into the room. In their reflection on an officer's glasses, the
couple looked like Stardance, without antennae and wings.

"What are you doing in here?" the officer asked.

"Changing clothes," Stardance said. "Mine ripped."

The officer shook his head. "Use the water-closet next time."

The police moved to the next room.

"Let's get back to the ship," Stardance said. "Remember, this wasn't my idea and
you're a guest in my armor."

"You're amazing," Josiah said.

"Shut up," Stardance snapped. "People will get suspicious if they hear me with
your voice. Just let me walk."

Josiah forced his body to cooperate with the armor's strange gait. THe uniform
was too tight for Stardance. Josiah caught a glimpse of their reflection and
liked what he saw.

"Stop thinking I'm cute," Stardance hissed. "This wasn't my idea and it's too
wierd. You were just my captain 15 minutes ago."

*'Can she read my mind?'* Josiah wondered.

"No, I can't read your mind," Stardance said. "I can feel your true emotions, so
I might as well be able to."

The couple passed a police officer, who searched their bag and waved them on.

At their ship, a spotless freighter, they boarded and met Fixer Honesty,
Stardance's brother.

"Where's the Captain," Fixer asked, then noticed his sister's increased height.
"Oh, I see. I didn't realize certification renewal was so romantic."

"Shut up, I don't want to talk about it," Stardance slumped into the navigator's
chair and started pre-flight checks.

"Aren't you going to weigh yourself?" Fixer asked.

"Right," the couple stood and placed hte duffel bag on the scale. Fixer entered
the weight on the manifest. To Josiah's surprise, he and Stardance weighed
together exactly what they had weighed apart before their marriage.

"My armor is dense," Stardance said.

After finishing pre-flight checks, Stardance made her antennae reappear, so she
could study the nav chart. After biting Josiah, she had every skill he ever
learned. She also had his blue eyes, like Wendy's. She missed Wendy. Biting her
wrist had turned Stardance into an ace navigator. Wendy's skills were like a
scar in her mind, just like Wendy's laugh. Stardance hated jokes. They hurt too
much.

After takeoff, Stardance stormed to her bunk.

"Uggh," she yelled and slammed her hand on the wall. "Why did I say yes?"

"I'm sorry," Josiah said. "I wanted to save you. I would have done the same
thing for your brother."

"Becoming a silk-friend isn't as beig of a deal as becoming a silk-groom,"
Stardance wept. "I'll never get to fall in love now."

Josiah slumped inside the armor.

"Sorry," Stardance said. "It's not you. It's jus thow it happened. I wanted a I
wanted a man to walk me back to my ship in the late afternoon. I wanted a
wedding on a diamond moon with a trane flowing in the light gravity."

"I'm sorry I can't give that to you," Josiah put their left hand on their
shoulder. "Everything I do have is yours."

"Please, don't touch me," Stardance said. "I'm not ready for that yet."

"We can separate, if you want," Josiah said.

"I'm too scared to try," Stardance said. "I'm tired. Please just go to sleep.
Fixer gan get us through dark-jump."

Because of her calming symbiont in him, Josiah fell asleep instantly.

***

In the middle of dark-jump, Stardance woke with a shout.

"What's wrong?" Josiah's voice sounded far away, as darkness muffled sound."

**"I had a nightmare about Wenndy,"** Stardance thrummed. **"I miss her. After
Uncle Rusty died, my eba raised Wendy. Wendy helped me hatch from my cocoon. We
were sisters, but they killed her while trying to kill me."**

"I'm sorry, comet-blossom," Josiah used a common endearment. "I know what that's
like. They took my dad too."

**"Don't call me that,"** Stardance said. **"I'm not in love with you."**

Stardance felt the sting her words caused Josiah, but didn't care. Despite their
physical proximity, they felt alone, drifting through dark-space.

## 2 Outrage

After exiting dark-jump, Stardance floated out of her bunk and checked the
cargo, crates of seeds and farming tools.

"What's your name now?" Fixer asked. "Stariah?"

"You don't get to pick our silk-name," Stardance said. She slammed her clipboard
against a crate.

"You're acting just like when Wendy died," Fixer said. "Is Captain Clark really
that ugly?"

"Just go away," Stardance yelled.

"I will," Fixer nibbled a ration bar. "I'm going to sleep."

Stardance floated to the bridge and checked the nav chart. They were a week from
New Beulah. Sas she listened tot he radio for reports of debris fields, she
plotted the fields on graphs.

"You forgot one in that sector," Josiah pointed.

"Let me control my own hand," Stardance snapped. "I don't need your help."

Josiah watched her silently.

*'What did I do to make her so angry?'* he wondered.

**"I'm angry because you ruined my life plans,"** Stardance said. **"You can't
even change your ID card to say you're married since I'm not legally a person."

"Silkwings know we're married and God knows," Josiah said.

"What are we going to do if customs asks why there are two crew instead of
three?" Stardance asked.

"You'll need to separate," Josiah said. "Though I don't want you to."

**"What if I can't separate?"**

"We pray the officer is like my dad," Josiah said.

**"I'm going to shower,"** Stardance said. **"Don't look while I do."**

"I thought you used my eyes to see," Josiah said.

**"I'll just grow new ones on my armor,"** she said.

Josiah dutifully kept his eyes closed as Stardance went through the antics of
chasing shampoo in zero-gravity. Josiah laughed and a bit of Wendy's voice came
out.

"Stop it," Stardance bawled. "You hav eno right to use her laugh." Stardance
shut off the water and vacuumed herself dry. Her hair floated like seaweeds.

"Why do humans have to ruin everything?" she asked. "You killed my mother and
Wendy. You scared, jealous monsters can't leave us in peace."

"I'm sorry they hurt you," Josiah said. "I wish I could stop them."

"Some animals can't be tamed," Stardance said. "You humans are savages in
spaceships."

Josiah took her tirade quietly. He wanted to hug her, but feared her wrath.

After Stardance dressed, Fixer called over the intercom. "Captain, a ship's
approaching. It looks like the law."

## 3 Boarded
