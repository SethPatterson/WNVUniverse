<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Princess Questionaire

Name: Princess Portentia of the Snowy Isles

Age: 26 years old

1. Do birds flock to you when you sing?

Yes, vultures seem to expect me to die soon whenever I sing.

2. Do dragons and princes fight over you?

Yes, the loser must take me to a royal ball. I cannot dance, so it greatly embarrasses them.

3. Do animals perform tasks for you without being asked?

Yes, moths regularly add new holes to my clothing, curtains, and towels.

4. Have you ever been asleep for more than a year?

No. I have too many business and charitable affairs to manage.

5. Has any knight worn your colors in a tournament?

Yes, but it was an accident. A practical joke against me got out of hand.

6. Have fairies ever made predictions about your future?

Yes, a fairy told me that if I sat on a bench, I would get wet. I did.

7. Have you ever kissed an unexpected person or been kissed by a knight or prince unexpectedly?

No, that's why I wear my silver mask.

8. Do you plan to live hapilly ever after?

Yes, but my silver mine is meeting unexpected difficulties. My competition doesn't bother to pay for protective equipment or safety training.

9. Describe your ideal husband.

A man with a kind heart, matchless courage, and the ability to lead men. I would settle for a man who is not an entitled narcissist, who kisses girls without permission.

10. Describe your wardrobe.

I wear conservative gowns, hand-deyed black, with pops of pastels and embroidered wildflowers to let everyone know I am not in mourning. I just like black.

I wear facepaint, with a skull on one side and aloe leaves on the other to remind myself that I am mortal, yet vibrantly alive.

11. Describe your kindred spirit.

Aileen, a purple fae trolless, with blue wings and hair, who organizes my library and teaches my people to read. We enjoy archery and naturalism together.

I was her maid of honor when she married Sir Took this summer.

12. Why are you inquiring with the Fairy Godmother Service?

I can do a better job than other godmothers because I am a real princess.

Also, trollesses should be eligable for the program too. I have experience with helping my friend Aileen marry a human knight.
