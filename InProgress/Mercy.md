<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->


# Mercy

A new leader takes over a company of war-criminals.

He tries to negotiate a surrender with the enemy dragon.

The dragon wants to kill the whole company for justice, but wants to spare the leader.

The leader had committed crimes in a previous war.