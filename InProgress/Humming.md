<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Humming

What if a girl was able to order the events in her life by humming? Whenever she
was in a chaotic or violent situation, she hummed and the noises around her
became music. That allowed her to predict the next few moments by listening for
patterns. She has a friend who is a guy, but it isn't a love story. The guy
already has a girlfriend.
