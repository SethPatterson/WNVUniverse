<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Crystal's Wishes

## Fountain of Strength

A woman entered a beauty contest. Because she was likely to win and the prize
was large, a rival had her kidnapped and locked up for the day. The woman wished
she were strong enough to escape.

Crystal made the woman muscular so that she could break her bonds and kick her
way out of the shed where she was tied up.

She made it to the beauty contest and won, thanks to Crystal's help with
repairing her dress.

Her husband met her at the convention center where the final contest would be.
When he saw that his wife was now muscular, he was surprised but not upset at
all.

Another rival told Crystal that she wished the fountain the finalists were
drinking from could make all the women as strong as the main character. Crystal
granted the wish and all the finalists except the rival became muscular too.

Because Crystal realized the rival had tricked her and wanted the contest to be
fair, she poured Strongwater in the rival's bottle without her noticing. Now the
contest was fair.

The main character took second place in the contest. The rival placed first.

Any woman who drank the Strongwater became strong permanently. The water had no
effect on men. The convention center made large amounts of money selling the
water from the fountain.

## Ball Gown

A girl wished for a nice ball gown. Crystal turned the girl into a skilled
seamstress and gave her dozens of bolts of fabric, needles, and much thread. The
girl didn't win the duke at the ball but she was never poor again because of her
sewing business. Many of the girls at the ball became her clients.

## Good Wish

A man wished he could think of a good wish. He thought of one.

## Farmers

An old farmer wished that the last three golden retriever puppies from his dog's
litter would survive. Their mother and siblings had died. The farmer's wife
wished they had kids. Crystal granted both wishes at once by personizing the
puppies. The farmer's wife nursed them as her own.

## Speak Up

Someone wished Crystal would speak up. She did.

## Noble

A child wished to marry a noble. The noble became as young as the child, so they
had to wait to grow up to get married.

## Only Human

A human man lived in a city with three million people and only 30 humans. When
he finally found a human girl, she found another man. His landlord's daughter,
who is a songrat, had helped with his search.

The daughter wished the human could find a girl. Crystal granted the wish by
making a silk dress for the girl and cooking a candlelight dinner with the
landlord's help. The girl offered to have a date with the human where they
pretended she was human, since his other date fell through. The meal ended with
him pecking her cheek and asking her to go steady.

## Tree People

Three generations ago, a tree man left his tree to find a bride. He had one week
before he would turn to a wooden statue. If he did not return to his tree by
then, he would likely be burned by suspicious commoners.

Most tree men did not care whether a girl wanted to go with them. They
considered time to be too short to trouble themselves over that.

The tree man carried a woman, with a broken ankle to her home, then was chased
out of the village. He also protected a shepherdess from a lion. As his week
neared an end, he saved a woman from robbers. She was said to be so beautiful
that when she looked at the moon, it reflected her face. Because of his selfless
actions, the woman kept the villagers from burning his wooden statue.

The last woman's great-granddaughter was the youngest child. Her sisters had
married all the good men in her village. When Crystal appeared, the girl wished
to do something heroic. Her brother wished to help her.

Crystal gave them a mission of returning to the tree man's tree to wake him from
his wooden slumber. This was dangerous because it was the time of year that
single tree people emerged from their trees to find mates.

On their way to the forest, the brother was chased into a void tree (a tree
whose owner had been burned or killed). Trapped in his wooden prison, he could
no longer protect his sister.

The sister fled the tree men, going deeper into the forest. In desperation, she
entered what she thought was a void tree. As she entered it, the good tree man
awoke from his slumber.

Vengeful tree men cut down the sister's tree, leaving her stuck until the stump
sprouted.

Ruby appeared and her sword killed the evil tree men before they could uproot
the stump. A gray fire spread through the forest, burning the trees of any tree
people, who had kidnapped people to trap in their trees. The fire freed the
brother from his tree. As he wandered through the gray flames, he helped freed
captives to reach the river (the flames only hurt the wicked trees).

A girl, who had been trapped in a void tree, was pinned under a falling branch.
The brother rescued her, but was knocked out by another falling limb. The girl
prayed to be able to save him. As she held the brother, she sprouted gray
flowers and grew roots. As a tree, she sheltered the brother from the forest
fire.

Elsewhere, a woman, who had been kidnapped, prayed for her kidnapper to be
spared. After he had kidnapped her, he felt so guilty that he spent the next
decade praying for the woman to be freed. He never used his position of power to
harm her. The woman's prayer was answered and she too became a gray flower tree.

In the morning, Ruby returned to the cleansed forest. She revealed that the gray
flower trees could shape-shift back into humans at will. Because of the
brother's humility, the sister's bravery, and the other woman's forgiveness, she
declared them king and queens of the forest.

When WoeNylVal visited the forest, she healed all the trees, so they could
shapeshift into human forms (Stewards are not allowed to create stationary
people).
