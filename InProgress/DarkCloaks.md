<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Dark Cloaks

A group of people wear black cloaks and hoods that cover their entire faces.
They are rumored to be monsters underneath the blackness who will curse anyone
who dares look beneath the cloth. A young princess removes the hood of a dark
person and is kidnapped because the dark girl couldn't bring herself to kill the
princess and adopted her instead. The princess could not adjust to the life of a
dark girl because she knew something none of the others did. The dark people
never had any children of their own. They filled their ranks by adopting
abandoned infants and small children. They could see through their hoods and the
cloaks of other members, but the view was distorted. All of their comrades
looked like monsters and that is why they could not bring themselves to marry
each other or ever be seen without their cloaks. The princess had seen her
"mother" without the black fabric to distort her view. They were unbelievably
beautiful without knowing it. Her "blasphemous" views led to the expulsion of
her and her mother from the colony and they were forced to wander in the ruins
of a once great city in invisible cloaks.

After years of scraping by, the princess's "mother", who had removed her
daughter's memories of her past life, decided that the only thing she could do
to save the princess from starvation is bring her back to the palace of the
girl's homeworld and reveal her as the princess.
