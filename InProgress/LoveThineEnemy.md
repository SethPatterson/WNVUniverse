<!--

SPDX-FileCopyrightText: 2023 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Love Thine Enemy

1. A shard of Nyla is kidnapped in a scrubby desert.
2. The monsters who kidnapped her are hideous, with rough, brown skin, knobby limbs, and spear-like claws. Water scalds their skin. They can only touch it with their long tongues.
3. A female monster is particularly cruel and scars Nyla's face.
4. Nyla is kind to her new masters.
5. Dust clouds in the distance signal the approach of soldiers.
6. The monsters and their human slaves hurry to a mesa-fortress.
7. The soldiers cut them off and announce that any monsters that surrender will be enslaved. Those who do not surrender will be slaughtered.
8. Nyla wades into a small pool and beckons any monsters, who wish to live to her.
9. The meanest monster enters the pool.
10. As the monster is submerged, Nyla screams, taking her pain.
11. The monster emerges, with humanity gifted to her by Nyla.
12. Nyla gives the monster her cloak and they mingle with the rescued humans.
13. The soldiers discover the monster.
14. To save the monster's life, Nyla offers to heal all the wounded soldiers. She nearly dies of dehydration.
15. Nyla and the monster go to the last surviving villages of the monster.
16. Since an evil keeper invented the monsters, the steward of the world wants them destroyed.
17. Nyla offers to become their steward to save them.
18. Nyla is cursed in exchange for being allowed to claim the monsters.
19. She humanizes the monsters.
20. Unstars move the monsters to their own area of the Lightish World.

<!-- Meant to be an answer to monsters all dying at the end of books like Kyleah's Mirrors or The Princess and the Goblin. The "baptism" of the monster is similar to Eustace's transformation in The Voyage of the Dawntreader. -->

