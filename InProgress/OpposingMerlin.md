<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Opposing Merlin

1. A man tells the chief advisor that he is an evil wizard.
2. The advisor punishes him by making him mute and unable to see up close.
3. The child prince befriends the man and they learn sign language from the royal tutor.
4. The man does the dirtiest chores.
5. The King dies and the prince is inagurated.
6. The prince banishes the advisor.
7. The advisor returns, disguised as a charming woman, who is distrusted by the good man. They "marry".
8. As revenge against a conquered kingdom and the good man, a queen is forced to marry the good man.
9. The queen is humiliated, but her husband is kind.
10. The man is told by Ruby that he will help to bring down the advisor.
11. The advisor is revealed and holds the new king hostage.
12. The good man dies to rescue the new king.
13. The new king and foreign queen are married and her firstborn becomes an heir.
14. Their kingdoms are at peace after they realize the advisor orchestrated the war.
