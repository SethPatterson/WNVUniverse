<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Storm Beast

1. A woman goes camping woth coworkers.
2. A triple lightning strike formas an aquilateral triangle around her, turning her into a beast.
3. The campers hide from hail in a cave, where another man was already staying.
4. As the woman turns into a beast, the man protects her from her fearful coworkers.
5. Because someone showed love and kindness to her, her beast form is different than others and has retractable claws instead of fixed claws. During storms, she becomes extra loving and compassionate instead od murderous.