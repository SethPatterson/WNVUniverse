<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Seashell People

Little people who live in shells on the beach and ride hermit crabs. The "sound
of the ocean" in a shell is really the sound of their portal generators. Every
shell is a portal to their underwater kingdom. Different shapes take you to
different kingdoms.
