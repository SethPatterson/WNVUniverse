<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Shield and Leaf

A girl at an abusive boarding school is protected by Shield and Leaf, two irremovable earings. Shield appears to protect her from physical harm and his little sister Leaf can heal injuries with an agonizing kiss.

