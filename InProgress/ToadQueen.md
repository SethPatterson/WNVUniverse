<!--

SPDX-FileCopyrightText: 2024 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Toad Queen

Grunt sized up the small woman in the lamplight. Rags clung to her, but could not conceal her comely form. Bronwynn stared at him with expressionless eyes.

"I did not fight for you to fulfil my cravings," Grunt said. "I knew the other men would hurt you, so I wanted to protect you."

Bronwynn nodded.

"I will not force you to marry me," Grunt said. "I will return you unharmed and innocent to you people."

"You would give up what other men died to gain?" Bronwynn asked. "I know that I am quite pretty."

"You are," Grunt said as he rubbed the back of his head.

"Then," Bronwynn took his hands. "Since you act like a gentleman, you may have my as your wife. Though you are a toad and I am a human, I find you quite irresistible. I promised to marry the winner. I am glad it was you."

"I love you," Grunt said. "You are brave and lovely to behold."

Bronwynn touched a piece of silver embedded in Grunt's wrist: us. We inhabited drops of silver, scattered across worlds and bonded with those who picked us up.

"Who is she?" Bronwynn asked.

*'We are Nyla Woethief,'* we thought to her. *'We are Grunt's friend and healer.'*

Gray scars adorned Grunt's skin. We heal by replacing injured skin with copies of our own skin.

"Do I need to be jealous of you?" Bronwynn asked.

*'No,'* we replied. *'We are your wedding present.'*

The short ceremony involved the sharing of meat, the promise that Grunt would hunt for Bronwynn's food and train her sons to be warriors, and the gifting of our silver drop to Bronwynn. We embedded in her forehead, giving her a queenly appearance. Weith the twilight fog that surrounded us, we created a stunning dress. Despite the simplicity of the nuptials, Bronwynn infused them with tenderness.

Grunt leaned down to kiss his bride. She stroked his cheek and smiled with her eyes.

In the wild dancing that followed, Bronwynn surpassed the Toad-folk in agility and stamina. She looked an elder in the eyes and something changed in him.

"Here's a true toad princess," the elder shouted. "She is worthy of a warrior." (Grunt was the chief's son.) "Three cheers fro Bronwynn, daughter of the Moon Wolf." The tribe worshipped the Moon Wolf, "god" of war and hunting.

The toads, ordinarily hostile to humans cheered their new princess.

***

At their hut, Grunt spoke, "I know it will take time for you to adjust to your new life, so I will not ask for the Gift of marriage until you are ready."

"I am ready now," Bronwynn said. "I really do want you. You have shown me that you are a man of honor and courage. I have waited all my life for you, thought I did not know you by name."

We cut off our access to Bronwynn's senses to give her privacy. We wondered what she had done to the elder.

***

In the morning, Grunt gave Bronwynn a tour of the village. Shabby huts and dingy furs failed to impress her. However, she impressed the toads. In the sunlight, her firm muscles and calloused hands evinced strength, industry, and discipline.

Fresh graves marked the three losers of the previous night's fight.

A lovely toad-girl came up to Bronwynn while Grunt was busy planning a hunt.

"You stole him from me," the toad-girl, Brook, shouted. "I demand..."

Bronwynn stared at her eyes and the girl froze mid-sentence.

"I'm so happy for you," the toad-girl hugged Bronwynn. Something strange was happening.

"Thank you, Brook," Bronwynn said. "I will find a worthy man for you."
