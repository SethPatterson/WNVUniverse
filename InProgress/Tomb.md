<!--

SPDX-FileCopyrightText: 2020 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# Tomb

We all go down to the base of the tomb. The ramps slope downward evermore. We
make another right, getting deeper into the abyss.

Why do the living make their abode with the dead? Why must we trip and struggle
over the skeletons of past sacrifices? Will somebody put an end to this ritual?
Are the priests right? Must we sacrifice to this idol?

I, the Woethief, heard their thoughts. I pitied them. My chains rattled as I
marched ahead of them. The sacrifices were never satisfied.

Somebody must end this practice. I determined to do something.

Wailing and chanting echoed up from the bottom of the abyss. The "great" idol
descended from the pinpoint of light above us.

1. WNV destroys the idol by using her twilight for camouflage to sneak behind
and slice its tethers.