<!--

SPDX-FileCopyrightText: 2021 Seth Patterson <NylaWeothief@disroot.org>

SPDX-License-Identifier: CC-BY-SA-4.0

-->

# The Tick

A deranged steward thirsted for knowledge so much that she transformed herself
into a giant tick. She hunted those renowned for their intelligence.

Every year, her people sacrificed the four most intelligent people to her. She
carefully attached to their nervous systems and absorbed their brains. Through
her power, she kept the brains alive.

People stopped learning to read for fear of being intelligent. WNV offered
herself instead of the normal sacrifice.. The tick ate her stone, but WNV helped
the trapped brains to overwhelm the tick's mind. The tick's brain was surgically
removed. WNV took over the tick's body.

She offered to free the brains as spheres which could talk to their spouses'
minds if held. Or she could devour the spouse too.

After she received her own mood, she died. The hatchlings (brains) became new
Grey/Red/Silkwing people. Her people were recreated.